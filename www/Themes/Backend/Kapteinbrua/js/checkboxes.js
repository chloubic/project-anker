$("form label").each(function () {
    if($(this).find("input[type=checkbox]").length > 0)
    {
        $(this).find("input[type=checkbox]").css({display: "none"});
        $(this).find("input[type=checkbox]").after("<div class='kapteinbrua_checkbox'></div>");
        $(this).addClass("kapteinbrua_checkbox_container");
        $(this).parent().find("br").each(function () {
            $(this).remove();
        })
    }
});