<?php

$container = require __DIR__ . '/../app/bootstrap.php';

// Creates new Nette application container
$container->getByType(Nette\Application\Application::class)
	->run();
