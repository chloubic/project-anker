<?php

namespace Anker\BL\ACL;

use Anker\Common\Enums\Constants;
use Anker\DAL\Entities\Role;
use Anker\DAL\Entities\UserMeta;
use Anker\DAL\Infrastructure\AppDbContext;
use Nette\Security\Permission;
use Anker\DAL\Entities\Permission as PermissionEntity;

/**
 * Class AnkerACL represents object for managing Anker users ACLs
 */
class AnkerACL extends Permission
{

    /**
     * @var AppDbContext Database Anker UoW context
     */
    protected $appDbContext;

    public function __construct(AppDbContext $appDbContext)
    {
        $this->appDbContext = $appDbContext;
    }

    /**
     * Initializes ACL data for logged user
     * @param int $id User id
     */
    public function initializeACL(int $id): void
    {
        $this->removeAllResources();
        $this->removeAllRoles();

        $userMetaRole = $this->appDbContext->getRepository(UserMeta::class)->findOneBy(["user" => $id, "identifier" => Constants::USER_ROLE_META_IDENTIFIER]);
        $userMetaAllowed = $this->appDbContext->getRepository(UserMeta::class)->findOneBy(["user" => $id, "identifier" => Constants::USER_PERMISSIONS_META_IDENTIFIER]);
        if ($userMetaRole != null) {
            $roleEntity = $this->appDbContext->getRepository(Role::class)->findOneBy(["identifier" => $userMetaRole->getData()]);
            if ($roleEntity != null) {
                $this->addRole($roleEntity->getIdentifier());

                $this->processPermissions($roleEntity->getAllowed(), $roleEntity->getIdentifier());

                if ($userMetaAllowed != null) {
                    $this->processPermissions(json_decode($userMetaAllowed->getData()), $roleEntity->getIdentified());
                }
            }
        }
    }

    /**
     * Registers database permission into Anker ACL permission system
     * @param string $allowed Database permission identifier
     * @param string $identifier Permission identifier
     */
    protected function processPermissions($allowed, string $identifier): void
    {
        $permissionArray = $this->appDbContext->getRepository(PermissionEntity::class)->findBy(["identifier" => $allowed]);
        if ($permissionArray != null) {
            foreach ($permissionArray as $permission) {
                $resource = Permission::ALL;
                if ($permission->getResource() != "Anker_ALL") {
                    if(!$this->hasResource($permission->getResource())) $this->addResource($permission->getResource());
                    $resource = $permission->getResource();
                }
                $privilege = $permission->getPrivilege() == "Anker_ALL" ? Permission::ALL : $permission->getPrivilege();
                $this->allow($identifier, $resource, $privilege);
            }
        }
    }

    /**
     * Checks if user has permission for specific operation
     * @param |null $resource Resource identifier
     * @param |null $privilege Privilege identifier
     * @return bool If user has permission to do operation
     */
    public function hasACL($resource = self::ALL, $privilege = self::ALL): bool
    {
        if ($resource != self::ALL && !parent::hasResource($resource))
        {
            foreach ($this->getRoles() as $role) {
                if (parent::isAllowed($role, self::ALL, self::ALL)) {
                    return true;
                }
            }
            return false;
        }
        foreach ($this->getRoles() as $role) {
            if (parent::isAllowed($role, $resource, $privilege)) {
                return true;
            }
        }
        return false;
    }

}
