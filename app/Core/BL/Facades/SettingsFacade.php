<?php

namespace Anker\BL\Facades;

use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\DAL\Repositories\SettingsRepository;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class SettingsFacade
{

    private $settingsRepository;

    public function __construct(SettingsRepository $settingsRepository)
    {
        $this->settingsRepository = $settingsRepository;
    }

    public function getValueByIdentifier(string $identifier) : string
    {
        return $this->settingsRepository->findSingleValueByIdentifier($identifier);
    }

    public function setOrCreate(string $identifier, string $value) : void
    {
        $this->settingsRepository->setOrCreate($identifier, $value);
    }

    public function setNewIcon(string $fileName) : void
    {
        $oldName = $this->settingsRepository->setNewIcon($fileName);
        if(!Text::isEmpty($oldName)) FileSystem::delete(Paths::getWWWDir()."/".$oldName);
    }

}