<?php

namespace Anker\BL\Facades;

use Anker\DAL\Repositories\MediaRepository;

class MediaFacade
{

    private $mediaRepository;

    public function __construct(MediaRepository $mediaRepository)
    {
        $this->mediaRepository = $mediaRepository;
    }

    public function insertMedias(\Nette\Security\User $user, array $files) : void
    {
        $this->mediaRepository->insertMedias($user, $files);
    }

    public function getAllMedia(int $offset, int $limit) : array
    {
        return $this->mediaRepository->getAllMedia($offset, $limit);
    }

    public function getById(int $id)
    {
        return $this->mediaRepository->getById($id);
    }

    public function removeById(int $id) : void
    {
        $this->mediaRepository->removeById($id);
    }

    public function getTotalCount() : int
    {
        return $this->mediaRepository->getTotalCount();
    }

}