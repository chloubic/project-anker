<?php


namespace Anker\BL\Facades;


use Anker\DAL\Repositories\PermissionRepository;

class PermissionFacade
{

    private $permissionRepository;

    public function __construct(PermissionRepository $permissionRepository)
    {
        $this->permissionRepository = $permissionRepository;
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->permissionRepository->getAllWithLimit($offset, $limit);
    }

    public function delete(int $id) : void
    {
        $this->permissionRepository->delete($id);
    }

    public function create(string $identifier, string $resource, string $privilege) : void
    {
        $this->permissionRepository->create($identifier, $resource, $privilege);
    }

    public function edit(int $id, string $identifier, string $resource, string $privilege) : void
    {
        $this->permissionRepository->edit($id, $identifier, $resource, $privilege);
    }

    public function getById(int $id)
    {
        return $this->permissionRepository->getById($id);
    }

    public function getAllGrouped() : array
    {
        return $this->permissionRepository->getAllGrouped();
    }

    public function getTotalCount() : int
    {
        return $this->permissionRepository->getTotalCount();
    }
}