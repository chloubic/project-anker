<?php


namespace Anker\BL\Facades;


use Anker\BL\ACL\AnkerACL;
use Anker\DAL\Entities\User;
use Anker\DAL\Entities\UserMeta;
use Anker\DAL\Repositories\SettingsRepository;
use Anker\DAL\Repositories\UserRepository;
use Nette\Security\AuthenticationException;
use Nette\Security\IAuthenticator;
use Nette\Security\Identity;
use Nette\Security\IIdentity;
use Nette\Security\Passwords;
use Nette\Utils\Strings;
use Nette\Utils\Validators;
use Tracy\Debugger;

class UserFacade implements IAuthenticator
{

    private $userRepository;

    private $settingsRepository;

    private $ankerACL;

    public function __construct(UserRepository $userRepository, SettingsRepository $settingsRepository, AnkerACL $ankerACL)
    {
        $this->userRepository = $userRepository;
        $this->settingsRepository = $settingsRepository;
        $this->ankerACL = $ankerACL;
    }

    public function create(
        string $username,
        string $password,
        string $email,
        string $name = "",
        array $metas = array()
    ) : bool
    {
        if(!$this->settingsRepository->registrationEnabled()) return false;

        if($this->userRepository->userExists($username, $email))
            return false;

        if(!Validators::isEmail($email)) return false;

        $user = new User;
        $user->setUsername($username);
        $user->setName($name);
        $user->setNormalizedName(Strings::upper(Strings::normalize($name)));
        $user->setEmail($email);
        $user->setPassword(Passwords::hash($password));

        $this->userRepository->createUser($user);

        $metasArray = [];

        foreach($metas as $key => $value) {
            $meta = new UserMeta;
            $meta->setUser($user);
            $meta->setIdentifier($key);
            $meta->setData($value);
            array_push($metasArray, $meta);
        }

        $this->userRepository->addMetas($metasArray);

        return true;
    }

    public function authenticate(array $credentials) : IIdentity
    {
        [$identifier, $password] = $credentials;
        $user = $this->userRepository->getByAuthenticationData($identifier, $password);
        if(!$user) throw new AuthenticationException("Uživatel nenalezen!", self::IDENTITY_NOT_FOUND);

        $identity = new Identity($user->getId());

        $userRole = $this->userRepository->getUserRoleIdentifier($user->getId());
        if($userRole != "")
        {
            $identity->setRoles([$userRole]);
        }

        $this->ankerACL->initializeACL($user->getId());

        return $identity;
    }

    public function getById(int $id) : User
    {
        return $this->userRepository->getById($id);
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->userRepository->getAllWithLimit($offset, $limit);
    }

    public function getAllAuditsWithLimit(int $offset, int $limit) : array
    {
        return $this->userRepository->getAllAuditsWithLimit($offset, $limit);
    }

    public function delete(int $id) : void
    {
        $this->userRepository->delete($id);
    }

    public function getUserRole(int $id)
    {
        return $this->userRepository->getUserRole($id);
    }

    public function edit(int $id, string $username, string $password, string $email, string $name, array $metas) : void
    {
        $this->userRepository->edit($id, $username, $password, $email, $name, $metas);
    }

}