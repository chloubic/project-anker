<?php


namespace Anker\BL\Facades;


use Anker\DAL\Repositories\PostRepository;
use Nette\Security\IIdentity;
use Nette\Security\User;
use Tracy\Debugger;

class PostFacade
{

    private $postRepository;

    public function __construct(PostRepository $postRepository)
    {
        $this->postRepository = $postRepository;
    }

    public function createPostType(string $slug, array $structure) : void
    {
        $this->postRepository->createPostType($slug, $structure);
    }

    public function getPostTypes() : array
    {
        return $this->postRepository->getPostTypes();
    }

    public function removePostType($id) : void
    {
        $this->postRepository->removePostType($id);
    }

    public function getPostStructure(string $postTypeSlug)
    {
        return $this->postRepository->getPostStructure($postTypeSlug);
    }

    public function createPost(User $user, \stdClass $values, $mutationId) : void
    {
        $this->postRepository->createPost($user, $values, $mutationId);
    }

    public function getPostTypeById(int $id)
    {
        return $this->postRepository->getPostTypeById($id);
    }

    public function getPostTypeByPostId(int $id)
    {
        return $this->postRepository->getPostTypeByPostId($id);
    }

    public function editPostType(int $id, string $slug, array $structure)
    {
        $this->postRepository->editPostType($id, $slug, $structure);
    }

    public function getPostsByTypeAndLangWithLimit(string $type, string $lang, int $offset, int $limit) : array
    {
        return $this->postRepository->getPostsByTypeAndLangWithLimit($type, $lang, $offset, $limit);
    }

    public function filterAvailableLanguages(array $langs, int $mutationId)
    {
        if($mutationId != 0) {
            $posts = $this->postRepository->getPostMutationsById($mutationId);
            if(count($posts) > 0)
            {
                foreach ($posts as $post)
                {
                    $lang = "";
                    foreach ($post["metas"] as $meta)
                    {
                        if($meta["identifier"] == "lang") {
                            $lang = $meta["data"];
                            break;
                        }
                    }
                    if(isset($langs[$lang])) unset($langs[$lang]);
                }
            }
        }
        return $langs;
    }

    public function delete(int $id) : string
    {
        return $this->postRepository->delete($id);
    }

    public function getPostById(int $id)
    {
        return $this->postRepository->getPostById($id);
    }

    public function getLangByPostId(int $id) : string
    {
        return $this->postRepository->getLangByPostId($id);
    }

    public function editPost(\stdClass $values) : void
    {
        $this->postRepository->editPost($values);
    }

    public function getMutationsByPostId(int $id) : array
    {
        return $this->postRepository->getPostMutationsById($id);
    }

    public function getPostBySlug(string $slug, string $locale)
    {
        return $this->postRepository->getPostBySlug($slug, $locale);
    }

    public function getTotalCount() : int
    {
        return $this->postRepository->getTotalCount();
    }

    public function getPostBySlugAndType(string $type, string $slug, string $locale)
    {
        return $this->postRepository->getPostBySlugAndType($type, $slug, $locale);
    }

}