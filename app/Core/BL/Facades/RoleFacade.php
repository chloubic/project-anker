<?php


namespace Anker\BL\Facades;


use Anker\DAL\Entities\Role;
use Anker\DAL\Repositories\RoleRepository;
use Kdyby\Translation\Translator;
use Tracy\Debugger;

class RoleFacade
{

    private $roleRepository;

    private $translator;

    public function __construct(RoleRepository $roleRepository, Translator $translator)
    {
        $this->roleRepository = $roleRepository;
        $this->translator = $translator;
    }

    public function getUserRole(string $identifier) : Role
    {
        return $this->roleRepository->getRoleByIdentifier($identifier);
    }

    public function getRoles() : array
    {
        $roles = [];
        $roleArray = $this->roleRepository->getAllRoles();

        foreach ($roleArray as $role)
        {
            $roles[$role->getIdentifier()] = $this->translator->translate("anker.roles." . $role->getIdentifier());
        }

        return $roles;
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->roleRepository->getAllWithLimit($offset, $limit);
    }

    public function delete(int $id) : void
    {
        $this->roleRepository->delete($id);
    }

    public function create(string $identifier, array $permissions) : void
    {
        $this->roleRepository->create($identifier, $permissions);
    }

    public function getById(int $id)
    {
        return $this->roleRepository->getById($id);
    }

    public function edit(int $id, string $identifier, array $permissions) : void
    {
        $this->roleRepository->edit($id, $identifier, $permissions);
    }

    public function getTotalCount() : int
    {
        return $this->roleRepository->getTotalCount();
    }

}