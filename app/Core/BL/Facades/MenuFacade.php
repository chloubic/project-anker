<?php


namespace Anker\BL\Facades;


use Anker\DAL\Repositories\MenuRepository;

class MenuFacade
{

    private $menuRepository;

    public function __construct(MenuRepository $menuRepository)
    {
        $this->menuRepository = $menuRepository;
    }

    public function delete(int $id) : void
    {
        $this->menuRepository->delete($id);
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->menuRepository->getAllWithLimit($offset, $limit);
    }

    public function create(string $identifier, array $structure) : void
    {
        $this->menuRepository->create($identifier, $structure);
    }

    public function getById(int $id)
    {
        return $this->menuRepository->getById($id);
    }

    public function getByIdentifier(string $identifier)
    {
        return $this->menuRepository->getByIdentifier($identifier);
    }

    public function edit(int $id, string $identifier, array $structure) : void
    {
        $this->menuRepository->edit($id, $identifier, $structure);
    }

}