<?php


namespace Anker\BL\Managers;


use Anker\Common\Utils\Text;
use Kdyby\Translation\Translator;
use Nette\Utils\Html;
use Tracy\Debugger;

class MenuManager
{

    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function renderMenu(array $menu, string $icon = "")
    {
        $result = Html::el("ul");
        foreach ($menu as $item) {
            $wrapper = Html::el("li");
            $result->addHtml($wrapper);
            $name = Html::el("a")
                ->setAttribute("href", $item["link"])
                ->setText($this->translator->translate($item["identifier"]));
            if(isset($item["children"]) && !Text::isEmpty($icon))
            {
                $iconEl = Html::el("img")->setAttribute("src", $icon);
                $name->addHtml($iconEl);
            }
            $wrapper->addHtml($name);
            if (isset($item["children"])) $wrapper->addHtml($this->renderMenu($item["children"], $icon));
        }
        return $result;
    }

}