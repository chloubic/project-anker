<?php

namespace Anker\BL\Managers;

use Anker\Common\Utils\File;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Kdyby\Translation\ITranslator;
use Kdyby\Translation\Translator;
use Kdyby\TranslationControl\Components\TranslationControl\TranslationControl;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Nette\Utils\Strings;
use Tracy\Debugger;

class LanguageManager
{

    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function getPreparedAvailableLanguages(): array
    {
        $locales = $this->translator->getAvailableLocales();
        $result = [];

        $result[""] = $this->translator->translate("anker.languages.lang.no");

        foreach ($locales as $locale) {
            $result[$locale] = $this->translator->translate("anker.languages.lang." . $locale);
        }

        return $result;
    }

    public function getAvailableLanguages(): array
    {
        return $this->translator->getAvailableLocales();
    }

    public function getAvailableLanguagesWithCatalogues(): array
    {
        $collection = [];

        $locales = $this->translator->getAvailableLocales();

        foreach ($locales as $locale) {
            $catalogues = [];
            foreach (Finder::findFiles('*.' . $locale . '.neon')->in(Paths::getLangDir()) as $key => $file) {
                $fileName = str_replace(Paths::getLangDir(), "", $key);
                $fileName = trim($fileName, "/\\");
                $catalogueName = str_replace("." . $locale . ".neon", "", $fileName);
                $catalogues[$catalogueName] = str_replace(".neon", "", $fileName);
            }
            $collection[$locale] = $catalogues;
        }

        return $collection;
    }

    private function languageExists(string $code): bool
    {
        $bool = false;
        foreach (Finder::findFiles('*.' . $code . '.neon')->in(Paths::getLangDir()) as $key => $file) {
            $bool = true;
            break;
        }
        return $bool;
    }

    public function createLanguage(string $code): void
    {
        if ($this->languageExists($code)) return;
        FileSystem::write(Paths::getLangDir() . '/anker.' . $code . '.neon', '');
    }

    public function removeLanguage(string $code): void
    {
        foreach (Finder::findFiles('*.' . $code . '.neon')->in(Paths::getLangDir()) as $key => $file) {
            FileSystem::delete($key);
        }
    }

    private function catalogueExists(string $catalogue): bool
    {
        $bool = false;
        foreach (Finder::findFiles($catalogue . '.neon')->in(Paths::getLangDir()) as $key => $file) {
            $bool = true;
            break;
        }
        return $bool;
    }

    public function createCatalogue(string $catalogue): void
    {
        if ($this->catalogueExists($catalogue)) return;
        FileSystem::write(Paths::getLangDir() . '/' . $catalogue . '.neon', '');
    }

    public function removeCatalogue(string $catalogue): void
    {
        foreach (Finder::findFiles($catalogue . '.neon')->in(Paths::getLangDir()) as $key => $file) {
            FileSystem::delete($key);
        }
    }

    public function getCatalogueResources(string $locale, string $domain)
    {
        $catalogue = $this->translator->getCatalogue($locale);
        if (!in_array($domain, $catalogue->getDomains())) return [];
        return $catalogue->all($domain);
    }

    public function removeResource(string $locale, string $domain, string $identifier): void
    {
        if (!in_array($locale, $this->translator->getAvailableLocales())) return;
        $catalogue = $this->translator->getCatalogue($locale);
        if (!in_array($domain, $catalogue->getDomains())) return;
        $filePath = Paths::getLangDir() . "/" . $domain . "." . $locale . ".neon";
        if (file_exists($filePath)) {
            $decoded = Neon::decode(file_get_contents($filePath));
            if(isset($decoded[$identifier])) unset($decoded[$identifier]);
            FileSystem::write($filePath, Neon::encode($decoded, Neon::BLOCK));
        }
    }

    /**
     * Saves translation to catalog. ONLY NEON IS SUPPORTED AT THIS MOMENT
     *
     * @param string $locale
     * @param $domain
     * @param string $code
     * @param string $string
     */
    public function saveTranslationToCatalogue($locale, $domain, $code, $string)
    {
        Debugger::barDump("blin");
        if (!in_array($locale, $this->translator->getAvailableLocales())) {
            return;
        }
        $filePath = Paths::getLangDir() . "/" . $domain . "." . $locale . ".neon";
        Debugger::barDump($filePath);
        Debugger::barDump($domain, $locale);
        if (file_exists($filePath)) {
            $this->saveTranslationToNeonFile($filePath, $code, $string);
        }
    }

    /**
     * @param string $filePath
     * @param string $code
     * @param string $string
     */
    private function saveTranslationToNeonFile($filePath, $code, $string)
    {
        $actualCatalog = Neon::decode(file_get_contents($filePath));
        if (!is_array($actualCatalog)) {
            $actualCatalog = array();
        }
        $actualCatalog[$code] = $string;
        file_put_contents($filePath, Neon::encode($actualCatalog, Neon::BLOCK));
    }

    public function hasResource(string $locale, string $domain, string $identifier) : bool
    {
        if (!in_array($locale, $this->translator->getAvailableLocales())) return false;
        $catalogue = $this->translator->getCatalogue($locale);
        if (!in_array($domain, $catalogue->getDomains())) return false;
        return $catalogue->has($identifier, $domain);
    }

    public function getTranslator() : ITranslator
    {
        return $this->translator;
    }

    public function getCatalogueResourcesWithLimit(string $locale, string $domain, int $offset, int $limit)
    {
        $resources = $this->getCatalogueResources($locale, $domain);

        $result = [];
        $total = count($resources);
        if($offset < 0 || $offset > ($total - 1)) $offset = count($resources) - 1;
        if(($limit + $offset) > $total) $limit = $total - $offset;
        if(count($resources) > 0) {
            $keys = array_keys($resources);
            for ($i = $offset; $i < ($limit + $offset); $i++) {
                $result[$keys[$i]] = $resources[$keys[$i]];
            }
        }
        return $result;
    }

    public function getTotalCount(string $locale, string $domain) : int
    {
        return count($this->getCatalogueResources($locale, $domain));
    }
}