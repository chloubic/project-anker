<?php


namespace Anker\BL\Managers;


use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\File;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\Extensions\ExtensionList;
use Contributte\Scheduler\Helpers\Debugger;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;

class ExtensionsManager
{

    public function getActiveExtensions() : array
    {
        $result = [];

        $decoded = Neon::decode(File::getContents(Paths::getAnkerExtensions()));

        if(isset($decoded["active"]))
        {
            $result = $decoded["active"];
        }

        return $result;
    }

    public function activate(string $identifier, ExtensionList $extensionList, AnkerWrapper $ankerWrapper) : void
    {
        $extension = $extensionList->seachByIdentifier($identifier);
        if($extension != null)
        {
            $decoded = Neon::decode(File::getContents(Paths::getAnkerExtensions()));

            $activeExtensions = [];

            if(isset($decoded["active"]))
            {
                $activeExtensions = $decoded["active"];
            }

            if(!in_array($identifier, $activeExtensions))
            {
                array_push($activeExtensions, $identifier);
            }

            $decoded["active"] = $activeExtensions;
            FileSystem::write(Paths::getAnkerExtensions(), Neon::encode($decoded, Neon::BLOCK));
            $extension->getExtensionClass()::install($ankerWrapper);
        }
    }

    public function deactivate(string $identifier, ExtensionList $extensionList, AnkerWrapper $ankerWrapper) : void
    {
        $extension = $extensionList->seachByIdentifier($identifier);
        if($extension != null)
        {
            $decoded = Neon::decode(File::getContents(Paths::getAnkerExtensions()));

            $activeExtensions = [];

            if (isset($decoded["active"])) {
                $activeExtensions = $decoded["active"];
            }

            if (in_array($identifier, $activeExtensions)) {
                if (($key = array_search($identifier, $activeExtensions)) !== false) {
                    unset($activeExtensions[$key]);
                }
            }

            $decoded["active"] = $activeExtensions;
            FileSystem::write(Paths::getAnkerExtensions(), Neon::encode($decoded, Neon::BLOCK));
            $extension->getExtensionClass()::uninstall($ankerWrapper);
        }
    }

    public function delete(string $identifier, ExtensionList $extensionList) : void
    {
        $extension = $extensionList->seachByIdentifier($identifier);
        if($extension != null)
        {
            $path = dirname($extension->getInfoFilePath());
            FileSystem::delete($path);
        }
    }

}