<?php

namespace Anker\BL\Managers;

use Anker\Common\Utils\Paths;
use Anker\BL\Facades\MediaFacade;
use Nette\Utils\FileSystem;
use Tracy\Debugger;

class MediaManager
{

    private $mediaFacade;

    public function __construct(MediaFacade $mediaFacade)
    {
        $this->mediaFacade = $mediaFacade;
    }

    public function uploadFiles(\Nette\Security\User $user, array $uploads) : void
    {
        $files = [];
        foreach ($uploads as $file) {
            if ($file->isOk()) {
                $fileExt = strtolower(mb_substr($file->getSanitizedName(), strrpos($file->getSanitizedName(), ".")));
                $newName = str_replace($fileExt, "-", $file->getSanitizedName()) . date('Y-m-d-H-m-s');
                $fileName = $newName . $fileExt;
                $path = Paths::getUploadsDir() . "/" . date("y") . "/" . date("m") . "/" . date("d") . "/" . $fileName;
                array_push($files, ["path" => $path, "dateTime" => date("")]);
                $file->move($path);
            }
        }

        $this->mediaFacade->insertMedias($user, $files);
    }

    public function initilizeWithFileType(int $offset, int $limit) : array
    {
        $mediaArray = [];

        $mediaEntities = $this->mediaFacade->getAllMedia($offset, $limit);

        foreach ($mediaEntities as $media)
        {

                $result = [];

                $result["exists"] = file_exists(Paths::getWWWDir() . $media->getPath());
                $result["isImage"] = $result["exists"] ? (exif_imagetype(Paths::getWWWDir() . $media->getPath()) != false) : false;
                $result["name"] = basename(Paths::getWWWDir() . $media->getPath());
                $result["id"] = $media->getId();

                $mediaArray[$media->getPath()] = $result;
        }

        return $mediaArray;
    }

    public function delete(int $id) : void
    {
        $mediaEntity = $this->mediaFacade->getById($id);
        FileSystem::delete(Paths::getWWWDir() . $mediaEntity->getPath());
        $this->mediaFacade->removeById($id);
    }

    public function getMediaById(int $id)
    {
        $media = $this->mediaFacade->getById($id);
        $result = [];

        $result["path"] = "/" . trim($media->getPath(), "\\/");
        $result["exists"] = file_exists(Paths::getWWWDir() . $media->getPath());
        $result["isImage"] = $result["exists"] ? (exif_imagetype(Paths::getWWWDir() . $media->getPath()) != false) : false;
        $result["name"] = basename(Paths::getWWWDir() . $media->getPath());
        $result["id"] = $media->getId();

        return $result;
    }

    public function getTotalCount() : int
    {
        return $this->mediaFacade->getTotalCount();
    }

}