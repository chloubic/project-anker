<?php


namespace Anker\BL\Managers;


use Anker\Common\Enums\Constants;
use Anker\Common\Utils\Paths;
use Kdyby\Translation\Translator;
use Nette\Neon\Neon;
use Nette\Utils\FileSystem;
use Nette\Utils\Finder;
use Tracy\Debugger;

class ThemesManager
{

    private $translator;

    public function __construct(Translator $translator)
    {
        $this->translator = $translator;
    }

    public function getFrontendThemes() : array
    {
        return $this->loadThemes(Paths::getThemesDir() . "/Frontend");
    }

    public function getBackendThemes() : array
    {
        return $this->loadThemes(Paths::getThemesDir() . "/Backend");
    }

    protected function loadThemes(string $dirPath) : array
    {
        $result = [];

        foreach (Finder::findDirectories('/*/')->in($dirPath) as $key => $dir) {
            $info = [];

            foreach (Finder::findFiles('info.neon')->in($key) as $path => $file)
            {
                $decoded = Neon::decode(file_get_contents($path));
                if(isset($decoded[Constants::ANKER_SECTION_NAME]) && isset($decoded[Constants::ANKER_SECTION_NAME][Constants::THEME_SECTION_NAME]))
                {
                    $decoded = $decoded[Constants::ANKER_SECTION_NAME][Constants::THEME_SECTION_NAME];
                    if(isset($decoded[Constants::THEME_NAME_ATTR]))
                    {
                        $info[Constants::THEME_NAME_ATTR] = $decoded[Constants::THEME_NAME_ATTR];
                    }
                    else
                    {
                        $info[Constants::THEME_NAME_ATTR] = $this->translator->translate("anker.themes.info.name.undefined");
                    }
                    if(isset($decoded[Constants::THEME_DESCRIPTION_ATTR]))
                    {
                        $info[Constants::THEME_DESCRIPTION_ATTR] = $decoded[Constants::THEME_DESCRIPTION_ATTR];
                    }
                    else
                    {
                        $info[Constants::THEME_DESCRIPTION_ATTR] = $this->translator->translate("anker.themes.info.description.undefined");
                    }
                    if(isset($decoded[Constants::THEME_AUTHOR_ATTR]))
                    {
                        $info[Constants::THEME_AUTHOR_ATTR] = $decoded[Constants::THEME_AUTHOR_ATTR];
                    }
                    else
                    {
                        $info[Constants::THEME_AUTHOR_ATTR] = $this->translator->translate("anker.themes.info.author.undefined");
                    }
                    if(isset($decoded[Constants::THEME_VERSION_ATTR]))
                    {
                        $info[Constants::THEME_VERSION_ATTR] = $decoded[Constants::THEME_VERSION_ATTR];
                    }
                    else
                    {
                        $info[Constants::THEME_VERSION_ATTR] = $this->translator->translate("anker.themes.info.version.undefined");
                    }
                    if(isset($decoded[Constants::THEME_IMAGE_ATTR]))
                    {
                        $info[Constants::THEME_IMAGE_ATTR] = $decoded[Constants::THEME_IMAGE_ATTR];
                    }
                    else
                    {
                        $info[Constants::THEME_IMAGE_ATTR] = $this->translator->translate("anker.themes.info.image.undefined");
                    }
                }
                else
                {
                    $info = $this->fillInfoArray($info, [
                        Constants::THEME_NAME_ATTR => $this->translator->translate("anker.themes.info.name.undefined"),
                        Constants::THEME_DESCRIPTION_ATTR => $this->translator->translate("anker.themes.info.description.undefined"),
                        Constants::THEME_AUTHOR_ATTR => $this->translator->translate("anker.themes.info.author.undefined"),
                        Constants::THEME_VERSION_ATTR => $this->translator->translate("anker.themes.info.version.undefined"),
                        Constants::THEME_IMAGE_ATTR => $this->translator->translate("anker.themes.info.image.undefined")
                    ]);
                }
            }

            if(count($info) == 0)
            {
                $info = $this->fillInfoArray($info, [
                    Constants::THEME_NAME_ATTR => $this->translator->translate("anker.themes.info.name.undefined"),
                    Constants::THEME_DESCRIPTION_ATTR => $this->translator->translate("anker.themes.info.description.undefined"),
                    Constants::THEME_AUTHOR_ATTR => $this->translator->translate("anker.themes.info.author.undefined"),
                    Constants::THEME_VERSION_ATTR => $this->translator->translate("anker.themes.info.version.undefined"),
                    Constants::THEME_IMAGE_ATTR => $this->translator->translate("anker.themes.info.image.undefined")
                ]);
            }

            $result[basename($key)] = $info;
        }

        return $result;
    }

    private function fillInfoArray(array $array, array $keys) : array
    {
        foreach ($keys as $key => $value) {
            $array[$key] = $value;
        }

        return $array;
    }

    protected function themeExists(string $path, string $identifier) : bool
    {
        foreach (Finder::findDirectories('/*/')->in($path) as $key => $dir) {
            if(basename($key) == $identifier) return true;
        }
        return false;
    }

    protected function setActivatedTheme(string $type, string $identifier) : void
    {
        $content = file_get_contents(Paths::getAnkerThemes());
        if($content != false)
        {
            $decoded = Neon::decode($content);
            if(isset($decoded[Constants::ANKER_SECTION_NAME]))
            {
                if(isset($decoded[Constants::ANKER_SECTION_NAME]["themes"]))
                {
                    if(isset($decoded[Constants::ANKER_SECTION_NAME]["themes"][$type]))
                    {
                        $decoded[Constants::ANKER_SECTION_NAME]["themes"][$type] = [$identifier];
                        FileSystem::write(Paths::getAnkerThemes(), Neon::encode($decoded, Neon::BLOCK));
                    }
                }
            }
        }
    }

    public function activate(string $identifier) : string
    {
        $type = "frontend";
        $exists = $this->themeExists(Paths::getThemesDir() . "/Frontend", $identifier);
        if(!$exists) {
            $exists = $this->themeExists(Paths::getThemesDir() . "/Backend", $identifier);
            if($exists) {
                $this->setActivatedTheme("backend", $identifier);
                $type = "backend";
            }
        }
        else $this->setActivatedTheme("frontend", $identifier);
        return $type;
    }

    public function delete(string $identifier)
    {
        $type = "frontend";
        $exists = $this->themeExists(Paths::getThemesDir() . "/Frontend", $identifier);
        if(!$exists) {
            $exists = $this->themeExists(Paths::getThemesDir() . "/Backend", $identifier);
            if($exists) {
                FileSystem::delete(Paths::getThemesDir() . "/Backend/" . $identifier);
                $type = "backend";
            }
        }
        else FileSystem::delete(Paths::getThemesDir() . "/Frontend/" . $identifier);
        return $type;
    }

}