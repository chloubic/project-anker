<?php

namespace Anker\BL\Factories;

use Anker\BL\Managers\LanguageManager;
use Anker\DAL\Entities\PostType;
use Anker\Forms\Form;

/**
 * Class PostFactory represents form generator for Anker native post types
 */
class PostFactory
{

    /**
     * @var LanguageManager Anker language manager
     */
    private $languageManager;

    /**
     * @var array Factory registered inputs
     */
    private $inputs = [];

    public function __construct(LanguageManager $languageManager)
    {
        $this->languageManager = $languageManager;
        $this->initializePredefinedInputs();
    }

    /**
     * Registers input factories for Anker forms
     */
    private function initializePredefinedInputs() : void
    {
        $addText = [
            "name" => $this->languageManager->getTranslator()->translate("anker.post.factory.inputs.text.name"),
            "description" => $this->languageManager->getTranslator()->translate("anker.post.factory.inputs.text.description")
        ];

        $this->inputs["addText"] = $addText;

        $addWYSIWYG = [
            "name" => $this->languageManager->getTranslator()->translate("anker.post.factory.inputs.wysiwyg.name"),
            "description" => $this->languageManager->getTranslator()->translate("anker.post.factory.inputs.wysiwyg.description")
        ];

        $this->inputs["addWYSIWYG"] = $addWYSIWYG;
    }

    /**
     * Adds input factory into array
     * @param string $methodName Factory method name
     * @param array $params Input structure
     */
    public function addInput(string $methodName, array $params) : void
    {
        $this->inputs[$methodName] = $params;
    }

    /**
     * @return array Registered inputs in factory
     */
    public function getInputs() : array
    {
        return $this->inputs;
    }

    /**
     * Generates Anker form for custom native post type
     * @param PostType $structure Post type structure array
     * @param array $availableMutations List of available mutations
     * @param int $mutationId Post mutation id
     * @return Form Post type form
     */
    public function create(PostType $structure, array $availableMutations, $mutationId) : Form
    {
        $form = new Form;

        $form->addHidden('_postType')->setRequired(true)->setDefaultValue($structure->getSlug());
        $form->addHidden('_mutationId')->setRequired(true)->setDefaultValue($mutationId);
        $form->addSelect('_lang', $this->languageManager->getTranslator()->translate("anker.post.factory.item._lang"), $availableMutations);
        $form->addText('_slug', $this->languageManager->getTranslator()->translate("anker.post.factory.item._slug"))->setRequired(true);

        foreach ($structure->getFactories() as $object)
        {
            $form->{$object["factory"]}($object["slug"], $this->languageManager->getTranslator()->translate("anker.post.factory.item.".$structure->getSlug().".".$object["slug"]));
        }

        return $form;
    }
}
