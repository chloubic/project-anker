<?php

namespace Anker\Admin\Menu;

use Anker\Common\Enums\Constants;
use Anker\Common\Utils\File;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\DAL\Entities\PostType;
use Anker\DAL\Infrastructure\AppDbContext;
use Anker\Extensions\ExtensionList;
use Anker\Themes\Info;
use Nette\Neon\Neon;
use Nette\Security\User;

/**
 * Class AdminMenuInitializer represents class for admin menu initialization
 */
class AdminMenuInitializer
{

    /**
     * Initializes admin menu for native Anker post types
     * @param AdminMenu $adminMenu Admin menu instance
     * @param AppDbContext $appDbContext Database UoW context
     * @param User $user Logger user instance
     */
    public static function initializeAdminMenuPostTypes(AdminMenu $adminMenu, AppDbContext $appDbContext, User $user) : void
    {
        $postTypeRepository = $appDbContext->getRepository(PostType::class);
        $postTypeEntities = $postTypeRepository->findAll();
        foreach ($postTypeEntities as $entity)
        {
            if(!$user->getAuthorizator()->hasACL('anker_post_'.$entity->getSlug(), 'overview'))
            {
                continue;
            }
            $item = new AdminMenuItem($entity->getSlug(), "/admin/post/overview/".$entity->getSlug());
            $item->addChild(new AdminMenuItem($entity->getSlug().".overview", "/admin/post/overview/".$entity->getSlug()));
            if($user->getAuthorizator()->hasACL('anker_post_'.$entity->getSlug(), 'new'))
            {
                $item->addChild(new AdminMenuItem($entity->getSlug().".new", "/admin/post/new/".$entity->getSlug()));
            }
            $adminMenu->addItem($item);
        }
    }

    /**
     * Initializes admin menu for themes and extensions
     * @param Info $themesInfo Active themes information instance
     * @param ExtensionList $extensionList List of valid Anker extensions
     * @param User $user Logged user instance
     * @param array $activeExtensions List of valid active Anker extensions
     * @return AdminMenu Admin menu instance
     */
    public static function initializeAdminMenu(Info $themesInfo, ExtensionList $extensionList, User $user, array $activeExtensions): AdminMenu
    {
        $adminMenu = new AdminMenu();
        $backendDir = FileSystem::fullPathFrom([Paths::getThemesDir(), Constants::BACKEND_THEMES_DIR, $themesInfo->getBackend(), Constants::PRESENTERS_CONFIG_FILE]);
        if (!Text::isEmpty($backendDir)) {
            self::initializeMenu($backendDir, $adminMenu, $user);
        }
        foreach ($extensionList->getExtensions() as $extension)
        {
            if(in_array($extension->getExtensionIdentifier(), $activeExtensions))
            self::initializeMenu($extension->getInfoFilePath(), $adminMenu, $user);
        }
        return $adminMenu;
    }

    /**
     * Initializes admin menu from configuration NEON file
     * @param string $path Configuration NEON file path
     * @param AdminMenu $adminMenu Admin menu instance
     * @param User $user Logged user instance
     */
    private static function initializeMenu(string $path, AdminMenu $adminMenu, User $user): void
    {
        $configFileContents = File::getContents($path);
        $configObjects = Neon::decode($configFileContents);
        if (isset($configObjects[Constants::ANKER_SECTION_NAME])) {
            $configObjects = $configObjects[Constants::ANKER_SECTION_NAME];
            if (isset($configObjects[Constants::ANKER_ADMIN_SECTION_NAME])) {
                $configObjects = $configObjects[Constants::ANKER_ADMIN_SECTION_NAME];
                if (isset($configObjects[Constants::ANKER_ADMIN_MENU_SECTION_NAME])) {
                    $configObjects = $configObjects[Constants::ANKER_ADMIN_MENU_SECTION_NAME];
                    foreach ($configObjects as $identifier => $object) {
                        if (!isset($object["link"])) continue;
                        if(!self::hasPermissions($object, $user)) continue;
                        $item = new AdminMenuItem($identifier, $object["link"]);
                        if (isset($object["items"])) {
                            self::createMenuItem($item, $object["items"], $user);
                        }
                        $adminMenu->addItem($item);
                    }
                }
            }
        }
    }

    /**
     * Recursively creates menu structure
     * @param AdminMenuItem $parentItem Instance of parent menu item
     * @param mixed $objects Permissions object
     * @param User $user Logged user instance
     */
    private static function createMenuItem(AdminMenuItem $parentItem, $objects, User $user): void
    {
        foreach ($objects as $identifier => $object)
        {
            if (!isset($object["link"])) continue;
            if(!self::hasPermissions($object, $user)) continue;
            $item = new AdminMenuItem($parentItem->getIdentifier() . "." . $identifier, $object["link"]);
            if (isset($object["items"])) {
                self::createMenuItem($item, $object["items"], $user);
            }
            $parentItem->addChild($item);
        }
    }

    /**
     * Checks if logged user has menu permission
     * @param mixed $object Permissions object
     * @param User $user Logged user instance
     * @return bool If user has permission
     */
    private static function hasPermissions($object, User $user) : bool
    {
        if(isset($object["permissions"]))
        {
            $object = $object["permissions"];
            $resource = null;
            $privilege = null;
            if(isset($object["resource"])) {
                $resource = $object["resource"];
                if($resource == "Anker_ALL") $resource = null;
            }
            if(isset($object["privilege"])) {
                $privilege = $object["privilege"];
                if($privilege == "Anker_ALL") $privilege = null;
            }
            return $user->getAuthorizator()->hasACL($resource, $privilege);
        }
        return true;
    }

}
