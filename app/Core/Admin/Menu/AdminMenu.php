<?php

namespace Anker\Admin\Menu;

/**
 * Class AdminMenu describes structure of Anker admin menu
 */
class AdminMenu
{

    /**
     * @var array Hierarchical admin menu items structure
     */
    protected $items;

    public function __construct()
    {
        $this->items = array();
    }

    /**
     * Adds menu item into admin menu
     * @param AdminMenuItem $item Menu item
     * @return AdminMenu Self reference
     */
    public function addItem(AdminMenuItem $item) : AdminMenu
    {
        array_push($this->items, $item);
        return $this;
    }

    /**
     * Returns admin menu items
     * @return array Admin menu items
     */
    public function getItems(): array
    {
        return $this->items;
    }

}
