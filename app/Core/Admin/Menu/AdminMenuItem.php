<?php

namespace Anker\Admin\Menu;

/**
 * Class AdminMenuItem represents admin menu item
 */
class AdminMenuItem
{

    /**
     * @var string Item identifier
     */
    protected $identifier;

    /**
     * @var AdminMenuItem Item child
     */
    protected $children;

    /**
     * @var string Item link
     */
    protected $link;

    public function __construct(string $identifier, string $link)
    {
        $this->children = array();
        $this->identifier = $identifier;
        $this->link = $link;
    }

    /**
     * Adds child into menu structure
     * @param AdminMenuItem $child Item child
     * @return AdminMenuItem Self reference
     */
    public function addChild(AdminMenuItem $child) : AdminMenuItem
    {
        array_push($this->children, $child);
        return $this;
    }

    /**
     * @return array Item children
     */
    public function getChildren() : array
    {
        return $this->children;
    }

    /**
     * @return string Item identifier
     */
    public function getIdentifier(): string
    {
        return $this->identifier;
    }

    /**
     * @return string Item label
     */
    public function getLabel(): string
    {
        return "anker.admin.menu." . $this->identifier;
    }

    /**
     * @return string Item link
     */
    public function getLink(): string
    {
        return $this->link;
    }

}
