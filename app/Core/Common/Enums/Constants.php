<?php


namespace Anker\Common\Enums;

/**
 * Class Constants represents first part of Anker constants
 */
class Constants
{

    public const ANKER_SECTION_NAME = "anker";
    public const ANKER_ADMIN_SECTION_NAME = "admin";
    public const ANKER_INITIALIZER_SECTION_NAME = "initializer";
    public const ANKER_ADMIN_MENU_SECTION_NAME = "menu";
    public const ANKER_PRESENTERS_SECTION_NAME = "presenters";
    public const PRESENTER_SLUG_SECTION_NAME = "SLUG";
    public const FRONTEND_THEMES_DIR = "Frontend";
    public const BACKEND_THEMES_DIR = "Backend";
    public const PRESENTERS_CONFIG_FILE = "anker.config.neon";
    public const PRIVATE_PRESENTER = "Private";
    public const PUBLIC_PRESENTER = "Public";
    public const API_PRESENTER = "Api";
    public const ADD_PREFIX = "add";
    public const USER_ROLE_META_IDENTIFIER = "role";
    public const USER_PERMISSIONS_META_IDENTIFIER = "allowed";
    public const THEME_SECTION_NAME = "theme";
    public const THEME_NAME_ATTR = "name";
    public const THEME_DESCRIPTION_ATTR = "description";
    public const THEME_AUTHOR_ATTR = "author";
    public const THEME_VERSION_ATTR = "version";
    public const THEME_IMAGE_ATTR = "image";
    public const EXTENSION_IDENTIFIER_ATTR = "identifier";
    public const THEME_CLASS_ATTR = "class";

}
