<?php

namespace Anker\Common\Enums;

use MyCLabs\Enum\Enum;

/**
 * Class EntranceType represents available entrance types form Anker CMS
 */
class EntranceType extends Enum
{

    public const PRIVATE = "private";
    public const PUBLIC = "public";
    public const API = "api";

}
