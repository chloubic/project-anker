<?php

namespace Anker\Common\Utils;

/**
 * Class DateTime represents actual datetime provider
 */
class DateTime
{

    /**
     * Returns actual datetime
     * @return \Nette\Utils\DateTime Actual datetime
     */
    public static function now() : \Nette\Utils\DateTime
    {
        return \Nette\Utils\DateTime::from(date(""));
    }

}
