<?php

declare(strict_types=1);

namespace Anker\Common\Utils;

/**
 * Class Paths represents enum of predefined Anker paths
 */
class Paths
{

    private const paths = [
        "baseDir" => __DIR__ . "/../../../..",
        "logDir" => __DIR__ . "/../../../../log",
        "tempDir" => __DIR__ . "/../../../../temp",
        "appDir" => __DIR__ . "/../../..",
        "wwwDir" => __DIR__ . "/../../../../www",
        "langDir" => __DIR__ . "/../../../lang",
        "uploads" => __DIR__ . "/../../../../www/uploads",
        "themesDir" => __DIR__ . "/../../../Themes",
        "extensionsDir" => __DIR__ . "/../../../Extensions",
        "configDir" => __DIR__ . "/../../../config",
        "ankerConfig" => __DIR__ . "/../../../config/anker.config.neon",
        "ankerThemes" => __DIR__ . "/../../../config/anker.themes.neon",
        "ankerExtensions" => __DIR__ . "/../../../config/anker.extensions.neon"
    ];

    public static function getBaseDir() : string
    {
        return realpath(self::paths["baseDir"]);
    }

    public static function getLogDir() : string
    {
        return realpath(self::paths["logDir"]);
    }

    public static function getTempDir() : string
    {
        return realpath(self::paths["tempDir"]);
    }

    public static function getAppDir() : string
    {
        return realpath(self::paths["appDir"]);
    }

    public static function getExtensionsDir() : string
    {
        return realpath(self::paths["extensionsDir"]);
    }

    public static function getThemesDir() : string
    {
        return realpath(self::paths["themesDir"]);
    }

    public static function getConfigDir() : string
    {
        return realpath(self::paths["configDir"]);
    }

    public static function getAnkerConfig() : string
    {
        return realpath(self::paths["ankerConfig"]);
    }

    public static function getAnkerThemes() : string
    {
        return realpath(self::paths["ankerThemes"]);
    }

    public static function getUploadsDir() : string
    {
        if(!file_exists(self::paths["uploads"])) \Nette\Utils\FileSystem::createDir(self::paths["uploads"]);
        return realpath(self::paths["uploads"]);
    }

    public static function getWWWDir()
    {
        return realpath(self::paths["wwwDir"]);
    }

    public static function getLangDir()
    {
        return realpath(self::paths["langDir"]);
    }

    public static function getAnkerExtensions()
    {
        return realpath(self::paths["ankerExtensions"]);
    }

}
