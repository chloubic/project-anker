<?php

declare(strict_types=1);

namespace Anker\Common\Utils;

/**
 * Class FileSystem represents tool for generating valid paths fr filesystem
 */
class FileSystem
{

    /**
     * Generates valid path or empty string if doesn't exist
     * @param array $paths Path parts array
     * @return string Real path or empty string
     */
    public static function fullPathFrom(array $paths) : string
    {
        $path = realpath(join("/", $paths));
        return $path ? $path : "";
    }

    /**
     * Returns file or directory name from full path
     * @param string $path File/Directory full path
     * @return string File/Directory name
     */
    public static function getDirOrFileName(string $path) : string
    {
        $explodedPath = explode(DIRECTORY_SEPARATOR, $path);
        return $explodedPath[count($explodedPath) - 1];
    }

}
