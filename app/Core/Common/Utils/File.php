<?php

declare(strict_types=1);

namespace Anker\Common\Utils;

/**
 * Class File represents tool for reading file contents
 */
class File
{

    /**
     * Reads and returns file content or empty string if file doesn't exist
     * @param string $path File path
     * @return string File content
     */
    public static function getContents(string $path) : string
    {
        $content = file_get_contents($path);
        return $content == false ? "" : $content;
    }

}
