<?php

declare(strict_types=1);

namespace Anker\Common\Utils;

/**
 * Class Text represents basic tool for text manipulation
 */
class Text
{

    /**
     * Checks if string is empty or not
     * @param string $string String to be checked
     * @return bool If string is empty
     */
    public static function isEmpty(string  $string) : bool
    {
        return empty($string);
    }

    /**
     * Checks if string contains substring
     * @param string $string String value
     * @param string $substring Substring value
     * @return bool If string contains substring
     */
    public static function contains(string $string, string $substring) : bool
    {
        if (strpos($string, $substring) !== false) {
            return true;
        }
        return false;
    }

}
