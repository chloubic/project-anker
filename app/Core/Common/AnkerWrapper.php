<?php

declare(strict_types=1);

namespace Anker\Common;

use Anker\Admin\Menu\AdminMenu;
use Anker\Admin\Menu\AdminMenuInitializer;
use Anker\BL\Facades\SettingsFacade;
use Anker\BL\Managers\ExtensionsManager;
use Anker\Common\Enums\Constants;
use Anker\Common\Enums\EntranceType;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\DAL\Infrastructure\AppDbContext;
use Anker\Extensions\ExtensionList;
use Anker\Extensions\ExtensionManager;
use Anker\Latte\IAnkerVindu;
use Anker\Latte\TemplateFinder;
use Anker\Presenters\Handlers\HandlerPresenter;
use Anker\Presenters\Loader\Loader;
use Anker\Presenters\Loader\PresentersList;
use Anker\Themes\IInitializer;
use Anker\Themes\Info;
use Anker\Vinduer\BroVindu;
use Anker\Vinduer\MenuRendererVindu;
use Anker\Vinduer\PagerVindu;
use Anker\Vinduer\TranslateSwitcherVindu;
use Kdyby\Translation\Translator;
use Nette\Application\UI\ITemplate;
use Nette\Application\UI\Presenter;
use Nette\DI\Container;
use Nette\Neon\Neon;
use Nette\Security\User;

/**
 * Class AnkerWrapper represents structure of Anker Core for data manipulation
 */
class AnkerWrapper
{

    /**
     * Default latte Anker error template name with extension
     */
    private const ERROR_TEMPLATE_NAME_EXT = "anker.error.latte";

    /**
     * Default latte Anker error template name without extension
     */
    private const ERROR_TEMPLATE_NAME = "anker.error";

    /**
     * @var EntranceType Value of Anker presenter entrance type
     */
    private $entranceType;

    /**
     * @var Presenter Instance of actual handler presenter
     */
    private $presenter;

    /**
     * @var Translator Nette translator instance
     */
    private $translator;

    /**
     * @var array Initialized array of URL params
     */
    private $params;

    /**
     * @var string Path of actual latte layout template
     */
    private $layout;

    /**
     * @var string Path of actual latte template
     */
    private $template;

    /**
     * @var AnkerHeader Instance of Anker header structure
     */
    private $ankerHeader;

    /**
     * @var Container Instance of Nette DI container
     */
    private $diContainer;

    /**
     * @var ExtensionList List of connected Anker extensions
     */
    private $extensionList;

    /**
     * @var array List of active Anker extensions
     */
    private $activeExtensions;

    /**
     * @var Info Instance of settings of active Anker themes
     */
    private $themesInfo;

    /**
     * @var AppDbContext Instance of database UoW context
     */
    private $appDbContext;

    /**
     * @var PresentersList List of connected Anker presenters
     */
    private $presentersList;

    /**
     * @var AdminMenu Instance of Anker admin menu structure
     */
    private $adminMenu;

    /**
     * @var array List of registered Vindu widgets
     */
    private $vindu;

    /**
     * @var SettingsFacade Instance of settings facade
     */
    private $settingsFacade;

    public function __construct(Container $diContainer, Translator $translator, AppDbContext $appDbContext, AnkerHeader $ankerHeader, SettingsFacade $settingsFacade)
    {
        $this->diContainer = $diContainer;
        $this->appDbContext = $appDbContext;
        $this->translator = $translator;
        $this->ankerHeader = $ankerHeader;
        $this->initializeAnkerHeader($settingsFacade);
        $extensionsManager = $this->diContainer->getByType(ExtensionsManager::class);
        $this->activeExtensions = $extensionsManager->getActiveExtensions();
        $this->extensionList = ExtensionManager::loadExtensions($translator);
        $this->themesInfo = Info::loadActiveThemes();
        $this->presentersList = Loader::loadPresenters($this->extensionList, $this->themesInfo, $this->activeExtensions);
        $this->vindu = array();
        $this->settingsFacade = $settingsFacade;
        $this->registerDefaultVinduer();
        $this->initializeConnectedExtensions();
    }

    /**
     * Initializes instance of admin Anker menu structure
     * @param User $user Logged user instance
     * @param array $activeExtensions List of active extensions
     */
    public function initializeAdminMenu(User $user, array $activeExtensions) : void
    {
        $this->adminMenu = AdminMenuInitializer::initializeAdminMenu($this->themesInfo, $this->extensionList, $user, $activeExtensions);
        AdminMenuInitializer::initializeAdminMenuPostTypes($this->adminMenu, $this->appDbContext, $user);
    }

    /**
     * Returns Anker admin menu structure
     * @return AdminMenu Anker admin menu structure
     */
    public function getAdminMenu(): AdminMenu
    {
        return $this->adminMenu;
    }

    /**
     * Return instance of Anker header
     * @return AnkerHeader Instance of Anker header
     */
    public function getAnkerHeader(): AnkerHeader
    {
        return $this->ankerHeader;
    }

    /**
     * Sets Anker presenter entrance type
     * @param mixed $entranceType Anker entrance type
     */
    public function setEntranceType($entranceType) : void
    {
        $this->entranceType = $entranceType;
    }

    /**
     * Sets instance of actual handler presenter
     * @param Presenter $presenter Instance of handler presenter
     */
    public function setPresenter(Presenter $presenter) : void
    {
        $this->presenter = $presenter;
    }

    /**
     * Return instance of actual handler presenter
     * @return Presenter Instance of handler presenter
     */
    public function getPresenter(): Presenter
    {
        return $this->presenter;
    }

    /**
     * Initializes URL parameters array
     */
    public function prepareParams() : void
    {
        $params = $this->presenter->getRequest()->getParameters()["params"];
        if($params == null)
        {
            $this->params = [];
        }
        else
        {
            $this->params = explode("/", $params);
        }
    }

    /**
     * Returns Nette DI container instance
     * @return Container Nette DI container
     */
    public function getDIContainer() : Container
    {
        return $this->diContainer;
    }

    /**
     * Returns latte layout path
     * @return string Latte layout path
     */
    public function getLayout() : string
    {
        $this->layout = "";
        if($this->entranceType == EntranceType::PRIVATE)
        {
            $this->layout = FileSystem::fullPathFrom([Paths::getThemesDir(), "Backend", $this->themesInfo->getBackend(), "Templates/@layout.latte"]);
        }
        else if($this->entranceType == EntranceType::PUBLIC)
        {
            $this->layout = FileSystem::fullPathFrom([Paths::getThemesDir(), "Frontend", $this->themesInfo->getFrontend(), "Templates/@layout.latte"]);
        }
        return $this->layout;
    }

    /**
     * Returns list of connected extensions
     * @return ExtensionList List of connected extensions
     */
    public function getExtensionList(): ExtensionList
    {
        return $this->extensionList;
    }

    /**
     * Return template path
     * @return string Template path
     */
    public function getTemplate() : string
    {
        return $this->template;
    }

    /**
     * Return list of URL params
     * @return array Array of URL params
     */
    public function getParams(): array
    {
        return $this->params;
    }

    /**
     * Returns list of connected Anker presenters
     * @return PresentersList Connected Anker presenters
     */
    public function getPresentersList(): PresentersList
    {
        return $this->presentersList;
    }

    /**
     * Searches for theme template by name/path in Templates directory
     * @param string $templateName Template path/name
     */
    public function searchForTemplate(string $templateName) : void
    {
        $templatePath = TemplateFinder::findThemeTemplate(
            $this->entranceType,
            $this->themesInfo,
            $templateName
        );
        if(Text::isEmpty($templatePath)) $templatePath = TemplateFinder::findExtensionTemplate($this->extensionList, $templateName);
        if(Text::isEmpty($templatePath)) $this->searchForErrorTemplate();
        else $this->template = $templatePath;
    }

    /**
     * Searches for latte error template and sets the path
     */
    private function searchForErrorTemplate() : void
    {
        $templatePath = TemplateFinder::findThemeTemplate(
            EntranceType::PUBLIC,
            $this->themesInfo,
            self::ERROR_TEMPLATE_NAME
        );
        if(Text::isEmpty($templatePath)) {
            $templatePath = FileSystem::fullPathFrom([Paths::getAppDir(), "Core/Latte/Templates", self::ERROR_TEMPLATE_NAME_EXT]);
        }
        $this->template = $templatePath;
    }

    /**
     * Sets latte template path
     * @param string $template Template path
     */
    public function setTemplate(string $template) : void
    {
        $this->template = $template;
    }

    /**
     * Returns latte template engine
     * @return ITemplate Latte template engine
     */
    public function getTemplateEngine() : ITemplate
    {
        return $this->presenter->template;
    }

    /**
     * Initializes connected Anker extensions
     */
    private function initializeConnectedExtensions() : void
    {
        foreach ($this->extensionList->getExtensions() as $extension)
        {
            if($extension->isConnected())
            {
                if(in_array($extension->getExtensionIdentifier(), $this->activeExtensions)) {
                    $decoded = Neon::decode(file_get_contents($extension->getInfoFilePath()));
                    $service = new $decoded[Constants::ANKER_SECTION_NAME][AnkerVariables::EXTENSION_CONFIG_SECTION_CLASS]($this);
                    $extension->setObject($service);
                    $service->initialize();
                }
            }
        }
    }

    /**
     * Calls initialization method of active theme if exists
     * @param HandlerPresenter $presenter Instance of handler presenter
     */
    public function callInitializer(HandlerPresenter $presenter) : void
    {
        $backendDir = FileSystem::fullPathFrom([Paths::getThemesDir(), Constants::BACKEND_THEMES_DIR, $this->themesInfo->getBackend(), Constants::PRESENTERS_CONFIG_FILE]);
        if (!Text::isEmpty($backendDir)) {
            $neonContent = file_get_contents($backendDir);
            $neonDecoded = Neon::decode($neonContent);
            if(isset($neonDecoded[Constants::ANKER_SECTION_NAME]))
            {
                $neonDecoded = $neonDecoded[Constants::ANKER_SECTION_NAME];
                if(isset($neonDecoded[Constants::ANKER_INITIALIZER_SECTION_NAME]))
                {
                    $neonDecoded = $neonDecoded[Constants::ANKER_INITIALIZER_SECTION_NAME];
                    try {
                        $rc = new \ReflectionClass($neonDecoded);
                        if($rc->implementsInterface(IInitializer::class))
                        {
                            $neonDecoded::initialize($presenter);
                        }
                    } catch (\ReflectionException $e) {
                    }
                }
            }
        }
    }

    /**
     * Returns translator instance
     * @return Translator Translator instance
     */
    public function getTranslator() : Translator
    {
        return $this->translator;
    }

    /**
     * Returns themes info
     * @return Info Themes info
     */
    public function getThemesInfo() : Info
    {
        return $this->themesInfo;
    }

    /**
     * Registers Vindu into Anker Core
     * @param string $identifier Vindu widget identifier
     * @param IAnkerVindu $vindu Vindu widget instance
     * @return bool If registration was successful
     */
    public function registerVindu(string $identifier, IAnkerVindu $vindu) : bool
    {
        if(isset($this->vindu[$identifier])) return false;
        $checkResult = true;
        try {
            $classReflection = new \ReflectionClass(get_class($vindu));
            $checkResult = $checkResult && $classReflection->isInstantiable();
            $checkResult = $checkResult && $classReflection->implementsInterface(IAnkerVindu::class);
            unset($classReflection);
        } catch (\ReflectionException $e) {
            return false;
        }
        if(!$checkResult) return false;
        $this->vindu[$identifier] = $vindu;
        return true;
    }

    /**
     * Returns list of registered Vindu widgets
     * @return array List of registered Vindu widgets
     */
    public function getVinduArray() : array
    {
        return $this->vindu;
    }

    /**
     * Return Vindu widget instance by identifier
     * @param string $identifier Vindu identifier
     * @return mixed|null Vindu instance
     */
    public function getVinduByIdentifier(string $identifier)
    {
        if(!isset($this->vindu[$identifier])) return null;
        return $this->vindu[$identifier];
    }

    /**
     * Registers default Anker Vindu widgets
     */
    private function registerDefaultVinduer() : void
    {
        $translateSwitcherVindu = $this->diContainer->getByType(TranslateSwitcherVindu::class);
        $this->registerVindu('ankerTranslateSwitcher', $translateSwitcherVindu);
        $menuRendererVindu = $this->diContainer->getByType(MenuRendererVindu::class);
        $this->registerVindu('ankerMenuRenderer', $menuRendererVindu);
        $broVindu = $this->diContainer->getByType(BroVindu::class);
        $this->registerVindu('ankerBro', $broVindu);
        $pagerVindu = $this->diContainer->getByType(PagerVindu::class);
        $this->registerVindu('ankerPager', $pagerVindu);
    }

    /**
     * Returns list of active extensions
     * @return array List of active extensions
     */
    public function getActiveExtensions() : array
    {
        return $this->activeExtensions;
    }

    /**
     * Initializes Anker header structure
     * @param SettingsFacade $settingsFacade Settings facade
     */
    private function initializeAnkerHeader(SettingsFacade $settingsFacade)
    {
        $this->ankerHeader->setIcon("/".$settingsFacade->getValueByIdentifier("icon"));
    }

    /**
     * Returns instance of settings facade
     * @return SettingsFacade Instance of settings facade
     */
    public function getSettingsFacade(): SettingsFacade
    {
        return $this->settingsFacade;
    }
}
