<?php

declare(strict_types=1);

namespace Anker\Common;

/**
 * Class Constants represents second part of Anker constants
 */
class AnkerVariables
{

    public const EXTENSION_CONFIG = "config.neon";

    public const EXTENSION_INFO = "info.neon";

    public const EXTENSION_CONFIG_SECTION_NAME = "anker";

    public const EXTENSION_CONFIG_SECTION_CLASS = "class";

    public const EXTENSION_CONFIG_SECTION_IDENTIFIER = "identifier";

}
