<?php

namespace Anker\Latte;

use Kdyby\Translation\Translator;
use Nette\Bridges\ApplicationLatte\Template;
use Nette\DI\Container;

/**
 * Class AnkerVinduBase represents minimal structure of Anker Vindu widget
 */
abstract class AnkerVinduBase implements IAnkerVindu
{

    /**
     * @var Container Instance of DI container
     */
    protected $container;

    /**
     * @var Template Instance of Latte template
     */
    protected $template;

    /**
     * @var Translator Instance of Nette translator
     */
    protected $translator;

    public function __construct(Container $container, Template $template, Translator $translator)
    {
        $this->container = $container;
        $this->template = $template;
        $this->translator = $translator;
        $this->template->setTranslator($translator);
    }

    /**
     * Generates and compiles Latte template of Vindu with params
     * @param array $args Vindu params
     * @param string $content Vindu content
     * @return string Compiled Latte template of Vindu
     */
    public abstract function renderVindu(array $args, string $content): string;

}
