<?php

namespace Anker\Latte;

/**
 * Interface IAnkerVindu represents structure of basic Vindu functions
 */
interface IAnkerVindu
{

    /**
     * Generates and compiles Latte template of Vindu with params
     * @param array $args Vindu params
     * @param string $content Vindu content
     * @return string Compiled Latte template of Vindu
     */
    public function renderVindu(array $args, string $content) : string;

}
