<?php

namespace Anker\Latte;

use Latte;
use Latte\MacroNode;
use Latte\Macros\MacroSet;
use Latte\PhpWriter;

/**
 * Class AnkerVindu represents Latte Macro set for Latte template engine
 */
class AnkerVindu extends MacroSet
{

    /**
     * Function registers macro into engine
     * @param Latte\Compiler $compiler Instance of latte compiler
     */
    public static function install(Latte\Compiler $compiler)
    {
        $set = new static($compiler);
        $set->addMacro('vindu', null, [$set, 'renderVindu']);
    }

    /**
     * Generates PHP code for requested Vindu widget in Latte template
     * @param MacroNode $node Macro node
     * @param PhpWriter $writer Php writer
     * @return string Generated PHP code for Vindu widget
     * @throws Latte\CompileException Compiler exception
     */
    public function renderVindu(MacroNode $node, PhpWriter $writer)
    {
        $content = $node->content;
        $node->content = "";
        return $writer->write('echo $_presenter->processVindu(%node.array,"'.$content.'")');
    }

}
