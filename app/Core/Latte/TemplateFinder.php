<?php

namespace Anker\Latte;

use Anker\Common\Enums\EntranceType;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\Extensions\Enums\ExtensionStatus;
use Anker\Extensions\ExtensionList;
use Anker\Themes\Info;

/**
 * Class TemplateFinder represents tool for template finding
 */
class TemplateFinder
{

    /**
     * Finds latte template and return real path
     * @param string $entranceType Entrance type
     * @param Info $themes Instance of active themes
     * @param string $templateName Latte template name
     * @return string Path of latte template
     */
    public static function findThemeTemplate(string $entranceType, Info $themes, string $templateName) : string
    {
        $themePath = "";
        if($entranceType == EntranceType::PRIVATE)
        {
            $themePath = FileSystem::fullPathFrom([
                Paths::getThemesDir(),
                "Backend",
                $themes->getBackend(),
                "Templates",
                $templateName . ".latte"
            ]);
        }
        else if($entranceType == EntranceType::PUBLIC)
        {
            $themePath = FileSystem::fullPathFrom([
                Paths::getThemesDir(),
                "Frontend",
                $themes->getFrontend(),
                "Templates",
                $templateName . ".latte"
            ]);
        }
        return $themePath;
    }

    public static function findExtensionTemplate(ExtensionList $extensionList, string $templateName) : string
    {
        $themePath = "";

        foreach ($extensionList->getExtensions() as $extension)
        {
            if($extension->getExtensionStatus() == ExtensionStatus::CONNECTED)
            {
                $themePath = FileSystem::fullPathFrom([
                    Paths::getExtensionsDir(),
                    $extension->getExtensionName(),
                    "Templates",
                    $templateName . ".latte"
                ]);
                if(!Text::isEmpty($themePath)) break;
            }
        }

        return $themePath;
    }

}
