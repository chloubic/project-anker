<?php


namespace Anker\Latte;

use Anker\Common\Utils\Text;
use Anker\Presenters\Handlers\HandlerPresenter;

/**
 * Class AnkerVinduParser represents Vindu widget parser
 */
class AnkerVinduParser
{

    /**
     * REGEX for Vindu parsing from text
     */
    private const REGEX = '/{\s*vindu(.*?)}(.*?){\s*\/\s*vindu}/m';

    /**
     * Processes Vindu requests from template parameters
     * @param HandlerPresenter $presenter Instance of actual handler presenter
     */
    public static function process(HandlerPresenter $presenter) : void
    {
        foreach ($presenter->getTemplate()->getParameters() as $key => $value)
        {
            if(is_string($value)) {
                $presenter->getTemplate()->{$key} = self::parse($presenter, $value);
            }
        }
    }

    /**
     * Parses Vindu requests from any entered string
     * @param HandlerPresenter $presenter Instance of actual handler presenter
     * @param string $value String value
     * @return string Compiled Latte template of Vindu
     */
    public static function parse(HandlerPresenter $presenter, string $value) : string
    {
        $array = [];
        preg_match_all(self::REGEX, $value, $array, PREG_SET_ORDER, 0);
        if(count($array) > 0) {
            foreach ($array as $vindu)
            {
                $from = '/'.preg_quote($vindu[0], '/').'/';
                $value = preg_replace(
                    $from,
                    $presenter->processVindu(
                        self::extractArguments($vindu[1]),
                        $vindu[2]
                    ),
                    $value,
                    1
                );
            }
        }
        return $value;
    }

    /**
     * Extracts Vindu parameters from string
     * @param string $unparsed Unparsed string of params
     * @return array Array of Vindu params
     */
    private static function extractArguments(string $unparsed) : array
    {
        $args = [];

        $unparsed = trim($unparsed);
        $unparsed = explode(',', $unparsed);

        foreach ($unparsed as $param)
        {
            $count = 0;
            $param = trim($param);
            $param = explode('=>', $param);

            $key = "";
            $value = "";

            foreach ($param as $exploded)
            {
                $exploded = trim($exploded);
                $exploded = trim($exploded, "'\"");
                if($count == 0) $key = $exploded;
                else if($count == 1) $value = $exploded;
                $count++;
            }

            if(!Text::isEmpty($key)) $args[$key] = $value;

        }

        return $args;
    }

}
