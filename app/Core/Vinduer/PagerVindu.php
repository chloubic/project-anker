<?php

namespace Anker\Vinduer;

use Anker\Latte\AnkerVinduBase;

/**
 * Class PagerVindu represents logic of Pager Vindu Widget
 */
class PagerVindu extends AnkerVinduBase
{

    /**
     * Method renders Pager Vindu widget
     */
    public function renderVindu(array $args, string $content) : string
    {
        if(isset($args["total"]) && isset($args["page"]) && isset($args["limit"]) && isset($args["prev"]) && isset($args["next"]) && isset($args["icon"])) {
            return $this->template->renderToString(__DIR__ . '/Templates/pager.latte',
                [
                    "total" => $args["total"],
                    "page" => $args["page"],
                    "limit" => $args["limit"],
                    "prev" => $args["prev"],
                    "next" => $args["next"],
                    "icon" => $args["icon"]
                ]
            );
        }
        return "";
    }
}
