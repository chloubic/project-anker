<?php

namespace Anker\Vinduer;

use Anker\Latte\AnkerVinduBase;
use Tracy\Debugger;

/**
 * Class TranslateSwitcherVindu represents logic of Translate Switcher Vindu Widget
 */
class TranslateSwitcherVindu extends AnkerVinduBase
{

    /**
     * Method renders Translate Switcher Vindu widget
     */
    public function renderVindu(array $args, string $content) : string
    {
        $lang = "";
        if(isset($_COOKIE["lang"])) $lang = $_COOKIE["lang"];
        return $this->template->renderToString(__DIR__ . '/Templates/translateSwitcher.latte',
            [
                "langs" => $this->translator->getAvailableLocales(),
                "selectedLang" => $lang
            ]
        );
    }
}
