<?php


namespace Anker\Vinduer;


use Anker\Common\AnkerWrapper;
use Anker\Latte\AnkerVinduBase;
use Tracy\Debugger;

/**
 * Class BroVindu represents logic of Bro Vindu Widget
 */
class BroVindu extends AnkerVinduBase
{

    /**
     * Method renders Bro Vindu widget
     */
    public function renderVindu(array $args, string $content): string
    {
        $ankerWrapper = $this->container->getByType(AnkerWrapper::class);
        return $this->template->renderToString(__DIR__ . '/Templates/bro.latte', [
            "logged" => $ankerWrapper->getPresenter()->getUser()->isLoggedIn()
        ]);
    }
}
