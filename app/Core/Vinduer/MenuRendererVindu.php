<?php


namespace Anker\Vinduer;


use Anker\BL\Facades\MenuFacade;
use Anker\BL\Managers\MenuManager;
use Anker\Latte\AnkerVinduBase;
use Tracy\Debugger;

/**
 * Class MenuRendererVindu represents logic of Menu Renderer Vindu Widget
 */
class MenuRendererVindu extends AnkerVinduBase
{

    /**
     * Method renders Menu Renderer Vindu widget
     */
    public function renderVindu(array $args, string $content) : string
    {
        $menuManager = $this->container->getByType(MenuManager::class);
        $menuFacade = $this->container->getByType(MenuFacade::class);
        if(isset($args["menu"]))
        {
            $menu = $menuFacade->getByIdentifier($args["menu"]);
            if($menu != null)
            {
                $icon = "";
                if(isset($args["icon"])) $icon = $args["icon"];
                return $this->template->renderToString(__DIR__ . '/Templates/menuRenderer.latte', [
                    "menu" => $menuManager->renderMenu($menu->getItems(), $icon)
                ]);
            }
        }
        return "";
    }
}
