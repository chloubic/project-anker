<?php


namespace Anker\Forms;

use Anker\Forms\Components\UIButtonControl;
use Anker\Forms\Components\WysiwygControl;
use Nette\Forms\Container;
use Nette\Http\Url;
use Nette;
use Nette\Application\UI;
use Nette\Forms\Validator;

/**
 * Class Form represents Anker version of Nette Forms
 */
class Form extends UI\Form
{

    public function __construct(Nette\ComponentModel\IContainer $parent = null, $name = null)
    {
        parent::__construct($parent, $name);
        // Corrects form URL for Anker CMS
        $url = new Url("http://$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
        $this->setAction($url->path);
        $this->initializePredefinedInputs();
    }

    /**
     * Function initializes predefined Anker form components for Nette Forms
     */
    protected function initializePredefinedInputs() : void
    {
        // Adds support for WYSIWYG control
        Container::extensionMethod(
            'addWYSIWYG',
            function(Container $container, $name, $title = 'WYSIWYG field') {
                return $container[$name] = new WysiwygControl($title);
            }
        );

        // Adds support for button control with icon
        Container::extensionMethod(
            'addUIButton',
            function(Container $container, $name, $title = 'UI Button') {
                return $container[$name] = new UIButtonControl($title);
            }
        );
    }

    /**
     * Overridden function that corrects bug in Nette Forms
     */
    public function validateMaxPostSize()
    {
        if (!$this->isSubmitted() || !$this->isMethod('post') || empty($_SERVER['CONTENT_LENGTH'])) {
            return;
        }
        $maxSize = ini_get('post_max_size');
        $units = ['k' => 10, 'm' => 20, 'g' => 30];
        if (isset($units[$ch = strtolower(substr($maxSize, -1))])) {
            $maxSize = substr($maxSize, 0, strlen($maxSize) - 1);
            $maxSize <<= $units[$ch];
        }
        if ($maxSize > 0 && $maxSize < $_SERVER['CONTENT_LENGTH']) {
            $this->addError(sprintf(Validator::$messages[self::MAX_FILE_SIZE], $maxSize));
        }
    }

}
