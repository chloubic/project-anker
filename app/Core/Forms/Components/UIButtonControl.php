<?php

namespace Anker\Forms\Components;

use Nette\Forms\Controls\BaseControl;
use Nette\Forms\IControl;
use Nette\Utils\Html;

/**
 * Class UIButtonControl represents button control with image icon
 */
class UIButtonControl extends BaseControl implements IControl
{

    /**
     * @var string Component label
     */
    protected $title;

    /**
     * @var mixed Component options
     */
    protected $options;

    /**
     * @var string Component image icon URL
     */
    protected $icon;

    public function __construct($caption = NULL)
    {
        parent::__construct();
        $this->control = Html::el('button');
        $this->title = $caption;
        $this->caption = null;
        $this->icon = null;
    }

    /**
     * @param string $url URL of image icon
     * @return IControl Self reference
     */
    public function setIcon(string $url) : IControl
    {
        $this->icon = $url;
        return $this;
    }

    /**
     * Generates control's HTML element.
     * @return Html|string
     */
    public function getControl()
    {
        $this->setOption('rendered', TRUE);
        $el = clone $this->control;
        if($this->icon != null)
        {
            $icon = Html::el('img')
                ->setAttribute('src', $this->icon);
            $el->setHtml($icon)
                ->addText($this->title);
        }
        return $el;
    }
}
