<?php

namespace Anker\Forms\Components;

use Nette\Forms\Controls\BaseControl;
use Nette\Forms\Helpers;
use Nette\Forms\IControl;
use Nette\Utils\Html;

/**
 * Class WysiwygControl represents WYSIWYG editor control
 */
class WysiwygControl extends BaseControl implements IControl
{

    /**
     * @var string Component editor value
     */
    protected $value;

    /**
     * @var mixed Component options
     */
    protected $options;

    public function __construct($caption = NULL)
    {
        parent::__construct();
        $this->control = Html::el('div');
        $this->control->addHtml(Html::el('input', ['type' => NULL, 'name' => NULL]));
        $this->control->addHtml(Html::el('div', ['class' => 'wysiwyg']));
        $this->caption = $caption;
    }

    /**
     * Generates control's HTML element.
     * @return Html|string
     */
    public function getControl()
    {
        $this->setOption('rendered', TRUE);
        $el = clone $this->control;
        $el->getChildren()[0]->addAttributes([
            'name' => $this->getHtmlName(),
            'id' => $this->getHtmlId(),
            'required' => $this->isRequired(),
            'type' => 'hidden',
            'disabled' => $this->isDisabled(),
            'data-nette-rules' => Helpers::exportRules($this->getRules()) ?: NULL,
        ]);
        $el->getChildren()[1]->setId($this->getHtmlName());
        $el->addHtml('<script>
            const ' . $this->getHtmlName() . ' = new Quill(\'#' . $this->getHtmlName() . '\', {
            modules: {
                imageResize: {
                    displaySize: true
                },
                toolbar: (typeof toolbarOptions !== \'undefined\' ? toolbarOptions : [])
            },
            theme: \'snow\'
            });
            ' . $this->getHtmlName() . '.root.innerHTML = `' . addslashes($this->getValue()) . '`;
            ' . $this->getHtmlName() . '.on(\'text-change\', function() {
                $("input[name=\'' . $this->getHtmlName() . '\']").val(' . $this->getHtmlName() . '.root.innerHTML);
            });
        </script>');
        return $el;
    }
}
