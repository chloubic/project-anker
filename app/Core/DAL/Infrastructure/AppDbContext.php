<?php

namespace Anker\DAL\Infrastructure;

use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

/**
 * Class AppDbContext represents object of database UoW context
 */
class AppDbContext
{

    /**
     * @var EntityManager Instance of entity manager
     */
    private $em;

    public function __construct(EntityManager $em)
    {
        $this->em = $em;
    }

    public function getRepository(string $class) : EntityRepository
    {
        return $this->em->getRepository($class);
    }

    public function saveChanges($object) : void
    {
        $this->em->persist($object);
    }

    public function commitChanges() : void
    {
        $this->em->flush();
    }

    public function getEntityManager() : EntityManager
    {
        return $this->em;
    }

    public function removeEntity($object) : void
    {
        $this->em->remove($object);
    }

}
