<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_media")
 */
class Media
{
    use Identifier;

    /**
     * Many audits have one user. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="medias")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $addedDateTimeGMT;

    /**
     * @ORM\Column(type="string")
     */
    protected $path;

    /**
     * One user has many medias. This is the inverse side.
     * @ORM\OneToMany(targetEntity="MediaMeta", mappedBy="media")
     */
    protected $metas;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getAddedDateTimeGMT()
    {
        return $this->addedDateTimeGMT;
    }

    /**
     * @param mixed $addedDateTimeGMT
     */
    public function setAddedDateTimeGMT($addedDateTimeGMT): void
    {
        $this->addedDateTimeGMT = $addedDateTimeGMT;
    }

    /**
     * @return mixed
     */
    public function getPath()
    {
        return $this->path;
    }

    /**
     * @param mixed $path
     */
    public function setPath($path): void
    {
        $this->path = $path;
    }
}