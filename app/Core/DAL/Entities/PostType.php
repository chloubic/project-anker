<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_posttypes")
 */
class PostType
{

    use Identifier;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $slug;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    protected $factories;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    protected $permissions;

    /**
     * One user has many posts. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Post", mappedBy="type")
     */
    protected $posts;

    public function __construct(string $slug, array $factories, array $permissions = [])
    {
        $this->slug = $slug;
        $this->factories = $factories;
        $this->permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getFactories()
    {
        return $this->factories;
    }

    /**
     * @param mixed $factories
     */
    public function setFactories($factories): void
    {
        $this->factories = $factories;
    }

    /**
     * @return mixed
     */
    public function getPermissions()
    {
        return $this->permissions;
    }

    /**
     * @param mixed $permissions
     */
    public function setPermissions($permissions): void
    {
        $this->permissions = $permissions;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

}