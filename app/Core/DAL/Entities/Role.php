<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_roles")
 */
class Role
{

    use Identifier;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    protected $allowed;

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getAllowed()
    {
        return $this->allowed;
    }

    /**
     * @param mixed $allowed
     */
    public function setAllowed($allowed): void
    {
        $this->allowed = $allowed;
    }

}