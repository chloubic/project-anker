<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_audit")
 */
class Audit
{

    use Identifier;

    /**
     * Many audits have one user. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="audits")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $user;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $dateTimeGMT;

    /**
     * @ORM\Column(type="string")
     */
    protected $agent;

    /**
     * @ORM\Column(type="string")
     */
    protected $ip;

    /**
     * @ORM\Column(type="string")
     */
    protected $place;

    /**
     * One user has many audits. This is the inverse side.
     * @ORM\OneToMany(targetEntity="AuditMeta", mappedBy="audit")
     */
    protected $metas;

    /**
     * @return mixed
     */
    public function getUser()
    {
        return $this->user;
    }

    /**
     * @param mixed $user
     */
    public function setUser($user): void
    {
        $this->user = $user;
    }

    /**
     * @return mixed
     */
    public function getDateTimeGMT()
    {
        return $this->dateTimeGMT;
    }

    /**
     * @param mixed $dateTimeGMT
     */
    public function setDateTimeGMT($dateTimeGMT): void
    {
        $this->dateTimeGMT = $dateTimeGMT;
    }

    /**
     * @return mixed
     */
    public function getAgent()
    {
        return $this->agent;
    }

    /**
     * @param mixed $agent
     */
    public function setAgent($agent): void
    {
        $this->agent = $agent;
    }

    /**
     * @return mixed
     */
    public function getIp()
    {
        return $this->ip;
    }

    /**
     * @param mixed $ip
     */
    public function setIp($ip): void
    {
        $this->ip = $ip;
    }

    /**
     * @return mixed
     */
    public function getPlace()
    {
        return $this->place;
    }

    /**
     * @param mixed $place
     */
    public function setPlace($place): void
    {
        $this->place = $place;
    }

}