<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_permissions")
 */
class Permission
{

    use Identifier;

    /**
     * @ORM\Column(type="string", unique=true, nullable=false)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $resource;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $privilege;

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getResource()
    {
        return $this->resource;
    }

    /**
     * @param mixed $resource
     */
    public function setResource($resource): void
    {
        $this->resource = $resource;
    }

    /**
     * @return mixed
     */
    public function getPrivilege()
    {
        return $this->privilege;
    }

    /**
     * @param mixed $privilege
     */
    public function setPrivilege($privilege): void
    {
        $this->privilege = $privilege;
    }

}