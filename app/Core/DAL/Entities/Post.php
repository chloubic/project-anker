<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_posts")
 */
class Post
{

    use Identifier;

    /**
     * Many posts have one user. This is the owning side.
     * @ORM\ManyToOne(targetEntity="User", inversedBy="posts")
     * @ORM\JoinColumn(name="user_id", referencedColumnName="id")
     */
    protected $author;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $slug;

    /**
     * @ORM\Column(type="json", nullable=false)
     */
    protected $fields;

    /**
     * @ORM\Column(type="smallint", nullable=false)
     */
    protected $status;

    /**
     * Many posts have one user. This is the owning side.
     * @ORM\ManyToOne(targetEntity="PostType", inversedBy="posts")
     * @ORM\JoinColumn(name="posttype_id", referencedColumnName="id")
     */
    protected $type;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $createdDateTimeGMT;

    /**
     * @ORM\Column(type="datetime", nullable=false)
     */
    protected $modifiedDateTimeGMT;

    /**
     * One post has many metas. This is the inverse side.
     * @ORM\OneToMany(targetEntity="PostMeta", mappedBy="post")
     */
    protected $metas;

    /**
     * @return mixed
     */
    public function getAuthor()
    {
        return $this->author;
    }

    /**
     * @param mixed $author
     */
    public function setAuthor($author): void
    {
        $this->author = $author;
    }

    /**
     * @return mixed
     */
    public function getSlug()
    {
        return $this->slug;
    }

    /**
     * @param mixed $slug
     */
    public function setSlug($slug): void
    {
        $this->slug = $slug;
    }

    /**
     * @return mixed
     */
    public function getFields()
    {
        return $this->fields;
    }

    /**
     * @param mixed $fields
     */
    public function setFields($fields): void
    {
        $this->fields = $fields;
    }

    /**
     * @return mixed
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * @param mixed $status
     */
    public function setStatus($status): void
    {
        $this->status = $status;
    }

    /**
     * @return mixed
     */
    public function getType()
    {
        return $this->type;
    }

    /**
     * @param mixed $type
     */
    public function setType($type): void
    {
        $this->type = $type;
    }

    /**
     * @return mixed
     */
    public function getCreatedDateTimeGMT()
    {
        return $this->createdDateTimeGMT;
    }

    /**
     * @param mixed $createdDateTimeGMT
     */
    public function setCreatedDateTimeGMT($createdDateTimeGMT): void
    {
        $this->createdDateTimeGMT = $createdDateTimeGMT;
    }

    /**
     * @return mixed
     */
    public function getModifiedDateTimeGMT()
    {
        return $this->modifiedDateTimeGMT;
    }

    /**
     * @param mixed $modifiedDateTimeGMT
     */
    public function setModifiedDateTimeGMT($modifiedDateTimeGMT): void
    {
        $this->modifiedDateTimeGMT = $modifiedDateTimeGMT;
    }

    /**
     * @return mixed
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * @param mixed $metas
     */
    public function setMetas($metas): void
    {
        $this->metas = $metas;
    }

}