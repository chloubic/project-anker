<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Doctrine\Common\Collections\ArrayCollection;
use Kdyby\Doctrine\Entities\Attributes\Identifier;
use Nette\Utils\DateTime;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_users")
 */
class User
{

    const STATUS_ACTIVE = "active";
    const STATUS_INACTIVE = "inactive";
    const STATUS_BANNED = "banned";

    use Identifier;

    /**
     * @ORM\Column(type="string")
     */
    protected $username;

    /**
     * @ORM\Column(type="string")
     */
    protected $password;

    /**
     * @ORM\Column(type="string")
     */
    protected $name;

    /**
     * @ORM\Column(type="string")
     */
    protected $normalizedName;

    /**
     * @ORM\Column(type="string")
     */
    protected $email;

    /**
     * @ORM\Column(type="string")
     */
    protected $activationKey;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $activationKeyValidGMT;

    /**
     * @ORM\Column(type="string")
     */
    protected $recoveryKey;

    /**
     * @ORM\Column(type="datetime")
     */
    protected $recoveryKeyValidGMT;

    /**
     * One user has many metas. This is the inverse side.
     * @ORM\OneToMany(targetEntity="UserMeta", mappedBy="user")
     */
    protected $metas;

    /**
     * One user has many medias. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Media", mappedBy="user")
     */
    protected $medias;

    /**
     * One user has many posts. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Post", mappedBy="author")
     */
    protected $posts;

    /**
     * One user has many audits. This is the inverse side.
     * @ORM\OneToMany(targetEntity="Audit", mappedBy="user")
     */
    protected $audits;

    /** @ORM\Column(type="string") */
    protected $status;

    public function __construct()
    {
        $this->activationKey = "";
        $this->activationKeyValidGMT = DateTime::from(date(""));
        $this->recoveryKey = "";
        $this->recoveryKeyValidGMT = DateTime::from(date(""));
        $this->status = 0;
        $this->metas = new ArrayCollection();
    }

    /**
     * @return mixed
     */
    public function getUsername() : string
    {
        return $this->username;
    }

    /**
     * @param mixed $username
     */
    public function setUsername($username): void
    {
        $this->username = $username;
    }

    /**
     * @return mixed
     */
    public function getPassword() : string
    {
        return $this->password;
    }

    /**
     * @param mixed $password
     */
    public function setPassword($password): void
    {
        $this->password = $password;
    }

    /**
     * @return mixed
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param mixed $name
     */
    public function setName($name): void
    {
        $this->name = $name;
    }

    /**
     * @return mixed
     */
    public function getNormalizedName() : string
    {
        return $this->normalizedName;
    }

    /**
     * @param mixed $normalizedName
     */
    public function setNormalizedName($normalizedName): void
    {
        $this->normalizedName = $normalizedName;
    }

    /**
     * @return mixed
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param mixed $email
     */
    public function setEmail($email): void
    {
        $this->email = $email;
    }

    /**
     * @return mixed
     */
    public function getActivationKey() : string
    {
        return $this->activationKey;
    }

    /**
     * @param mixed $activationKey
     */
    public function setActivationKey($activationKey): void
    {
        $this->activationKey = $activationKey;
    }

    /**
     * @return mixed
     */
    public function getActivationKeyValidGMT()
    {
        return $this->activationKeyValidGMT;
    }

    /**
     * @param mixed $activationKeyValidGMT
     */
    public function setActivationKeyValidGMT($activationKeyValidGMT): void
    {
        $this->activationKeyValidGMT = $activationKeyValidGMT;
    }

    /**
     * @return mixed
     */
    public function getRecoveryKey() : string
    {
        return $this->recoveryKey;
    }

    /**
     * @param mixed $recoveryKey
     */
    public function setRecoveryKey($recoveryKey): void
    {
        $this->recoveryKey = $recoveryKey;
    }

    /**
     * @return mixed
     */
    public function getRecoveryKeyValidGMT()
    {
        return $this->recoveryKeyValidGMT;
    }

    /**
     * @param mixed $recoveryKeyValidGMT
     */
    public function setRecoveryKeyValidGMT($recoveryKeyValidGMT): void
    {
        $this->recoveryKeyValidGMT = $recoveryKeyValidGMT;
    }

    /**
     * @return mixed
     */
    public function getMetas()
    {
        return $this->metas;
    }

    /**
     * @param mixed $metas
     */
    public function setMetas($metas): void
    {
        $this->metas = $metas;
    }

    /**
     * @return mixed
     */
    public function getMedias()
    {
        return $this->medias;
    }

    /**
     * @param mixed $medias
     */
    public function setMedias($medias): void
    {
        $this->medias = $medias;
    }

    /**
     * @return mixed
     */
    public function getPosts()
    {
        return $this->posts;
    }

    /**
     * @param mixed $posts
     */
    public function setPosts($posts): void
    {
        $this->posts = $posts;
    }

    /**
     * @return mixed
     */
    public function getAudits()
    {
        return $this->audits;
    }

    /**
     * @param mixed $audits
     */
    public function setAudits($audits): void
    {
        $this->audits = $audits;
    }

    public function getStatus() : string
    {
        return $this->status;
    }

    public function setStatus($status) : void
    {
        if (!in_array($status, array(self::STATUS_ACTIVE, self::STATUS_INACTIVE, self::STATUS_BANNED))) {
            throw new \InvalidArgumentException("Invalid user status");
        }
        $this->status = $status;
    }

}