<?php


namespace Anker\DAL\Entities;

use Doctrine\ORM\Mapping as ORM;
use Kdyby\Doctrine\Entities\Attributes\Identifier;

/**
 * @ORM\Entity
 * @ORM\Table(name="anker_auditmeta")
 */
class AuditMeta
{
    use Identifier;

    /**
     * Many metas have one user. This is the owning side.
     * @ORM\ManyToOne(targetEntity="Audit", inversedBy="metas")
     * @ORM\JoinColumn(name="audit_id", referencedColumnName="id")
     */
    protected $audit;

    /**
     * @ORM\Column(type="string", nullable=false)
     */
    protected $identifier;

    /**
     * @ORM\Column(type="string")
     */
    protected $data;

    /**
     * @return mixed
     */
    public function getAudit()
    {
        return $this->audit;
    }

    /**
     * @param mixed $audit
     */
    public function setAudit($audit): void
    {
        $this->audit = $audit;
    }

    /**
     * @return mixed
     */
    public function getIdentifier()
    {
        return $this->identifier;
    }

    /**
     * @param mixed $identifier
     */
    public function setIdentifier($identifier): void
    {
        $this->identifier = $identifier;
    }

    /**
     * @return mixed
     */
    public function getData()
    {
        return $this->data;
    }

    /**
     * @param mixed $data
     */
    public function setData($data): void
    {
        $this->data = $data;
    }

}