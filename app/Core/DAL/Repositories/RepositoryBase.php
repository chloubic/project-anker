<?php

namespace Anker\DAL\Repositories;

use Anker\DAL\Infrastructure\AppDbContext;
use Kdyby\Doctrine\EntityManager;
use Kdyby\Doctrine\EntityRepository;

/**
 * Class RepositoryBase represents parent object of all repositories
 */
abstract class RepositoryBase
{

    private $appDbContext;

    protected $class;

    public function __construct(AppDbContext $appDbContext, string $class)
    {
        $this->appDbContext = $appDbContext;
        $this->class = $class;
    }

    protected function getEntityRepository() : EntityRepository
    {
        return $this->appDbContext->getRepository($this->class);
    }

    protected function getEM() : EntityManager
    {
        return $this->appDbContext->getEntityManager();
    }

    protected function getRepositoryByEntity(string $class) : EntityRepository
    {
        return $this->appDbContext->getRepository($class);
    }

    protected function saveChanges($object) : void
    {
        $this->appDbContext->saveChanges($object);
    }

    protected function removeEntity($object) : void
    {
        $this->appDbContext->removeEntity($object);
    }

    protected function commitChanges() : void
    {
        $this->appDbContext->commitChanges();
    }

}
