<?php

namespace Anker\DAL\Repositories;

use Anker\Common\Utils\Paths;
use Anker\DAL\Entities\Media;
use Anker\DAL\Entities\User;
use Anker\DAL\Infrastructure\AppDbContext;
use Nette\Utils\DateTime;

class MediaRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Media::class);
    }

    public function insertMedias(\Nette\Security\User $user, array $files) : void
    {
        $userRepository = $this->getRepositoryByEntity(User::class);
        $userEntity = $userRepository->findOneBy(["id" => $user->getId()]);

        foreach ($files as $file)
        {
            $media = new Media;

            $media->setUser($userEntity);
            $media->setPath(str_replace(Paths::getWWWDir(), "", $file["path"]));
            $media->setAddedDateTimeGMT(DateTime::from($file["dateTime"]));

            $this->saveChanges($media);
        }

        $this->commitChanges();
    }

    public function getAllMedia(int $offset, int $limit) : array
    {
        return $this->getEntityRepository()
            ->findBy([], ["addedDateTimeGMT" => "DESC"], $limit, $offset);
    }

    public function getById(int $id)
    {
        return $this->getEntityRepository()->findOneBy(["id" => $id]);
    }

    public function removeById(int $id) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        $this->removeEntity($entity);
        $this->commitChanges();
    }

    public function getTotalCount() : int
    {
        return $this->getEntityRepository()->count([]);
    }

}
