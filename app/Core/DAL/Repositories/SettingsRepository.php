<?php


namespace Anker\DAL\Repositories;

use Anker\DAL\Infrastructure\AppDbContext;
use Kdyby\Doctrine\EntityManager;
use Anker\DAL\Entities\Settings;
use Tracy\Debugger;

class SettingsRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Settings::class);
    }

    public function all()
    {
        return $this->getEntityRepository()->findAll();
    }

    public function findSingleValueByIdentifier(string $identifier) : string
    {
        $entity = $this->getEntityRepository()->findOneBy(["identifier" => $identifier]);
        return $entity ? $entity->getData() : "";
    }

    public function registrationEnabled() : bool
    {
        return $this->findSingleValueByIdentifier("registration_enabled") == 1;
    }

    public function setOrCreate(string $identifier, string $value) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["identifier" => $identifier]);
        if($entity == null)
        {
            $entity = new Settings;
            $entity->setIdentifier($identifier);
            $entity->setData($value);
        }
        else
        {
            $entity->setData($value);
        }
        $this->saveChanges($entity);
        $this->commitChanges();
    }

    public function setNewIcon(string $fileName) : string
    {
        $entity = $this->getEntityRepository()->findOneBy(["identifier" => "icon"]);
        if($entity != null)
        {
            $oldName = $entity->getData();
            $entity->setData($fileName);
            $this->saveChanges($entity);
            $this->commitChanges();
            return $oldName;
        }
        else
        {
            $entity = new Settings;
            $entity->setData($fileName);
            $entity->setIdentifier("icon");
            $this->saveChanges($entity);
            $this->commitChanges();
            return "";
        }
    }

}