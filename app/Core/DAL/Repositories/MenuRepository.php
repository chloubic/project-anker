<?php

namespace Anker\DAL\Repositories;

use Anker\DAL\Entities\Menu;
use Anker\DAL\Infrastructure\AppDbContext;

class MenuRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Menu::class);
    }

    public function delete(int $id)
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $this->removeEntity($entity);
            $this->commitChanges();
        }
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->getEntityRepository()->findBy([], [], $limit, $offset);
    }

    public function create(string $identifier, array $structure) : void
    {
        $entity = new Menu;
        $entity->setIdentifier($identifier);
        $entity->setItems($structure);

        $this->saveChanges($entity);
        $this->commitChanges();
    }

    public function getById(int $id)
    {
        return $this->getEntityRepository()->findOneBy(["id" => $id]);
    }

    public function edit(int $id, string $identifier, array $structure)
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $entity->setIdentifier($identifier);
            $entity->setItems($structure);
            $this->saveChanges($entity);
            $this->commitChanges();
        }
    }

    public function getByIdentifier(string $identifier)
    {
        return $this->getEntityRepository()->findOneBy(["identifier" => $identifier]);
    }

}
