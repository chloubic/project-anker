<?php


namespace Anker\DAL\Repositories;

use Anker\Common\Utils\DateTime;
use Anker\Common\Utils\Text;
use Anker\DAL\Entities\PostMeta;
use Anker\DAL\Entities\PostType;
use Anker\DAL\Entities\User;
use Anker\DAL\Infrastructure\AppDbContext;
use Anker\DAL\Entities\Post;
use Tracy\Debugger;

class PostRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Post::class);
    }

    public function createPostType(string $slug, array $structure): void
    {
        $postType = new PostType($slug, $structure);
        $this->saveChanges($postType);
        $this->commitChanges();
    }

    public function getPostTypes(): array
    {
        $repository = $this->getRepositoryByEntity(PostType::class);
        return $repository->findAll();
    }

    public function removePostType($id): void
    {
        $repository = $this->getRepositoryByEntity(PostType::class);
        $entity = $repository->findOneBy(["id" => $id]);
        if ($entity != null) {
            $posts = $this->getRepositoryByEntity(Post::class)->findBy(["type" => $entity->getId()]);
            foreach ($posts as $post) {
                $metas = $this->getRepositoryByEntity(PostMeta::class)->findBy(["post" => $post->getId()]);
                foreach ($metas as $meta) {
                    $this->removeEntity($meta);
                }
                $this->removeEntity($post);
            }
            $this->removeEntity($entity);
            $this->commitChanges();
        }
    }

    public function getPostStructure(string $postTypeSlug)
    {
        $repository = $this->getRepositoryByEntity(PostType::class);
        return $repository->findOneBy(["slug" => $postTypeSlug]);
    }

    public function createPost(\Nette\Security\User $user, \stdClass $values, $mutationId): void
    {
        $posts = $this->getPostMutationsById($mutationId);
        $postTypeRepository = $this->getRepositoryByEntity(PostType::class);
        $postTypeEntity = $postTypeRepository->findOneBy(["slug" => $values["_postType"]]);

        $userRepository = $this->getRepositoryByEntity(User::class);
        $userEntity = $userRepository->findOneBy(["id" => $user->getId()]);
        $post = new Post;
        $post->setAuthor($userEntity);
        $post->setType($postTypeEntity);
        $post->setSlug($values->_slug);

        $fields = [];

        foreach ($postTypeEntity->getFactories() as $factory) {
            $fields[$factory["slug"]] = $values[$factory["slug"]];
        }

        $post->setFields($fields);
        $post->setCreatedDateTimeGMT(DateTime::now());
        $post->setModifiedDateTimeGMT(DateTime::now());
        $post->setStatus(0);
        $this->saveChanges($post);

        $langMeta = new PostMeta;
        $langMeta->setPost($post);
        $langMeta->setIdentifier("lang");
        $langMeta->setData($values->_lang);
        $this->saveChanges($langMeta);

        $mutationIds = [];

        if (count($posts) > 0) {
            $mutationsMeta = [];
            foreach ($posts[0]["metas"] as $meta) {
                if ($meta["identifier"] == "mutations") {
                    $mutationsMeta = json_decode($meta["data"]);
                    break;
                }
            }
            array_push($mutationsMeta, $posts[0]["id"]);
            $mutationIds = $mutationsMeta;
        }

        $mutations = new PostMeta;
        $mutations->setPost($post);
        $mutations->setIdentifier("mutations");
        $mutations->setData(json_encode($mutationIds));

        $this->saveChanges($mutations);

        $this->commitChanges();

        foreach ($mutationIds as $mutId) {
            $mutEntity = $this->getRepositoryByEntity(PostMeta::class)->findOneBy(["post" => $mutId, "identifier" => "mutations"]);
            if ($mutEntity != null) {
                $arr = json_decode($mutEntity->getData());
                array_push($arr, $post->getId());
                $mutEntity->setData(json_encode($arr));
                $this->saveChanges($mutEntity);
            }
        }

        $this->commitChanges();
    }

    public function getPostTypeById(int $id)
    {
        return $this->getRepositoryByEntity(PostType::class)->findOneBy(["id" => $id]);
    }

    public function editPostType(int $id, string $slug, array $structure)
    {
        $entity = $this->getRepositoryByEntity(PostType::class)->findOneBy(["id" => $id]);
        if ($entity != null) {
            $entity->setSlug($slug);
            $entity->setFactories($structure);
            $this->saveChanges($entity);
            $this->commitChanges();
        }
    }

    public function getPostsByTypeAndLangWithLimit(string $type, string $lang, int $offset, int $limit): array
    {
        $query = $this->getEM()->createQueryBuilder()
            ->select("a", "u", "t")
            ->from(Post::class, "a")
            ->leftJoin("a.metas", "u")
            ->join("a.type", "t");
        $query = $query->where("t.slug = :type")->setParameter("type", $type);
        if (!Text::isEmpty($lang)) {
            $query = $query->andWhere("u.identifier = 'lang'")->andWhere("u.data = '' OR u.data = :lang")->setParameter("lang", $lang);
        }
        $query = $query->setFirstResult($offset)->setMaxResults($limit)->orderBy("a.createdDateTimeGMT", "DESC");

        return $query->getQuery()->getResult();
    }

    public function getPostById(int $id)
    {
        $query = $this->getEM()->createQueryBuilder()
            ->select("a", "u")
            ->from(Post::class, "a")
            ->leftJoin("a.metas", "u")
            ->where("a.id = :id")
            ->setParameter("id", $id);
        return $query->getQuery()->getArrayResult();
    }

    public function getPostMutationsById(int $mutationId)
    {
        $result = [];
        $entity = $this->getEM()->createQueryBuilder()
            ->select("a", "u", "t")
            ->from(Post::class, "a")
            ->leftJoin("a.metas", "u")
            ->join("a.type", "t")
            ->where("a.id = :id")
            ->setParameter("id", $mutationId)
            ->getQuery()->getArrayResult();
        if (count($entity) > 0) {
            $entity = $entity[0];
            $metas = $entity["metas"];
            $mutations = [];
            foreach ($metas as $meta) {
                if ($meta["identifier"] == "mutations") {
                    $mutations = json_decode($meta["data"]);
                    break;
                }
            }
            $query = $this->getEM()->createQueryBuilder()
                ->select("a", "u", "t")
                ->from(Post::class, "a")
                ->leftJoin("a.metas", "u")
                ->join("a.type", "t")
                ->where("a.id = :id")
                ->setParameter("id", $mutationId);
            $count = 0;
            foreach ($mutations as $mutation) {
                $query = $query->orWhere("a.id = :id{$count} AND a.type = :type")->setParameter("id{$count}", $mutation);
                $count++;
            }
            if ($count > 0) $query = $query->setParameter("type", $entity["type"]["id"]);
            $result = $query->getQuery()->getArrayResult();
        }


        return $result;
    }

    public function delete(int $id): string
    {
        $entity = $this->getPostById($id);
        $slug = "";
        if (count($entity) > 0) {
            $entity = $entity[0];
            $mutations = [];
            foreach ($entity["metas"] as $meta) {
                if ($meta["identifier"] == "mutations") {
                    $mutations = json_decode($meta["data"]);
                    break;
                }
            }
            foreach ($mutations as $mutation) {
                $mutEntity = $this->getRepositoryByEntity(PostMeta::class)->findOneBy(["post" => $mutation, "identifier" => "mutations"]);
                if ($mutEntity != null) {
                    $arr = json_decode($mutEntity->getData());
                    if (($key = array_search($id, $arr)) !== false) unset($arr[$key]);
                    $mutEntity->setData(json_encode($arr));
                    $this->saveChanges($mutEntity);
                }
            }
            $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
            $entityMetas = $this->getRepositoryByEntity(PostMeta::class)->findBy(["post" => $id]);
            foreach ($entityMetas as $entityMeta) {
                $this->removeEntity($entityMetas);
            }
            $slug = $entity->getType()->getSlug();
            $this->removeEntity($entity);
            $this->commitChanges();
        }

        return $slug;
    }

    public function getPostTypeByPostId(int $id)
    {
        $entity = $this->getEM()->createQueryBuilder()
            ->select("a", "t")
            ->from(Post::class, "a")
            ->join("a.type", "t")
            ->where("a.id = :id")
            ->setParameter("id", $id)
            ->getQuery()->getArrayResult();
        if (count($entity) > 0) return $entity[0];
        return null;
    }

    public function getLangByPostId(int $id)
    {
        $entity = $this->getEM()->createQueryBuilder()
            ->select("a", "t")
            ->from(Post::class, "a")
            ->join("a.metas", "t")
            ->where("a.id = :id")
            ->setParameter("id", $id)
            ->getQuery()->getArrayResult();
        if (count($entity) > 0) {
            foreach ($entity[0]["metas"] as $meta) {
                if ($meta["identifier"] == "lang") return $meta["data"];
            }
        }
        return "";
    }

    public function editPost(\stdClass $values)
    {
        if ($values->_lang == "no") $values->_lang = "";
        $entity = $this->getEntityRepository()->findOneBy(["id" => $values->_mutationId]);
        $postTypeRepository = $this->getRepositoryByEntity(PostType::class);
        $postTypeEntity = $postTypeRepository->findOneBy(["slug" => $values["_postType"]]);
        if ($entity != null) {
            $entity->setSlug($values->_slug);
            $fields = [];

            foreach ($postTypeEntity->getFactories() as $factory) {
                $fields[$factory["slug"]] = $values[$factory["slug"]];
            }

            $entity->setFields($fields);
            $entity->setModifiedDateTimeGMT(DateTime::now());
            $this->saveChanges($entity);

            $langMeta = $this->getRepositoryByEntity(PostMeta::class)->findOneBy(["post" => $entity->getId(), "identifier" => "lang"]);
            if ($langMeta != null) {
                $langMeta->setData($values->_lang);
                $this->saveChanges($langMeta);
            }
            $this->saveChanges($langMeta);

            $this->commitChanges();
        }
    }

    public function getPostBySlug(string $slug, string $locale)
    {
        $post = $this->getEntityRepository()->findOneBy(["slug" => $slug]);
        if ($post != null) {
            $posts = $this->getPostMutationsById($post->getId());
            $default = null;
            $lang = null;
            foreach ($posts as $item) {
                $meta = $this->getLangByPostId($item["id"]);
                if (Text::isEmpty($meta)) $default = $item;
                if ($meta == $locale) $lang = $item;
            }
            if ($lang != null) return [$lang];
            if ($default != null) return [$default];
        }

        return [];
    }

    public function getTotalCount(): int
    {
        return $this->getRepositoryByEntity(PostType::class)->count([]);
    }

    public function getPostBySlugAndType(string $type, string $slug, string $locale)
    {
        $post = $this->getEntityRepository()->findOneBy(["slug" => $slug]);
        if ($post != null) {
            $posts = $this->getPostMutationsById($post->getId());
            $default = null;
            $lang = null;
            $postType = $this->getPostTypeByPostId($post->getId())["type"]["slug"];
            foreach ($posts as $item) {
                $meta = $this->getLangByPostId($item["id"]);
                if (Text::isEmpty($meta)) $default = $item;
                if ($meta == $locale) $lang = $item;
            }
            if($postType == $type) {
                if ($lang != null) return [$lang];
                if ($default != null) return [$default];
            }
        }

        return [];
    }

}