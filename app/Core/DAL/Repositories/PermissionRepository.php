<?php

namespace Anker\DAL\Repositories;

use Anker\DAL\Entities\Permission;
use Anker\DAL\Infrastructure\AppDbContext;

class PermissionRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Permission::class);
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->getEntityRepository()->findBy([], [], $limit, $offset);
    }

    public function delete(int $id)
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $this->removeEntity($entity);
            $this->commitChanges();
        }
    }

    public function create(string $identifier, string $resource, string $privilege) : void
    {
        $entity = new Permission;
        $entity->setIdentifier($identifier);
        $entity->setResource($resource);
        $entity->setPrivilege($privilege);

        $this->saveChanges($entity);
        $this->commitChanges();
    }

    public function edit(int $id, string $identifier, string $resource, string $privilege) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        $entity->setIdentifier($identifier);
        $entity->setResource($resource);
        $entity->setPrivilege($privilege);

        $this->saveChanges($entity);
        $this->commitChanges();
    }

    public function getById(int $id)
    {
        return $this->getEntityRepository()->findOneBy(["id" => $id]);
    }

    public function getAllGrouped() : array
    {
        $entities = $this->getEntityRepository()->findAll();
        $results = [];

        foreach ($entities as $entity)
        {
            $resource = $entity->getResource();
            if(!isset($results[$resource])) $results[$resource] = [];
            $results[$resource][$entity->getIdentifier()] = $entity->getPrivilege();
        }

        return $results;
    }

    public function getTotalCount() : int
    {
        return $this->getEntityRepository()->count([]);
    }

}
