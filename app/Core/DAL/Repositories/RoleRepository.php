<?php


namespace Anker\DAL\Repositories;


use Anker\DAL\Entities\Role;
use Anker\DAL\Infrastructure\AppDbContext;
use Tracy\Debugger;

class RoleRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, Role::class);
    }

    public function getRoleByIdentifier(string $identifier) : Role
    {
        return $this->getEntityRepository()->findOneBy(["identifier" => $identifier]);
    }

    public function getAllRoles() : array
    {
        return $this->getEntityRepository()->findAll();
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->getEntityRepository()->findBy([], [], $limit, $offset);
    }

    public function delete(int $id) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $this->removeEntity($entity);
            $this->commitChanges();
        }
    }

    public function create(string $identifier, array $permissions) : void
    {
        $entity = new Role;
        $entity->setIdentifier($identifier);
        $entity->setAllowed($permissions);

        $this->saveChanges($entity);
        $this->commitChanges();
    }

    public function getById(int $id)
    {
        return $this->getEntityRepository()->findOneBy(["id" => $id]);
    }

    public function edit(int $id, string $identifier, array $permissions) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $entity->setIdentifier($identifier);
            $entity->setAllowed($permissions);
            $this->saveChanges($entity);
            $this->commitChanges();
        }
    }

    public function getTotalCount() : int
    {
        return $this->getEntityRepository()->count([]);
    }

}