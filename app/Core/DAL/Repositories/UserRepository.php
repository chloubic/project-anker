<?php


namespace Anker\DAL\Repositories;


use Anker\Common\Enums\Constants;
use Anker\Common\Utils\Text;
use Anker\DAL\Entities\Audit;
use Anker\DAL\Entities\User;
use Anker\DAL\Entities\UserMeta;
use Anker\DAL\Infrastructure\AppDbContext;
use Doctrine\Common\Collections\Criteria;
use Doctrine\ORM\NonUniqueResultException;
use Doctrine\ORM\NoResultException;
use Exception;
use Nette\Security\Passwords;

class UserRepository extends RepositoryBase
{

    public function __construct(AppDbContext $appDbContext)
    {
        parent::__construct($appDbContext, User::class);
    }

    public function userExists(string $username, string $email): bool
    {
        return ($this->getEntityRepository()
                    ->count(["username" => $username]) + $this->getEntityRepository()
                    ->count(["email" => $email])) > 0;
    }

    public function createUser(User $user): void
    {
        $this->saveChanges($user);
        $this->commitChanges();
    }

    public function addMetas(array $metasArray): void
    {
        foreach ($metasArray as $meta) {
            $this->saveChanges($meta);
        }
        $this->commitChanges();
    }

    public function getByAuthenticationData(string $identifier, string $password)
    {
        $criteria = Criteria::create();
            $criteria->where(Criteria::expr()->eq("username", $identifier))
            ->orWhere(Criteria::expr()->eq("email", $identifier))
            ->andWhere(Criteria::expr()->eq("password", Passwords::hash($password)));
        try {
            $user = $this->getEntityRepository()
                ->createQueryBuilder('o')
                ->where("o.username = :identifier OR o.email = :identifier")
                ->setParameter("identifier", $identifier)
                ->getQuery()->getSingleResult();
            if($user && Passwords::verify($password, $user->getPassword()))
                return $user;
            else return null;
        } catch (Exception $e) {
            return null;
        }
    }

    public function getById(int $id)
    {
        return $this->getEntityRepository()->findOneBy(["id" => $id]);
    }

    public function getUserRoleIdentifier(int $id) : string
    {
        $userMetaRepository = $this->getRepositoryByEntity(UserMeta::class);
        $meta = $userMetaRepository->findOneBy(["identifier" => Constants::USER_ROLE_META_IDENTIFIER]);
        return $meta == null ? "" : $meta->getData();
    }

    public function getAllWithLimit(int $offset, int $limit) : array
    {
        return $this->getEM()->createQueryBuilder()
            ->select("a", "u")
            ->from(User::class, "a")
            ->leftJoin("a.metas", "u")
            ->where("u.identifier='role'")
            ->setFirstResult($offset)
            ->setMaxResults($limit)
            ->getQuery()->getArrayResult();
    }

    public function getAllAuditsWithLimit(int $offset, int $limit)
    {
        return $this->getRepositoryByEntity(Audit::class)->findBy([], [], $limit, $offset);
    }

    public function delete(int $id)
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $metas = $this->getRepositoryByEntity(UserMeta::class)->findBy(["user" => $id]);
            $this->removeEntity($metas);
            $this->removeEntity($entity);
            $this->commitChanges();
        }
    }

    public function getUserRole(int $id)
    {
        $entity = $this->getRepositoryByEntity(UserMeta::class)->findOneBy(["user" => $id, "identifier" => "role"]);
        if($entity != null)
        {
            return $entity->getData();
        }
        return "";
    }

    public function edit(int $id, string $username, string $password, string $email, string $name, array $metas) : void
    {
        $entity = $this->getEntityRepository()->findOneBy(["id" => $id]);
        if($entity != null)
        {
            $entity->setUsername($username);
            if(!Text::isEmpty($password)) $entity->setPassword($password);
            $entity->setEmail($email);
            $entity->setName($name);
            $this->saveChanges($entity);
            $metaRepository = $this->getRepositoryByEntity(UserMeta::class);
            foreach ($metas as $key => $meta)
            {
                $item = $metaRepository->findOneBy(["id" => $id, "identifier" => $key]);
                if($item != null) {
                    if(!Text::isEmpty($meta)) $item->setData($meta);
                    $this->saveChanges($item);
                }
                else {
                    $item = new UserMeta;
                    $item->setUser($entity);
                    $item->setIdentifier($key);
                    $item->setData($meta);
                    $this->saveChanges($item);
                }
            }
            $this->commitChanges();
        }
    }

}