<?php

declare(strict_types=1);

namespace Anker\Extensions;

use Anker\Common\AnkerVariables;
use Anker\Common\Utils\File;
use Nette\Neon\Neon;

/**
 * Class NeonTool represent tool for Anker extensions
 */
class NeonTool
{

    /**
     * @param string $path String path of configuration NEON file of extension
     * @return array Extension configuration file properties
     */
    public static function getExtensionProperties(string $path) : array
    {
        $configFileContents = File::getContents($path);
        $configObjects = Neon::decode($configFileContents);
        $ankerSectionObjects = $configObjects[AnkerVariables::EXTENSION_CONFIG_SECTION_NAME];
        return $ankerSectionObjects == null ? [] : $ankerSectionObjects;
    }

}
