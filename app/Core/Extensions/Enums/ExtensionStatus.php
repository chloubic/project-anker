<?php

declare(strict_types=1);

namespace Anker\Extensions\Enums;

use MyCLabs\Enum\Enum;

/**
 * Class ExtensionStatus statuses of extensions
 */
class ExtensionStatus extends Enum
{

    public const CONNECTED = "connected";

    public const NOT_CONNECTED = "not_connected";

    public const INVALID_INFO_FILE = "invalid_info_file";

}
