<?php

declare(strict_types=1);

namespace Anker\Extensions\Enums;

use MyCLabs\Enum\Enum;

/**
 * Class NeonFileStatus represents extension NEON configuration file status
 */
class NeonFileStatus extends Enum
{

    public const AVAILABLE = "available";

    public const NOT_FOUND = "not_found";

    public const NOT_INIT = "not_init";

}
