<?php

declare(strict_types=1);

namespace Anker\Extensions;

/**
 * Class ExtensionList stores instances of valid Anker extensions
 */
class ExtensionList
{

    /**
     * @var array List of valid extensions
     */
    private $extensions;

    public function __construct()
    {
        $this->extensions = array();
    }

    /**
     * Adds new valid extension into array
     * @param ExtensionWrapper $extensionWrapper Instance of valid extension
     */
    public function add(ExtensionWrapper $extensionWrapper) : void
    {
        $this->extensions[] = $extensionWrapper;
    }

    /**
     * Return list of valid extensions
     * @return array List of valid extensions
     */
    public function getExtensions() : array
    {
        return $this->extensions;
    }

    /**
     * Returns extension by identifier
     * @param string $identifier Extension identifier
     * @return ExtensionWrapper Instance of valid extension
     */
    public function seachByIdentifier(string $identifier)
    {
        foreach ($this->getExtensions() as $extension)
        {
            if($extension->getExtensionIdentifier() == $identifier)
                return $extension;
        }
        return null;
    }

}
