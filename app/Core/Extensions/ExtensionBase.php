<?php

namespace Anker\Extensions;

use Anker\Common\AnkerWrapper;

/**
 * Class ExtensionBase represents base class of every Anker extension
 */
abstract class ExtensionBase implements IExtension
{

    /**
     * @var AnkerWrapper Instance of Anker wrapper object for data manipulation
     */
    protected $ankerWrapper;

    public function __construct(AnkerWrapper $ankerWrapper)
    {
        $this->ankerWrapper = $ankerWrapper;
    }

}
