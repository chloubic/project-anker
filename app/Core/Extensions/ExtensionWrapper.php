<?php

declare(strict_types=1);

namespace Anker\Extensions;

use Anker\Common\AnkerVariables;
use Anker\Common\Enums\Constants;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\Extensions\Enums\NeonFileStatus;
use Anker\Extensions\Enums\ExtensionStatus;
use Kdyby\Translation\Translator;

/**
 * Class ExtensionWrapper represents instance of valid Anker extension
 */
class ExtensionWrapper
{

    /**
     * @var string Extension name
     */
    private $extensionName;

    /**
     * @var string Extension description
     */
    private $extensionDescription;

    /**
     * @var string Extension author
     */
    private $extensionAuthor;

    /**
     * @var string Extension version
     */
    private $extensionVersion;

    /**
     * @var string Extension thumb image
     */
    private $extensionImage;

    /**
     * @var string Extension identifier
     */
    private $extensionIdentifier;

    /**
     * @var string Extension initialization class namespace
     */
    private $extensionClass;

    /**
     * @var string Extension status
     */
    private $extensionStatus;

    /**
     * @var string Extension info file path
     */
    private $infoFilePath;

    /**
     * @var string Extension info file status
     */
    private $infoFileStatus;

    /**
     * @var string Extension configuration file path
     */
    private $configFilePath;

    /**
     * @var string Extension configuration file status
     */
    private $configFileStatus;

    /**
     * @var ExtensionBase Instance of extension class
     */
    private $object;

    /**
     * @var Translator Framework translator
     */
    private $translator;

    public function __construct(string $extensionName, Translator $translator)
    {
        $this->extensionName = $extensionName;
        $this->translator = $translator;
        $this->extensionIdentifier = null;
        $this->extensionStatus = ExtensionStatus::NOT_CONNECTED;
        $this->configFileStatus = NeonFileStatus::NOT_INIT;
        $this->initialize();
    }

    /**
     * Function initializes extension information from NEON files of extension
     */
    private function initialize(): void
    {
        $this->setupConfigFile();
        $this->setupInfoFile();
        if ($this->infoFileStatus == NeonFileStatus::AVAILABLE) {
            $extensionProperties = NeonTool::getExtensionProperties($this->infoFilePath);
            if (ExtensionChecker::checkMinimumRequirements($extensionProperties)) {
                if (isset($extensionProperties[Constants::THEME_NAME_ATTR])) {
                    $this->extensionName = $extensionProperties[Constants::THEME_NAME_ATTR];
                } else {
                    $this->extensionName = $this->translator->translate("anker.extensions.info.name.undefined");
                }
                if (isset($extensionProperties[Constants::THEME_DESCRIPTION_ATTR])) {
                    $this->extensionDescription = $extensionProperties[Constants::THEME_DESCRIPTION_ATTR];
                } else {
                    $this->extensionDescription = $this->translator->translate("anker.extensions.info.description.undefined");
                }
                if (isset($extensionProperties[Constants::THEME_AUTHOR_ATTR])) {
                    $this->extensionAuthor = $extensionProperties[Constants::THEME_AUTHOR_ATTR];
                } else {
                    $this->extensionAuthor = $this->translator->translate("anker.extensions.info.author.undefined");
                }
                if (isset($extensionProperties[Constants::THEME_VERSION_ATTR])) {
                    $this->extensionVersion = $extensionProperties[Constants::THEME_VERSION_ATTR];
                } else {
                    $this->extensionVersion = $this->translator->translate("anker.extensions.info.version.undefined");
                }
                if (isset($extensionProperties[Constants::THEME_IMAGE_ATTR])) {
                    $this->extensionImage = $extensionProperties[Constants::THEME_IMAGE_ATTR];
                } else {
                    $this->extensionImage = $this->translator->translate("anker.extensions.info.image.undefined");
                }
                if (isset($extensionProperties[Constants::THEME_CLASS_ATTR])) {
                    $this->extensionClass = $extensionProperties[Constants::THEME_CLASS_ATTR];
                } else {
                    $this->extensionClass = $this->translator->translate("anker.extensions.info.class.undefined");
                }
                if (isset($extensionProperties[Constants::EXTENSION_IDENTIFIER_ATTR])) {
                    $this->extensionIdentifier = $extensionProperties[Constants::EXTENSION_IDENTIFIER_ATTR];
                } else {
                    $this->extensionIdentifier = null;
                }
                if ($this->extensionIdentifier != null) {
                    $this->extensionStatus = ExtensionStatus::CONNECTED;
                } else {
                    $this->extensionStatus = ExtensionStatus::INVALID_INFO_FILE;
                    $this->extensionDescription = $this->translator->translate("anker.extensions.info.description.undefined");
                    $this->extensionAuthor = $this->translator->translate("anker.extensions.info.author.undefined");
                    $this->extensionVersion = $this->translator->translate("anker.extensions.info.version.undefined");
                    $this->extensionImage = $this->translator->translate("anker.extensions.info.image.undefined");
                    $this->extensionIdentifier = null;
                }
            } else {
                $this->extensionStatus = ExtensionStatus::INVALID_INFO_FILE;
                $this->extensionDescription = $this->translator->translate("anker.extensions.info.description.undefined");
                $this->extensionAuthor = $this->translator->translate("anker.extensions.info.author.undefined");
                $this->extensionVersion = $this->translator->translate("anker.extensions.info.version.undefined");
                $this->extensionImage = $this->translator->translate("anker.extensions.info.image.undefined");
                $this->extensionIdentifier = null;
            }
        }
    }

    /**
     * Function setups configuration NEON file of extension
     */
    private function setupConfigFile(): void
    {
        $configFilePath = FileSystem::fullPathFrom([Paths::getExtensionsDir(), $this->extensionName, AnkerVariables::EXTENSION_CONFIG]);
        if (!Text::isEmpty($configFilePath)) {
            $this->configFilePath = $configFilePath;
            $this->configFileStatus = NeonFileStatus::AVAILABLE;
        } else {
            $this->configFileStatus = NeonFileStatus::NOT_FOUND;
        }
    }

    /**
     * Function setups information NEON file of extension
     */
    private function setupInfoFile(): void
    {
        $infoFilePath = FileSystem::fullPathFrom([Paths::getExtensionsDir(), $this->extensionName, AnkerVariables::EXTENSION_INFO]);
        if (!Text::isEmpty($infoFilePath)) {
            $this->infoFilePath = $infoFilePath;
            $this->infoFileStatus = NeonFileStatus::AVAILABLE;
        } else {
            $this->infoFileStatus = NeonFileStatus::NOT_FOUND;
        }
    }

    /**
     * @return string
     */
    public function getExtensionName(): string
    {
        return $this->extensionName;
    }

    /**
     * @return mixed
     */
    public function getConfigFilePath()
    {
        return $this->configFilePath;
    }

    /**
     * @return string
     */
    public function getConfigFileStatus(): string
    {
        return $this->configFileStatus;
    }

    /**
     * @return string
     */
    public function getExtensionStatus(): string
    {
        return $this->extensionStatus;
    }

    /**
     * @return mixed
     */
    public function getInfoFileStatus()
    {
        return $this->infoFileStatus;
    }

    /**
     * @return mixed
     */
    public function getInfoFilePath()
    {
        return $this->infoFilePath;
    }

    public function isConnected(): bool
    {
        return $this->extensionStatus == ExtensionStatus::CONNECTED;
    }

    public function getObject()
    {
        return $this->object;
    }

    public function setObject($object): void
    {
        $this->object = $object;
    }

    /**
     * @return mixed
     */
    public function getExtensionDescription()
    {
        return $this->extensionDescription;
    }

    /**
     * @param mixed $extensionDescription
     */
    public function setExtensionDescription($extensionDescription): void
    {
        $this->extensionDescription = $extensionDescription;
    }

    /**
     * @return mixed
     */
    public function getExtensionAuthor()
    {
        return $this->extensionAuthor;
    }

    /**
     * @param mixed $extensionAuthor
     */
    public function setExtensionAuthor($extensionAuthor): void
    {
        $this->extensionAuthor = $extensionAuthor;
    }

    /**
     * @return mixed
     */
    public function getExtensionVersion()
    {
        return $this->extensionVersion;
    }

    /**
     * @param mixed $extensionVersion
     */
    public function setExtensionVersion($extensionVersion): void
    {
        $this->extensionVersion = $extensionVersion;
    }

    /**
     * @return mixed
     */
    public function getExtensionImage()
    {
        return $this->extensionImage;
    }

    /**
     * @param mixed $extensionImage
     */
    public function setExtensionImage($extensionImage): void
    {
        $this->extensionImage = $extensionImage;
    }

    /**
     * @return null
     */
    public function getExtensionIdentifier()
    {
        return $this->extensionIdentifier;
    }

    /**
     * @param null $extensionIdentifier
     */
    public function setExtensionIdentifier($extensionIdentifier): void
    {
        $this->extensionIdentifier = $extensionIdentifier;
    }

    /**
     * @return mixed
     */
    public function getExtensionClass()
    {
        return $this->extensionClass;
    }

    /**
     * @param mixed $extensionClass
     */
    public function setExtensionClass($extensionClass): void
    {
        $this->extensionClass = $extensionClass;
    }

}
