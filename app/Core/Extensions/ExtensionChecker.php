<?php

declare(strict_types=1);

namespace Anker\Extensions;

use Anker\Common\AnkerVariables;

/**
 * Class ExtensionChecker represents extension checker (checks validity of extension structure)
 */
class ExtensionChecker
{

    /**
     * @param array $ankerSectionObjects Array of extension configuration file structure
     * @return bool If extension meets minimum requirements
     */
    public static function checkMinimumRequirements(array $ankerSectionObjects) : bool
    {
        $checkResult = true;
        $classSectionExists = array_key_exists(AnkerVariables::EXTENSION_CONFIG_SECTION_CLASS, $ankerSectionObjects);
        $checkResult = $checkResult && $classSectionExists;
        $checkResult = $checkResult && array_key_exists(AnkerVariables::EXTENSION_CONFIG_SECTION_IDENTIFIER, $ankerSectionObjects);
        if($classSectionExists) {
            try {
                $classReflection = new \ReflectionClass($ankerSectionObjects[AnkerVariables::EXTENSION_CONFIG_SECTION_CLASS]);
                $checkResult = $checkResult && $classReflection->isInstantiable();
                $checkResult = $checkResult && $classReflection->isSubclassOf(ExtensionBase::class);
                unset($classReflection);
            } catch (\ReflectionException $e) {
                $checkResult = false;
            }
        }
        return $checkResult;
    }

}
