<?php

namespace Anker\Extensions;

use Anker\Common\AnkerWrapper;

/**
 * Interface IExtension defines functions of initialization class of Anker extension
 */
interface IExtension
{

    public function __construct(AnkerWrapper $ankerWrapper);

    /**
     * Initializes extension functions
     */
    public function initialize();

    /**
     * Function call during extension installation process
     * @param AnkerWrapper $ankerWrapper Object of Anker wrapper for data manipulation
     */
    public static function install(AnkerWrapper $ankerWrapper);

    /**
     * Function call during extension uninstallation process
     * @param AnkerWrapper $ankerWrapper Object of Anker wrapper for data manipulation
     */
    public static function uninstall(AnkerWrapper $ankerWrapper);

}
