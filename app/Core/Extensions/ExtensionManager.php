<?php

declare(strict_types=1);

namespace Anker\Extensions;

use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Kdyby\Translation\Translator;
use Nette\Utils\Finder;

/**
 * Class ExtensionManager manages Anker extensions
 */
class ExtensionManager
{

    /**
     * Creates instance of extension list
     * @param Translator $translator Framework translator
     * @return ExtensionList Instance of valid extensions crate
     */
    public static function loadExtensions(Translator $translator) : ExtensionList
    {
        $extensionList = new ExtensionList;
        self::createExtensionWrappers($extensionList, $translator);
        return $extensionList;
    }

    /**
     * Loads valid extensions
     * @param ExtensionList $extensionList Instance of valid extensions cratea
     * @param Translator $translator Framework translator
     */
    private static function createExtensionWrappers(ExtensionList $extensionList, Translator $translator) : void
    {
        foreach(Finder::findDirectories()->in(Paths::getExtensionsDir()) as $path => $directory)
        {
            $dirName = FileSystem::getDirOrFileName($path);
            $extensionWrapper = new ExtensionWrapper($dirName, $translator);
            $extensionList->add($extensionWrapper);
        }
    }

}
