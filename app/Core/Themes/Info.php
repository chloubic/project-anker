<?php


namespace Anker\Themes;


use Anker\Common\Utils\Paths;
use Anker\DAL\Repositories\SettingsRepository;
use Nette\Neon\Neon;

/**
 * Class Info stores information about active themes of Anker CMS
 */
class Info
{

    /**
     * @var string Active frontend theme identifier
     */
    private $frontend;

    /**
     * @var string Active backend theme identifier
     */
    private $backend;

    /**
     * Loads active theme identifiers from configuration file
     * @return Info Instance of active themes information crate
     */
    public static function loadActiveThemes() : Info
    {
        $info = new Info;

        $themesNeon = Neon::decode(file_get_contents(Paths::getAnkerThemes()));

        $frontendValue = '';
        $backendValue = '';

        if(isset($themesNeon['anker']['themes']['frontend'][0]))
        {
            $frontendValue = $themesNeon['anker']['themes']['frontend'][0];
        }

        if(isset($themesNeon['anker']['themes']['backend'][0]))
        {
            $backendValue = $themesNeon['anker']['themes']['backend'][0];
        }

        $info->setFrontend($frontendValue);
        $info->setBackend($backendValue);

        return $info;
    }

    /**
     * @return string Frontend theme identifier
     */
    public function getFrontend() : string
    {
        return $this->frontend;
    }

    /**
     * Sets frontend theme identifier
     * @param string $frontend Frontend theme identifier
     */
    public function setFrontend(string $frontend): void
    {
        $this->frontend = $frontend;
    }

    /**
     * @return string Backend theme identifier
     */
    public function getBackend() : string
    {
        return $this->backend;
    }

    /**
     * Sets backend theme identifier
     * @param string $backend backend theme identifier
     */
    public function setBackend(string $backend): void
    {
        $this->backend = $backend;
    }

}
