<?php

namespace Anker\Themes;

use Anker\Presenters\Handlers\HandlerPresenter;

/**
 * Interface IInitializer describes interface of theme initialize class
 */
interface IInitializer
{

    /**
     * Header of theme initialize function
     * @param HandlerPresenter $presenter Instance of Anker handler presenter
     */
    public static function initialize(HandlerPresenter $presenter) : void;

}
