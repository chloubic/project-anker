<?php

namespace Anker\Presenters\Loader;

use Anker\Common\AnkerWrapper;
use Anker\Presenters\Base\BasePresenter;

/**
 * Class ErrorPresenter represents Anker error template
 */
class ErrorPresenter extends BasePresenter
{

    public function __construct(AnkerWrapper $ankerWrapper)
    {
        parent::__construct($ankerWrapper);
    }

    public function renderDefault()
    {
        $this->setTemplate("anker.error");
    }

}
