<?php

namespace Anker\Presenters\Loader;

/**
 * Class PresentersList represents list structure of active Anker presenters
 */
class PresentersList
{

    /**
     * @var array List of private presenters
     */
    private $private;

    /**
     * @var array List of public presenters
     */
    private $public;

    /**
     * @var array List of RESTful API presenters
     */
    private $api;

    public function __construct()
    {
        $this->private = array();
        $this->public = array();
        $this->api = array();
    }

    /**
     * Return list of private presenters
     * @return array List of private presenters
     */
    public function getPrivate(): array
    {
        return $this->private;
    }

    /**
     * Return list of public presenters
     * @return array List of public presenters
     */
    public function getPublic(): array
    {
        return $this->public;
    }

    /**
     * Return list of RESTful API presenters
     * @return array List of RESTful API presenters
     */
    public function getApi(): array
    {
        return $this->api;
    }

    /**
     * Adds private presenter into array
     * @param string $slug Presenter slug
     * @param string $class Presenter namespace
     */
    public function addPrivate(string $slug, string $class) : void
    {
        $this->private[$slug] = $class;
    }

    /**
     * Adds public presenter into array
     * @param string $slug Presenter slug
     * @param string $class Presenter namespace
     */
    public function addPublic(string $slug, string $class) : void
    {
        $this->public[$slug] = $class;
    }

    /**
     * Adds RESTful API presenter into array
     * @param string $slug Presenter slug
     * @param string $class Presenter namespace
     */
    public function addApi(string $slug, string $class) : void
    {
        $this->api[$slug] = $class;
    }

}
