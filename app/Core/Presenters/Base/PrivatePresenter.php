<?php

namespace Anker\Presenters\Base;

use Anker\Common\AnkerWrapper;

/**
 * Class PrivatePresenter represents private base presenter
 */
abstract class PrivatePresenter extends BasePresenter
{
    public function __construct(AnkerWrapper $ankerWrapper)
    {
        parent::__construct($ankerWrapper);
    }

    public abstract function renderDefault(array $params);
}
