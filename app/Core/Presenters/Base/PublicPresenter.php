<?php

namespace Anker\Presenters\Base;

use Anker\Common\AnkerWrapper;

/**
 * Class PublicPresenter represents public base presenter
 */
abstract class PublicPresenter extends BasePresenter
{
    public function __construct(AnkerWrapper $ankerWrapper)
    {
        parent::__construct($ankerWrapper);
    }

    public abstract function renderDefault(array $params);
}
