<?php

namespace Anker\Presenters\Base;

use Anker\Common\AnkerWrapper;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;
use Nette\Security\User;

/**
 * Class BasePresenter represents parent of base presenters
 */
abstract class BasePresenter
{

    /**
     * @var AnkerWrapper Instance of actual Anker wrapper for data manipulation
     */
    protected $ankerWrapper;

    public function __construct(AnkerWrapper $ankerWrapper)
    {
        $this->ankerWrapper = $ankerWrapper;
    }

    /**
     * Sets latte template
     * @param string $templateName Template name
     */
    protected function setTemplate(string $templateName) : void
    {
        $this->ankerWrapper->searchForTemplate($templateName);
    }

    /**
     * Sets template engine variable
     * @param string $variableName Name
     * @param mixed $variableValue Value
     */
    protected function setVariable(string $variableName, $variableValue) : void
    {
        $this->ankerWrapper->getTemplateEngine()->{$variableName} = $variableValue;
    }

    /**
     * Return variable value
     * @param string $variableName Name
     * @return mixed Value
     */
    protected function getVariable(string $variableName)
    {
        return $this->ankerWrapper->getTemplateEngine()->{$variableName};
    }

    /**
     * Returns instance of handler presenter
     * @return Presenter Instance of handler presenter
     */
    public function getPresenter(): Presenter
    {
        return $this->ankerWrapper->getPresenter();
    }

    /**
     * Returns instance of Nette translator
     * @return Translator Instance of Nette translator
     */
    protected function getTranslator() : Translator
    {
        return $this->ankerWrapper->getTranslator();
    }

    /**
     * Redirects users
     * @param string $url URL path
     * @param null $httpCode HTTP code
     * @throws \Nette\Application\AbortException Abort exception
     */
    protected function redirect(string $url, $httpCode = null) : void
    {
        if(strlen($url) > 0 && $url[0] != "/") $url = "/" . $url;
        $this->ankerWrapper->getPresenter()->redirectUrl($url, $httpCode);
    }

    /**
     * Returns shifted parameters
     * @param int $shift Shift value
     * @return array Array of params
     */
    protected function getParameters(int $shift = 0) : array
    {
        $params = $this->ankerWrapper->getParams();
        for($i = 0; $i < $shift; $i++) array_shift($params);
        return $params;
    }

    /**
     * Flashes message
     * @param string $message Message
     * @param string $type Flash type
     */
    protected function flashMessage($message, $type = 'info') : void
    {
        $this->ankerWrapper->getPresenter()->flashMessage($message, $type);
    }

    /**
     * Return instance of logged user
     * @return User Instance of logged user
     */
    protected function getUser() : User
    {
        return $this->ankerWrapper->getPresenter()->getUser();
    }

    /**
     * Sends response object to user
     * @param mixed $response Response object
     * @throws \Nette\Application\AbortException Abort exception
     */
    protected function sendResponse($response) : void
    {
        $this->ankerWrapper->getPresenter()->sendResponse($response);
    }

}
