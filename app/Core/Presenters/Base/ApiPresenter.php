<?php

namespace Anker\Presenters\Base;

use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Nette\Application\Request;

/**
 * Class ApiPresenter represents RESTful API base presenter
 */
abstract class ApiPresenter extends BasePresenter
{

    public function __construct(AnkerWrapper $ankerWrapper)
    {
        parent::__construct($ankerWrapper);
        $this->ankerWrapper->setTemplate(
            FileSystem::fullPathFrom([
                Paths::getAppDir(),
                "Core/Latte/Templates/anker.api.latte"
            ])
        );
    }

    public abstract function callDefault(Request $request);
}
