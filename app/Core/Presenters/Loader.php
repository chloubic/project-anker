<?php

namespace Anker\Presenters\Loader;

use Anker\Common\Enums\Constants;
use Anker\Common\Utils\File;
use Anker\Common\Utils\FileSystem;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\Text;
use Anker\Extensions\Enums\ExtensionStatus;
use Anker\Extensions\Enums\NeonFileStatus;
use Anker\Presenters\Base\ApiPresenter;
use Anker\Presenters\Base\PrivatePresenter;
use Anker\Presenters\Base\PublicPresenter;
use Anker\Themes\Info;
use Nette\Neon\Neon;
use Anker\Extensions\ExtensionList;

/**
 * Class Loader represents tool for registering Anker presenters into core
 */
class Loader
{

    /**
     * Load Anker presenters
     * @param ExtensionList $extensionList List of connected extensions
     * @param Info $themes List of active themes
     * @param array $activeExtensions List of active extensions
     * @return PresentersList List of connected Anker presenters
     */
    public static function loadPresenters(ExtensionList $extensionList, Info $themes, array $activeExtensions) : PresentersList
    {
        $presentersList = new PresentersList();
        foreach ($extensionList->getExtensions() as $extension)
        {
            if($extension->getExtensionStatus() == ExtensionStatus::CONNECTED && $extension->getInfoFileStatus() == NeonFileStatus::AVAILABLE
                && in_array($extension->getExtensionIdentifier(), $activeExtensions))
            {
                self::registerPresenters($presentersList, $extension->getInfoFilePath());
            }
        }
        $frontendDir = FileSystem::fullPathFrom([Paths::getThemesDir(), Constants::FRONTEND_THEMES_DIR, $themes->getFrontend(), Constants::PRESENTERS_CONFIG_FILE]);
        $backendDir = FileSystem::fullPathFrom([Paths::getThemesDir(), Constants::BACKEND_THEMES_DIR, $themes->getBackend(), Constants::PRESENTERS_CONFIG_FILE]);
        if(!Text::isEmpty($frontendDir))
        {
            self::registerPresenters($presentersList, $frontendDir);
        }
        if(!Text::isEmpty($backendDir))
        {
            self::registerPresenters($presentersList, $backendDir);
        }
        self::registerPresenters($presentersList, Paths::getAnkerConfig());
        return $presentersList;
    }

    /**
     * Registers Anker presenters
     * @param PresentersList $presentersList List of connected Anker presenters
     * @param string $path Path of Anker presenters configuration file
     */
    private static function registerPresenters(PresentersList $presentersList, string $path) : void
    {
        $configFileContents = File::getContents($path);
        $configObjects = Neon::decode($configFileContents);
        if(isset($configObjects[Constants::ANKER_SECTION_NAME])){
            $configObjects = $configObjects[Constants::ANKER_SECTION_NAME];
            if(isset($configObjects[Constants::ANKER_PRESENTERS_SECTION_NAME])){
                $configObjects = $configObjects[Constants::ANKER_PRESENTERS_SECTION_NAME];
                foreach ($configObjects as $class)
                {
                    try {
                        $classReflection = new \ReflectionClass($class);
                        if($classReflection->isInstantiable() && $classReflection->hasConstant(Constants::PRESENTER_SLUG_SECTION_NAME))
                        {
                            $slug = $classReflection->getConstant(Constants::PRESENTER_SLUG_SECTION_NAME);
                            if($classReflection->isSubclassOf(PrivatePresenter::class))
                            {
                                self::registerAs($presentersList, Constants::PRIVATE_PRESENTER, $slug, $class);
                            }
                            else if($classReflection->isSubclassOf(PublicPresenter::class))
                            {
                                self::registerAs($presentersList, Constants::PUBLIC_PRESENTER, $slug, $class);
                            }
                            else if($classReflection->isSubclassOf(ApiPresenter::class))
                            {
                                self::registerAs($presentersList, Constants::API_PRESENTER, $slug, $class);
                            }
                        }
                        unset($classReflection);
                    } catch (\ReflectionException $e) {}
                }
            }
        }
    }

    /**
     * Registers Anker presenter slug into core
     * @param PresentersList $presentersList List of active presenters
     * @param string $presenterType Presenter entrance type
     * @param string $slug Presenter slug
     * @param string $class Presenter namespace
     */
    private static function registerAs(PresentersList $presentersList, string $presenterType, $slug, $class) : void
    {
        if(is_array($slug))
        {
            foreach ($slug as $singleSlug)
            {
                $presentersList->{Constants::ADD_PREFIX . $presenterType}($singleSlug, $class);
            }
        }
        else
        {
            $presentersList->{Constants::ADD_PREFIX . $presenterType}($slug, $class);
        }
    }

}
