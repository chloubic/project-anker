<?php

declare(strict_types=1);

namespace Anker\Presenters\Handlers;

use Anker\BL\ACL\AnkerACL;
use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Common\Enums\EntranceType;
use Kdyby\Translation\Translator;

/**
 * Class ApiPresenter represents RESTful API handler entrance presenter
 */
class ApiPresenter extends HandlerPresenter
{

    public function __construct(AnkerWrapper $ankerWrapper, Translator $translator, UserFacade $userFacade, AnkerACL $ankerACL)
    {
        parent::__construct($ankerWrapper, EntranceType::API, HandlerPresenter::API_PRESENTER, $translator, $userFacade, $ankerACL);
    }

    public function renderCall(): void
    {
    }

}
