<?php

declare(strict_types=1);

namespace Anker\Presenters\Handlers;

use Anker\BL\ACL\AnkerACL;
use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Common\Enums\EntranceType;
use Kdyby\Translation\Translator;

/**
 * Class PagePresenter represents public handler entrance presenter
 */
class PagePresenter extends HandlerPresenter
{

    public function __construct(AnkerWrapper $ankerWrapper, Translator $translator, UserFacade $userFacade, AnkerACL $ankerACL)
    {
        parent::__construct($ankerWrapper, EntranceType::PUBLIC, HandlerPresenter::PUBLIC_PRESENTER, $translator, $userFacade, $ankerACL);
    }

    public function renderRender(): void
    {
    }

}
