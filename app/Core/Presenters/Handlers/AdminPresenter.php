<?php

declare(strict_types=1);

namespace Anker\Presenters\Handlers;

use Anker\BL\ACL\AnkerACL;
use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Common\Enums\EntranceType;
use Kdyby\Translation\Translator;

/**
 * Class AdminPresenter represents private handler entrance presenter
 */
class AdminPresenter extends HandlerPresenter
{
    public function __construct(AnkerWrapper $ankerWrapper, Translator $translator, UserFacade $userFacade, AnkerACL $ankerACL)
    {
        parent::__construct($ankerWrapper, EntranceType::PRIVATE,HandlerPresenter::PRIVATE_PRESENTER, $translator, $userFacade, $ankerACL);
    }

    public function renderRender(){
    }

}
