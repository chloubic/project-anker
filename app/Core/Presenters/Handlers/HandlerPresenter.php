<?php

declare(strict_types=1);

namespace Anker\Presenters\Handlers;

use Anker\BL\ACL\AnkerACL;
use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Latte\AnkerVinduParser;
use Anker\Presenters\Base\BasePresenter;
use Anker\Presenters\Loader\ErrorPresenter;
use Kdyby\Translation\Translator;
use Nette\Application\UI\Presenter;

/**
 * Class PagePresenter represents parent of handler entrance presenters
 */
abstract class HandlerPresenter extends Presenter
{

    /**
     * Slug of private presenters array
     */
    protected const PRIVATE_PRESENTER = "getPrivate";

    /**
     * Slug of public presenters array
     */
    protected const PUBLIC_PRESENTER = "getPublic";

    /**
     * Slug of RESTful API presenters array
     */
    protected const API_PRESENTER = "getApi";

    /**
     * Error presenter name
     */
    protected const ERROR_PRESENTER = "error";

    /**
     * Component prefix string
     */
    private const CREATE_COMPONENT_PREFIX = "createComponent";

    /**
     * Length of component prefix string
     */
    private const CREATE_COMPONENT_PREFIX_LENGTH = 15;

    /**
     * Slug of default presenter
     */
    private const DEFAULT_PRESENTER = "";

    /**
     * Default presenters render functions
     */
    private const DEFAULTS = [
        self::PRIVATE_PRESENTER => "renderDefault",
        self::PUBLIC_PRESENTER => "renderDefault",
        self::API_PRESENTER => "callDefault",
        self::ERROR_PRESENTER => "renderDefault"
    ];

    /**
     * Default presenters render functions prefixes
     */
    private const PREFIXES = [
        self::PRIVATE_PRESENTER => "render",
        self::PUBLIC_PRESENTER => "render",
        self::API_PRESENTER => "call"
    ];

    /**
     * @var Translator Instance of Nette translator
     */
    public $translator;

    /**
     * @var AnkerWrapper Instance of Anker wrapper for data manipulation
     */
    private $ankerWrapper;

    /**
     * @var string Presenter type
     */
    private $presenterType;

    /**
     * @var BasePresenter Instance of loaded Anker presenter
     */
    private $loadedPresenter;

    /**
     * @var UserFacade Instance of user facade
     */
    private $userFacade;

    /**
     * @var AnkerACL Instance of Anker ACL manager
     */
    private $ankerACL;

    public function __construct(AnkerWrapper $ankerWrapper, $entranceType, $presenterType, Translator $translator, UserFacade $userFacade, AnkerACL $ankerACL)
    {
        parent::__construct();
        $this->ankerWrapper = $ankerWrapper;
        $this->presenterType = $presenterType;
        $this->translator = $translator;
        $this->ankerWrapper->setEntranceType($entranceType);
        $this->ankerWrapper->setPresenter($this);
        $this->userFacade = $userFacade;
        $this->ankerACL = $ankerACL;
    }

    /**
     * @return array Formats layout template file
     */
    public function formatLayoutTemplateFiles(): array
    {
        return [ $this->ankerWrapper->getLayout() ];
    }

    /**
     * @return array Formats template file
     */
    public function formatTemplateFiles(): array
    {
        return [ $this->ankerWrapper->getTemplate() ];
    }

    /**
     * Returns instance of Anker wrapper for data manipulation
     * @return AnkerWrapper Instance of Anker wrapper for data manipulation
     */
    public function getAnkerWrapper() : AnkerWrapper
    {
        return $this->ankerWrapper;
    }

    /**
     * Add Vindu filter into latte engine
     */
    public function beforeRender()
    {
        parent::beforeRender();
        $this->template->addFilter('vindu', function ($var) {
            return AnkerVinduParser::parse($this, htmlspecialchars_decode($var));
        });
    }

    /**
     * Loads requested Anekr presenter
     * @param string $presenterType Entrance type
     */
    protected function callAction($presenterType) : void
    {
        $types = array(self::PRIVATE_PRESENTER, self::PUBLIC_PRESENTER, self::API_PRESENTER);
        if(in_array($presenterType, $types)) {
            $params = $this->ankerWrapper->getParams();
            array_shift($params);
            if(isset($params[0]) && method_exists($this->loadedPresenter, self::PREFIXES[$presenterType] . $params[0]))
            {
                $functionName = $params[0];
                array_shift($params);
                if($presenterType == self::API_PRESENTER) $this->loadedPresenter->{self::PREFIXES[$presenterType] . $functionName}($this->getRequest());
                else $this->loadedPresenter->{self::PREFIXES[$presenterType] . $functionName}($params);
            }
            else
            {
                if(!isset($params[0]) && method_exists($this->loadedPresenter, self::DEFAULTS[$presenterType]))
                {
                    if($presenterType == self::API_PRESENTER) $this->loadedPresenter->{self::DEFAULTS[$presenterType]}($this->getRequest());
                    else $this->loadedPresenter->{self::DEFAULTS[$presenterType]}($params);
                }
                else
                {
                    if($presenterType == self::PUBLIC_PRESENTER) $this->loadedPresenter->{self::DEFAULTS[$presenterType]}($this->ankerWrapper->getParams());
                    else $this->callErrorPresenter();
                }
            }
        }
        else
        {
            $this->callErrorPresenter();
        }
    }

    /**
     * Initializes Anker presenter
     * @param string $presenterType Entrance type
     */
    protected function initializePresenter($presenterType) : void
    {
        $types = array(self::PRIVATE_PRESENTER, self::PUBLIC_PRESENTER, self::API_PRESENTER);
        if(in_array($presenterType, $types))
        {
            $this->template->ankerHeader = $this->ankerWrapper->getAnkerHeader();
            if($presenterType == self::PRIVATE_PRESENTER)
            {
                $this->template->ankerAdminMenu = $this->ankerWrapper->getAdminMenu();
                $this->template->resourcesUrl = "/Themes/Backend/" . $this->ankerWrapper->getThemesInfo()->getBackend();
            }
            else if($presenterType == self::PUBLIC_PRESENTER)
            {
                $this->template->resourcesUrl = "/Themes/Frontend/" . $this->ankerWrapper->getThemesInfo()->getFrontend();
            }
            $params = $this->ankerWrapper->getParams();
            $presenters = $this->ankerWrapper->getPresentersList()->{$presenterType}();
            if(isset($params[0]))
            {
                if(isset($presenters[$params[0]]))
                {
                    $this->loadedPresenter = $this->ankerWrapper->getDIContainer()->getByType($presenters[$params[0]]);
                }
                else
                {
                    if($presenterType == self::PUBLIC_PRESENTER)
                    {
                        if(isset($presenters[self::DEFAULT_PRESENTER]))
                        {
                            $this->loadedPresenter = $this->ankerWrapper->getDIContainer()->getByType($presenters[self::DEFAULT_PRESENTER]);
                        }
                        else
                        {
                            $this->callErrorPresenter();
                        }
                    }
                    else $this->callErrorPresenter();
                }
            }
            else
            {
                if(isset($presenters[self::DEFAULT_PRESENTER]))
                {
                    $this->loadedPresenter = $this->ankerWrapper->getDIContainer()->getByType($presenters[self::DEFAULT_PRESENTER]);
                }
                else
                {
                    $this->callErrorPresenter();
                }
            }
        }
        else $this->callErrorPresenter();
    }

    /**
     * Calls Anker error presenter
     */
    private function callErrorPresenter() : void
    {
        $presenterClass = new ErrorPresenter($this->ankerWrapper);
        $this->loadedPresenter = $presenterClass;
        $presenterClass->renderDefault();
    }

    /**
     * Registers presenter components
     */
    private function registerComponents() : void
    {
        try {
            $rc = new \ReflectionClass($this->loadedPresenter);
            foreach ($rc->getMethods() as $method)
            {
                if(substr(strtolower($method->getName()), 0, self::CREATE_COMPONENT_PREFIX_LENGTH) === strtolower(self::CREATE_COMPONENT_PREFIX))
                {
                    $componentName = lcfirst(substr($method->getName(), self::CREATE_COMPONENT_PREFIX_LENGTH, strlen($method->getName()) - self::CREATE_COMPONENT_PREFIX_LENGTH));
                    $this->addComponent($this->loadedPresenter->{$method->getName()}(), $componentName);
                }
            }
        } catch (\ReflectionException $e) {}
    }

    /**
     * Processes Anker signals and initializes Anker objects
     * @throws \Nette\Application\UI\BadSignalException Signal excetion
     */
    public function processSignal()
    {
        $this->ankerWrapper->prepareParams();
        $this->user->setAuthenticator($this->userFacade);
        $this->user->setAuthorizator($this->ankerACL);
        if($this->user->isLoggedIn()) $this->ankerACL->initializeACL($this->user->getId());
        $this->ankerWrapper->initializeAdminMenu($this->getUser(), $this->ankerWrapper->getActiveExtensions());
        $this->initializePresenter($this->presenterType);
        if(isset($_COOKIE["lang"])) $this->translator->setLocale($_COOKIE["lang"]);
        else $this->translator->setLocale($this->translator->getAvailableLocales()[0]);
        $this->template->setTranslator($this->translator);
        $this->registerComponents();
        $this->ankerWrapper->callInitializer($this);
        parent::processSignal();
        $this->callAction($this->presenterType);
    }

    /**
     * Generates and compiles Latte template for requested Vindu
     * @param array $args Vindu parameters
     * @param string $content Vindu content
     * @return string Compiled latte Vindu template
     */
    public function processVindu(array $args, string $content)
    {
        $error = '<p>'.$this->translator->translate('anker.vindu.error').'</p>';
        if(!isset($args["id"])) return $error;
        $vindu = $this->ankerWrapper->getVinduByIdentifier($args["id"]);
        if($vindu == null) return $error;
        unset($args["id"]);
        return $vindu->renderVindu($args, $content);
    }

    /**
     * Processes requested Vindus in template parameters
     */
    protected function afterRender()
    {
        parent::afterRender();
        AnkerVinduParser::process($this);
    }
}
