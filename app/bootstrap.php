<?php

// Imports AUTOLOAD file from Vendor
require __DIR__ . '/../vendor/autoload.php';

use Nette\Neon\Neon;
use Nette\Utils\Finder;
use Anker\Common\Utils\Paths;
use Anker\Common\Utils\File;
use Anker\Common\Enums\Constants;

// Creates Nette container configurator
$configurator = new Nette\Configurator;

$configurator->setTimeZone('Europe/Prague');
$configurator->setTempDirectory(__DIR__ . '/../temp');

// Creates and registers Robot loader instance for project files
$configurator->createRobotLoader()
	->addDirectory(__DIR__)
	->register();

// Fetches Anker themes settings
$themesNeon = Neon::decode(file_get_contents(__DIR__ . '/config/anker.themes.neon'));

// Loads basic Anker/Nette configuration files
$configurator
    ->addConfig(__DIR__ . '/config/config.neon')
    ->addConfig(__DIR__ . '/config/config.local.neon');

// URL checking of requested theme
$urlArray = explode("/", trim($_SERVER["REQUEST_URI"], "/"));
$isFirstParam = isset($urlArray[0]);

// Loads configuration file of frontend theme
if((!$isFirstParam || ($isFirstParam && $urlArray[0] != "admin")) && isset($themesNeon['anker']['themes']['frontend'][0]))
{
    $frontendTheme = $themesNeon['anker']['themes']['frontend'][0];
    $configurator
        ->addConfig(__DIR__ . '/Themes/Frontend/' . $frontendTheme . '/services.config.neon');
}

// Loads configuration file of backend theme
if(($isFirstParam && $urlArray[0] == "admin") && isset($themesNeon['anker']['themes']['backend'][0]))
{
    $backendTheme = $themesNeon['anker']['themes']['backend'][0];
    $configurator
        ->addConfig(__DIR__ . '/Themes/Backend/' . $backendTheme . '/services.config.neon');
}

// Array for active extension identifiers
$activeExtensions = [];

$decoded = Neon::decode(File::getContents(Paths::getAnkerExtensions()));

if(isset($decoded["active"]))
{
    $activeExtensions = $decoded["active"];
}

// Registers configuration files of active Anker extensions
foreach (Finder::findDirectories('/*/')->from(Paths::getExtensionsDir()) as $dirKey => $directory)
{
    $decoded = Neon::decode(File::getContents($dirKey . "/info.neon"));
    if(isset($decoded[Constants::ANKER_SECTION_NAME][Constants::EXTENSION_IDENTIFIER_ATTR])
        && in_array($decoded[Constants::ANKER_SECTION_NAME][Constants::EXTENSION_IDENTIFIER_ATTR], $activeExtensions))
    {
        foreach (Finder::findFiles('config.neon')->from($dirKey) as $fileKey => $file) {
            $configurator->addConfig($fileKey);
        }
    }
}

// Creates new application container
return $configurator->createContainer();
