<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200316172324 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE anker_posts (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, posttype_id INT DEFAULT NULL, slug VARCHAR(255) NOT NULL, fields LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', status SMALLINT NOT NULL, created_date_time_gmt DATETIME NOT NULL, modified_date_time_gmt DATETIME NOT NULL, INDEX IDX_BB9E4BB6A76ED395 (user_id), INDEX IDX_BB9E4BB6C5D8F75 (posttype_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_postmeta (id INT AUTO_INCREMENT NOT NULL, post_id INT DEFAULT NULL, identifier VARCHAR(255) NOT NULL, data VARCHAR(255) NOT NULL, INDEX IDX_B594B7DD4B89032C (post_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_posttypes (id INT AUTO_INCREMENT NOT NULL, slug VARCHAR(255) NOT NULL, factories LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', permissions LONGTEXT NOT NULL COMMENT \'(DC2Type:json)\', PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE anker_posts ADD CONSTRAINT FK_BB9E4BB6A76ED395 FOREIGN KEY (user_id) REFERENCES anker_users (id)');
        $this->addSql('ALTER TABLE anker_posts ADD CONSTRAINT FK_BB9E4BB6C5D8F75 FOREIGN KEY (posttype_id) REFERENCES anker_posttypes (id)');
        $this->addSql('ALTER TABLE anker_postmeta ADD CONSTRAINT FK_B594B7DD4B89032C FOREIGN KEY (post_id) REFERENCES anker_posts (id)');
        $this->addSql('DROP INDEX UNIQ_930F97A48A90ABA9 ON anker_settings');
        $this->addSql('ALTER TABLE anker_settings ADD identifier VARCHAR(255) NOT NULL, ADD data VARCHAR(255) NOT NULL, DROP `key`, DROP value');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_930F97A4772E836A ON anker_settings (identifier)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anker_postmeta DROP FOREIGN KEY FK_B594B7DD4B89032C');
        $this->addSql('ALTER TABLE anker_posts DROP FOREIGN KEY FK_BB9E4BB6C5D8F75');
        $this->addSql('DROP TABLE anker_posts');
        $this->addSql('DROP TABLE anker_postmeta');
        $this->addSql('DROP TABLE anker_posttypes');
        $this->addSql('DROP INDEX UNIQ_930F97A4772E836A ON anker_settings');
        $this->addSql('ALTER TABLE anker_settings ADD `key` VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, ADD value VARCHAR(255) CHARACTER SET utf8 NOT NULL COLLATE `utf8_unicode_ci`, DROP identifier, DROP data');
        $this->addSql('CREATE UNIQUE INDEX UNIQ_930F97A48A90ABA9 ON anker_settings (`key`)');
    }
}
