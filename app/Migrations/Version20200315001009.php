<?php

declare(strict_types=1);

namespace Migrations;

use Doctrine\DBAL\Schema\Schema;
use Doctrine\Migrations\AbstractMigration;

/**
 * Auto-generated Migration: Please modify to your needs!
 */
final class Version20200315001009 extends AbstractMigration
{
    public function getDescription() : string
    {
        return '';
    }

    public function up(Schema $schema) : void
    {
        // this up() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('CREATE TABLE anker_audit (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, date_time_gmt DATETIME NOT NULL, agent VARCHAR(255) NOT NULL, ip VARCHAR(255) NOT NULL, place VARCHAR(255) NOT NULL, INDEX IDX_A1DB0E35A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_auditmeta (id INT AUTO_INCREMENT NOT NULL, audit_id INT DEFAULT NULL, identifier VARCHAR(255) NOT NULL, data VARCHAR(255) NOT NULL, INDEX IDX_D984BE06BD29F359 (audit_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_media (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, added_date_time_gmt DATETIME NOT NULL, path VARCHAR(255) NOT NULL, INDEX IDX_59EF5040A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_mediameta (id INT AUTO_INCREMENT NOT NULL, media_id INT DEFAULT NULL, identifier VARCHAR(255) NOT NULL, data VARCHAR(255) NOT NULL, INDEX IDX_EB0B64CBEA9FDD75 (media_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_settings (id INT AUTO_INCREMENT NOT NULL, `key` VARCHAR(255) NOT NULL, value VARCHAR(255) NOT NULL, UNIQUE INDEX UNIQ_930F97A48A90ABA9 (`key`), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_users (id INT AUTO_INCREMENT NOT NULL, username VARCHAR(255) NOT NULL, password VARCHAR(255) NOT NULL, name VARCHAR(255) NOT NULL, normalized_name VARCHAR(255) NOT NULL, email VARCHAR(255) NOT NULL, activation_key VARCHAR(255) NOT NULL, activation_key_valid_gmt DATETIME NOT NULL, recovery_key VARCHAR(255) NOT NULL, recovery_key_valid_gmt DATETIME NOT NULL, status VARCHAR(255) NOT NULL, PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('CREATE TABLE anker_usermeta (id INT AUTO_INCREMENT NOT NULL, user_id INT DEFAULT NULL, identifier VARCHAR(255) NOT NULL, data VARCHAR(255) NOT NULL, INDEX IDX_7C55B25A76ED395 (user_id), PRIMARY KEY(id)) DEFAULT CHARACTER SET UTF8 COLLATE `UTF8_unicode_ci` ENGINE = InnoDB');
        $this->addSql('ALTER TABLE anker_audit ADD CONSTRAINT FK_A1DB0E35A76ED395 FOREIGN KEY (user_id) REFERENCES anker_users (id)');
        $this->addSql('ALTER TABLE anker_auditmeta ADD CONSTRAINT FK_D984BE06BD29F359 FOREIGN KEY (audit_id) REFERENCES anker_audit (id)');
        $this->addSql('ALTER TABLE anker_media ADD CONSTRAINT FK_59EF5040A76ED395 FOREIGN KEY (user_id) REFERENCES anker_users (id)');
        $this->addSql('ALTER TABLE anker_mediameta ADD CONSTRAINT FK_EB0B64CBEA9FDD75 FOREIGN KEY (media_id) REFERENCES anker_media (id)');
        $this->addSql('ALTER TABLE anker_usermeta ADD CONSTRAINT FK_7C55B25A76ED395 FOREIGN KEY (user_id) REFERENCES anker_users (id)');
    }

    public function down(Schema $schema) : void
    {
        // this down() migration is auto-generated, please modify it to your needs
        $this->abortIf($this->connection->getDatabasePlatform()->getName() !== 'mysql', 'Migration can only be executed safely on \'mysql\'.');

        $this->addSql('ALTER TABLE anker_auditmeta DROP FOREIGN KEY FK_D984BE06BD29F359');
        $this->addSql('ALTER TABLE anker_mediameta DROP FOREIGN KEY FK_EB0B64CBEA9FDD75');
        $this->addSql('ALTER TABLE anker_audit DROP FOREIGN KEY FK_A1DB0E35A76ED395');
        $this->addSql('ALTER TABLE anker_media DROP FOREIGN KEY FK_59EF5040A76ED395');
        $this->addSql('ALTER TABLE anker_usermeta DROP FOREIGN KEY FK_7C55B25A76ED395');
        $this->addSql('DROP TABLE anker_audit');
        $this->addSql('DROP TABLE anker_auditmeta');
        $this->addSql('DROP TABLE anker_media');
        $this->addSql('DROP TABLE anker_mediameta');
        $this->addSql('DROP TABLE anker_settings');
        $this->addSql('DROP TABLE anker_users');
        $this->addSql('DROP TABLE anker_usermeta');
    }
}
