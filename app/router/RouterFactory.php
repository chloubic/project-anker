<?php

declare(strict_types=1);

namespace App\Router;

use Nette;
use Nette\Application\Routers\Route;
use Nette\Application\Routers\RouteList;

// Application router class
final class RouterFactory
{
	use Nette\StaticClass;

    /**
     * @return RouteList
     */
	public static function createRouter(): RouteList
	{
		$router = new RouteList;
		// RESTful API endpoint router
        $router[] = new Route("api[/<params .*>]", "Api:call");
        // Admin router
        $router[] = new Route("admin[/<params .*>]", "Admin:render");
        // Public router
        $router[] = new Route("[<params .*>]", "Page:render");
		return $router;
	}
}
