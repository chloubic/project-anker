<?php


namespace Themes\Backend\Kapteinbrua\Utils;


use Nette\Utils\Html;
use Tracy\Debugger;

class Menu
{

    public static function unserialize(array $arr) : Html
    {
        $result = Html::el("ol")
        ->addAttributes(["class" => "dd-list"]);
        foreach ($arr as $item) {
            $wrapper = Html::el("li");
            $wrapper->addAttributes([
                "class" => "dd-item",
                "data-id" => $item["id"],
                "data-identifier" => $item["identifier"],
                "data-link" => $item["link"],
                "data-new" => $item["new"],
                "data-deleted" => $item["deleted"]
            ]);
            $result->addHtml($wrapper);
            $name = Html::el("div")
            ->addAttributes(["class" => "dd-handle"])
            ->setText($item["identifier"]);
            $wrapper->addHtml($name);
            $delete = Html::el("span")
                ->addAttributes([
                    "class" => "button-delete btn btn-default btn-xs pull-right",
                    "data-owner-id" => $item["id"]
                ]);
            $deleteIcon = Html::el("i")
                ->addAttributes([
                    "class" => "fa fa-times-circle-o",
                    "aria-hidden" => "true"
                ]);
            $delete->addHtml($deleteIcon);
            $wrapper->addHtml($delete);
            $edit = Html::el("span")
                ->addAttributes([
                    "class" => "button-edit btn btn-default btn-xs pull-right",
                    "data-owner-id" => $item["id"]
                ]);
            $editIcon = Html::el("i")
                ->addAttributes([
                    "class" => "fa fa-pencil",
                    "aria-hidden" => "true"
                ]);
            $edit->addHtml($editIcon);
            $wrapper->addHtml($edit);
            if(isset($item["children"])) $wrapper->addHtml(self::unserialize($item["children"]));
        }
        return $result;
    }
}