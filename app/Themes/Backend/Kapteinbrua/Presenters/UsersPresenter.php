<?php


namespace Themes\Backend\Kapteinbrua\Presenters;

use Anker\BL\Facades\RoleFacade;
use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\Text;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class UsersPresenter extends PrivatePresenter
{
    private const SLUG = "users";

    private $userFacade;

    private $roleFacade;

    public function __construct(AnkerWrapper $ankerWrapper, UserFacade $userFacade, RoleFacade $roleFacade)
    {
        parent::__construct($ankerWrapper);
        if (!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->userFacade = $userFacade;
        $this->roleFacade = $roleFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.overview"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_users', 'overview')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Users/overview");
            $users = $this->userFacade->getAllWithLimit($limit * ($page - 1), $limit);
            $this->setVariable("users", $users);
            if ($page != 1 && count($users) == 0) $this->setTemplate("anker.error");
        }
    }

    public function renderAudit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.audit"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_users', 'audit')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Users/audit");
            $audits = $this->userFacade->getAllAuditsWithLimit($limit * ($page - 1), $limit);
            $this->setVariable("audits", $audits);
            if ($page != 1 && count($audits) == 0) $this->setTemplate("anker.error");
        }
    }

    public function renderNew(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.new"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_users', 'new')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Users/new");
        }
    }

    public function createComponentNewUser(): Form
    {
        $form = new Form;

        $form->addText('username', $this->getTranslator()->translate('anker.users.new.username'));
        $form->addText('name', $this->getTranslator()->translate('anker.users.new.name'));
        $form->addEmail('email', $this->getTranslator()->translate('anker.users.new.email'));
        $form->addPassword('password', $this->getTranslator()->translate('anker.users.new.password'));
        $form->addSelect('role', $this->getTranslator()->translate('anker.users.new.role'))
            ->setItems($this->roleFacade->getRoles());

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.users.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'createNewUser'];

        return $form;
    }

    public function createNewUser(Form $form, \stdClass $values)
    {
        $this->userFacade->create($values->username, $values->password, $values->email, $values->name, ["role" => $values->role]);
        $this->redirect("/admin/users");
    }

    public function renderEdit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.edit"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_users', 'edit')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setTemplate("Users/edit");
                $this->setVariable("user", $this->userFacade->getById($params[0]));
            }
        }
    }

    public function createComponentEditUser(): Form
    {
        $params = $this->getParameters(2);

        $form = new Form;

        if (isset($params[0])) {
            $user = $this->userFacade->getById($params[0]);
            if ($user != null) {
                $role = $this->userFacade->getUserRole($params[0]);
                $form->addHidden("id")
                    ->setDefaultValue($user->getId());
                $form->addText('username', $this->getTranslator()->translate('anker.users.edit.username'))
                    ->setDefaultValue($user->getUsername())
                    ->setRequired(true);
                $form->addText('name', $this->getTranslator()->translate('anker.users.edit.name'))
                    ->setDefaultValue($user->getName())
                    ->setRequired(true);
                $form->addEmail('email', $this->getTranslator()->translate('anker.users.edit.email'))
                    ->setDefaultValue($user->getEmail())
                    ->setRequired(true);
                $form->addPassword('password', $this->getTranslator()->translate('anker.users.edit.password'));
                $form->addSelect('role', $this->getTranslator()->translate('anker.users.edit.role'))
                    ->setItems($this->roleFacade->getRoles());

                if (!Text::isEmpty($role)) $form["role"]->setDefaultValue($role);

                $form->addUIButton('submit', $this->getTranslator()->translate('anker.users.edit.submit'))
                    ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                    ->setAttribute('class', 'kapteinbrua_button');

                $form->onSuccess[] = [$this, 'editUser'];
            }
        }

        return $form;
    }

    public function editUser(Form $form, \stdClass $values)
    {
        if(!isset($values->role)) $values->role = "";
        $this->userFacade->edit($values->id, $values->username, $values->password, $values->email, $values->name, ["role" => $values->role]);
        $this->redirect("/admin/users");
    }

    public function renderProfile(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.profile"));
        $this->setTemplate("Users/profile");
        $this->setVariable("user", $this->userFacade->getById($this->getUser()->getId()));
    }

    public function createComponentProfileUser(): Form
    {
        $form = new Form;

        $user = $this->userFacade->getById($this->getUser()->getId());
        if ($user != null) {
            $role = $this->userFacade->getUserRole($this->getUser()->getId());
            $form->addHidden("id")
                ->setDefaultValue($user->getId());
            $form->addText('username', $this->getTranslator()->translate('anker.users.edit.username'))
                ->setDefaultValue($user->getUsername())
                ->setRequired(true);
            $form->addText('name', $this->getTranslator()->translate('anker.users.edit.name'))
                ->setDefaultValue($user->getName())
                ->setRequired(true);
            $form->addEmail('email', $this->getTranslator()->translate('anker.users.edit.email'))
                ->setDefaultValue($user->getEmail())
                ->setRequired(true);
            $form->addPassword('password', $this->getTranslator()->translate('anker.users.edit.password'));
            if ($this->getUser()->getAuthorizator()->hasACL('anker_users', 'edit'))
                $form->addSelect('role', $this->getTranslator()->translate('anker.users.edit.role'))
                    ->setItems($this->roleFacade->getRoles());

            if (!Text::isEmpty($role) && in_array($role, $this->roleFacade->getRoles())) $form["role"]->setDefaultValue($role);
            $form->addUIButton('submit', $this->getTranslator()->translate('anker.users.edit.submit'))
                ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                ->setAttribute('class', 'kapteinbrua_button');

            $form->onSuccess[] = [$this, 'editUser'];
        }

        return $form;
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.users.delete"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_users', 'delete')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->userFacade->delete($params[0]);
                $this->redirect("/admin/users");
            }
        }
    }
}