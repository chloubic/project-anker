<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Facades\MenuFacade;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Themes\Backend\Kapteinbrua\Utils\Menu;
use Tracy\Debugger;

class MenuPresenter extends PrivatePresenter
{

    private const SLUG = "menu";

    private $menuFacade;

    public function __construct(AnkerWrapper $ankerWrapper, MenuFacade $menuFacade)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->menuFacade = $menuFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.menu.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_menu', 'all')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Menu/overview");
            $roles = $this->menuFacade->getAllWithLimit($limit * ($page - 1), $limit);
            $this->setVariable("menu", $roles);
            if ($page != 1 && count($roles) == 0) $this->setTemplate("anker.error");
        }
    }

    public function renderNew(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.menu.new"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_menu', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Menu/new");
        }
    }

    public function createComponentNewMenu(): Form
    {
        $form = new Form;

        $form->addText('identifier', $this->getTranslator()->translate("anker.menu.new.identifier"))
            ->setRequired(true);

        $form->addHidden('structure', $this->getTranslator()->translate("anker.menu.new.structure"))
            ->setAttribute("id", "menuStructure")
            ->setRequired(true);

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.menu.new.submit'))
            ->setAttribute("id", "menuSubmit")
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, "createNewMenu"];

        return $form;
    }

    public function createNewMenu(Form $form, \stdClass $values)
    {
        $this->menuFacade->create($values->identifier, json_decode($values->structure));
        $this->redirect("/admin/menu");
    }

    public function renderEdit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.menu.edit"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_menu', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setTemplate("Menu/edit");
                $this->setVariable("menu", $this->menuFacade->getById($params[0]));
                $this->setVariable("unserialized", Menu::unserialize($this->getVariable("menu")->getItems()));
            }
        }
    }

    public function createComponentEditMenu(): Form
    {
        $params = $this->getParameters(2);

        $form = new Form;

        if (isset($params[0])) {
            $entity = $this->menuFacade->getById($params[0]);

            if($entity != null) {
                $form->addText('identifier', $this->getTranslator()->translate("anker.menu.edit.identifier"))
                    ->setDefaultValue($entity->getIdentifier())
                    ->setRequired(true);

                $form->addHidden('id')
                    ->setDefaultValue($entity->getId())
                    ->setRequired(true);

                $form->addHidden('structure', $this->getTranslator()->translate("anker.menu.edit.structure"))
                    ->setAttribute("id", "menuStructure")
                    ->setDefaultValue(json_encode($entity->getItems()))
                    ->setRequired(true);

                $form->addUIButton('submit', $this->getTranslator()->translate('anker.menu.edit.submit'))
                    ->setAttribute("id", "menuSubmit")
                    ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                    ->setAttribute('class', 'kapteinbrua_button');

                $form->onSuccess[] = [$this, "editMenu"];
            }
        }

        return $form;
    }

    public function editMenu(Form $form, \stdClass $values)
    {
        $this->menuFacade->edit($values->id, $values->identifier, json_decode($values->structure));
        $this->redirect("/admin/menu");
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.menu.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_menu', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->menuFacade->delete($params[0]);
                $this->redirect("/admin/menu");
            }
        }
    }

}