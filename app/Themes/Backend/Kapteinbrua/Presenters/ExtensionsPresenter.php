<?php

namespace Themes\Backend\Kapteinbrua\Presenters;

use Anker\BL\Managers\ExtensionsManager;
use Anker\Common\AnkerWrapper;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class ExtensionsPresenter extends PrivatePresenter
{

    private const SLUG = "extensions";

    private $extensionsManager;

    public function __construct(AnkerWrapper $ankerWrapper, ExtensionsManager $extensionsManager)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->extensionsManager = $extensionsManager;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.extensions.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_extensions', 'overview')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Extensions/overview");
            $this->setVariable("extensions", $this->ankerWrapper->getExtensionList()->getExtensions());
            $this->setVariable("active", $this->extensionsManager->getActiveExtensions());
        }
    }

    public function renderActivate(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.extensions.activation"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_extensions', 'activation')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) {
                $this->extensionsManager->activate($params[0], $this->ankerWrapper->getExtensionList(), $this->ankerWrapper);
                $this->redirect("/admin/extensions");
            } else $this->setTemplate("anker.error");
        }
    }

    public function renderDeactivate(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.extensions.activation"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_extensions', 'activation')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) {
                $this->extensionsManager->deactivate($params[0], $this->ankerWrapper->getExtensionList(), $this->ankerWrapper);
                $this->redirect("/admin/extensions");
            } else $this->setTemplate("anker.error");
        }
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.extensions.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_extensions', 'delete')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) {
                $this->extensionsManager->delete($params[0], $this->ankerWrapper->getExtensionList());
                $this->redirect("/admin/extensions");
            } else $this->setTemplate("anker.error");
        }
    }
}