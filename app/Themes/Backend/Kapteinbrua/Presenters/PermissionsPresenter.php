<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Facades\PermissionFacade;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class PermissionsPresenter extends PrivatePresenter
{

    private const SLUG = "permissions";

    private $permissionFacade;

    public function __construct(AnkerWrapper $ankerWrapper, PermissionFacade $permissionFacade)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->permissionFacade = $permissionFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.permissions.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_permissions', 'all')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Permissions/overview");
            $permissions = $this->permissionFacade->getAllWithLimit($limit * ($page - 1), $limit);
            $this->setVariable("permissions", $permissions);
            $this->setVariable("limit", $limit);
            $this->setVariable("page", $page);
            $this->setVariable("total", $this->permissionFacade->getTotalCount());
            if ($page != 1 && count($permissions) == 0) $this->setTemplate("anker.error");
        }
    }

    public function renderNew(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.permissions.new"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_permissions', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Permissions/new");
        }
    }

    public function createComponentNewPermission(): Form
    {
        $form = new Form;

        $form->addText('identifier', $this->getTranslator()->translate("anker.permissions.new.identifier"))
            ->setRequired(true);

        $form->addText('resource', $this->getTranslator()->translate("anker.permissions.new.resource"))
            ->setRequired(true);

        $form->addText('privilege', $this->getTranslator()->translate("anker.permissions.new.privilege"))
            ->setRequired(true);

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.permissions.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, "createNewPermission"];

        return $form;
    }

    public function createNewPermission(Form $form, \stdClass $values)
    {
        $this->permissionFacade->create($values->identifier, $values->resource, $values->privilege);
        $this->redirect("/admin/permissions");
    }

    public function renderEdit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.permissions.edit"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_permissions', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setTemplate("Permissions/edit");
                $permission = $this->permissionFacade->getById($params[0]);
                $this->setVariable("permission", $permission);
                if ($permission == null) $this->setTemplate("anker.error");
            }
        }
    }

    public function createComponentEditPermission(): Form
    {
        $params = $this->getParameters(2);

        $form = new Form;

        if (isset($params[0])) {
            $entity = $this->permissionFacade->getById($params[0]);

            $form->addHidden('id')
                ->setDefaultValue($entity->getId())
                ->setRequired(true);

            $form->addText('identifier', $this->getTranslator()->translate("anker.permissions.edit.identifier"))
                ->setDefaultValue($entity->getIdentifier())
                ->setRequired(true);

            $form->addText('resource', $this->getTranslator()->translate("anker.permissions.edit.resource"))
                ->setDefaultValue($entity->getResource())
                ->setRequired(true);

            $form->addText('privilege', $this->getTranslator()->translate("anker.permissions.edit.privilege"))
                ->setDefaultValue($entity->getPrivilege())
                ->setRequired(true);

            $form->addUIButton('submit', $this->getTranslator()->translate('anker.permissions.edit.submit'))
                ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                ->setAttribute('class', 'kapteinbrua_button');

            $form->onSuccess[] = [$this, "editPermission"];
        }

        return $form;
    }

    public function editPermission(Form $form, \stdClass $values)
    {
        $this->permissionFacade->edit($values->id, $values->identifier, $values->resource, $values->privilege);
        $this->redirect("/admin/permissions");
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.permissions.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_permissions', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->permissionFacade->delete($params[0]);
                $this->redirect("/admin/permissions");
            }
        }
    }
}