<?php

namespace Themes\Backend\Kapteinbrua\Presenters;

use Anker\BL\Managers\MediaManager;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class MediaPresenter extends PrivatePresenter
{
    private const SLUG = "media";

    private $mediaManager;

    public function __construct(AnkerWrapper $ankerWrapper, MediaManager $mediaManager)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->mediaManager = $mediaManager;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.media.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_media', 'overview')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Media/library");
            $this->setVariable('mediaList', $this->mediaManager->initilizeWithFileType($limit * ($page - 1), $limit));
            $this->setVariable("limit", $limit);
            $this->setVariable("page", $page);
            $this->setVariable("total", $this->mediaManager->getTotalCount());
        }
    }

    public function renderUpload(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.media.upload"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_media', 'upload')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Media/upload");
        }
    }

    public function createComponentUploadFiles() : Form
    {
        $form = new Form;

        $form->addMultiUpload('uploads', $this->getTranslator()->translate('anker.media.upload.files'));
        $form->addUIButton('submit', $this->getTranslator()->translate('anker.media.upload.submit'))
        ->setIcon('/Themes/Backend/Kapteinbrua/icons/upload.svg')
        ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, "uploadFiles"];

        return $form;
    }

    public function uploadFiles(Form $form, \stdClass $values) : void
    {
        $this->mediaManager->uploadFiles($this->getUser(), $values->uploads);
        $this->redirect("/admin/media");
    }

    public function renderDetail(array $params) : void
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.media.detail"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_media', 'overview')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setTemplate("Media/detail");
                $this->setVariable("media", $this->mediaManager->getMediaById($params[0]));
            }
        }
    }

    public function renderDelete(array $params) : void
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.media.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_media', 'delete')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->mediaManager->delete($params[0]);
                $this->redirect("/admin/media");
            }
        }
    }
}