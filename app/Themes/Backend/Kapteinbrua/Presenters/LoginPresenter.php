<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Facades\UserFacade;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Nette\Forms\Container;
use Nette\Security\AuthenticationException;
use Nette\Security\Passwords;
use Tracy\Debugger;

class LoginPresenter extends PrivatePresenter
{
    private const SLUG = "login";

    private $userFacade;

    public function __construct(AnkerWrapper $ankerWrapper, UserFacade $userFacade)
    {
        parent::__construct($ankerWrapper);
        $this->userFacade = $userFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.login"));
        if($this->getUser()->isLoggedIn()) $this->redirect("/admin");
        $this->setTemplate("Login/login");
    }

    public function renderOut(array $params)
    {
        $this->getUser()->logout();
        $this->redirect("/admin/login");
    }

    public function createComponentLoginForm(): Form
    {
        $form = new Form;
        $form->addText('identifier', $this->getTranslator()->translate('anker.login.identifier'))
            ->setRequired(true);
        $form->addPassword('password', $this->getTranslator()->translate('anker.login.password'))
            ->setRequired(true);
        $form->addUIButton('submit', $this->getTranslator()->translate('anker.login.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/sign-in.svg')
            ->setAttribute('class', 'kapteinbrua_button');
        $form->onSuccess[] = [$this, 'loginFormSucceeded'];
        return $form;
    }

    // volá se po úspěšném odeslání formuláře
    public function loginFormSucceeded(Form $form, \stdClass $values): void
    {
        try {
            $this->getUser()->login($values->identifier, $values->password);
            $this->redirect("/admin");
        } catch (AuthenticationException $e) { }
    }
}