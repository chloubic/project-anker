<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Facades\PermissionFacade;
use Anker\BL\Facades\RoleFacade;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class RolesPresenter extends PrivatePresenter
{

    private const SLUG = "roles";

    private $roleFacade;

    private $permissionFacade;

    public function __construct(AnkerWrapper $ankerWrapper, RoleFacade $roleFacade, PermissionFacade $permissionFacade)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->roleFacade = $roleFacade;
        $this->permissionFacade = $permissionFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.roles.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_roles', 'all')) $this->setTemplate("anker.error");
        else {
            $reqParams = $this->getPresenter()->getParameters();
            $page = 1;
            $limit = 50;
            if (isset($reqParams["page"])) $page = $reqParams["page"];
            if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
            if ($page < 1) $page = 1;
            if ($limit < 1) $limit = 1;
            $this->setTemplate("Roles/overview");
            $roles = $this->roleFacade->getAllWithLimit($limit * ($page - 1), $limit);
            $this->setVariable("roles", $roles);
            $this->setVariable("limit", $limit);
            $this->setVariable("page", $page);
            $this->setVariable("total", $this->roleFacade->getTotalCount());
            if ($page != 1 && count($roles) == 0) $this->setTemplate("anker.error");
        }
    }

    public function renderNew(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.roles.new"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_roles', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Roles/new");
        }
    }

    public function createComponentNewRole() : Form
    {
        $permissions = $this->permissionFacade->getAllGrouped();

        $form = new Form;

        $form->addText('identifier', $this->getTranslator()->translate("anker.roles.new.identifier"))
            ->setRequired(true);

        foreach ($permissions as $key => $permission)
        {
            $form->addCheckboxList($key, $key)
                ->setItems($permission);
        }

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.roles.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, "createNewRole"];

        return $form;
    }

    public function createNewRole(Form $form, \stdClass $values)
    {
        $identifier = $values->identifier;
        unset($values->identifier);
        unset($values->submit);
        $permissions = [];

        foreach ($values as $arr)
        {
            foreach ($arr as $value)
            {
                array_push($permissions, $value);
            }
        }

        $this->roleFacade->create($identifier, $permissions);
        $this->redirect("/admin/roles");
    }

    public function renderEdit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.roles.edit"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_roles', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setVariable("role", $this->roleFacade->getById($params[0]));
                $this->setTemplate("Roles/edit");
            }
        }
    }

    public function createComponentEditRole() : Form
    {
        $params = $this->getParameters(2);

        $permissions = $this->permissionFacade->getAllGrouped();

        $form = new Form;

        if (isset($params[0])) {

            $role = $this->roleFacade->getById($params[0]);

            if($role != null) {
                $form->addHidden('id')
                    ->setDefaultValue($role->getId());

                $form->addText('identifier', $this->getTranslator()->translate("anker.roles.edit.identifier"))
                    ->setDefaultValue($role->getIdentifier())
                    ->setRequired(true);

                foreach ($permissions as $key => $permission) {
                    $form->addCheckboxList($key, $key)
                        ->setItems($permission);
                }

                $form->addUIButton('submit', $this->getTranslator()->translate('anker.roles.edit.submit'))
                    ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                    ->setAttribute('class', 'kapteinbrua_button');

                $form->onSuccess[] = [$this, "editRole"];
            }
        }

        return $form;
    }

    public function editRole(Form $form, \stdClass $values)
    {
        $id = $values->id;
        $identifier = $values->identifier;
        unset($values->identifier);
        unset($values->id);
        unset($values->submit);
        $permissions = [];
        foreach ($values as $arr)
        {
            foreach ($arr as $value)
            {
                array_push($permissions, $value);
            }
        }
        $this->roleFacade->edit($id, $identifier, $permissions);
        $this->redirect("/admin/roles");
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.roles.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_roles', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->roleFacade->delete($params[0]);
                $this->redirect("/admin/roles");
            }
        }
    }
}