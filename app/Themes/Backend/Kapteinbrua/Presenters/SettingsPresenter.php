<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Facades\RoleFacade;
use Anker\BL\Facades\SettingsFacade;
use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\Paths;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class SettingsPresenter extends PrivatePresenter
{
    private const SLUG = "settings";

    private $settingsFacade;

    private $roleFacade;

    public function __construct(AnkerWrapper $ankerWrapper, SettingsFacade $settingsFacade, RoleFacade $roleFacade)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->settingsFacade = $settingsFacade;
        $this->roleFacade = $roleFacade;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.settings.general"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_settings', 'general')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Settings/general");
        }
    }

    public function createComponentGeneralForm(): Form
    {
        $form = new Form;

        $form->addText('web_name', $this->getTranslator()->translate('anker.settings.general.web_name'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('web_name'));
        $form->addText('web_description', $this->getTranslator()->translate('anker.settings.general.web_description'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('web_description'));
        $form->addText('web_author', $this->getTranslator()->translate('anker.settings.general.web_author'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('web_author'));
        $form->addCheckbox('registration_enabled', $this->getTranslator()->translate('anker.settings.general.registration_enabled'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('registration_enabled'));
        $form->addSelect('default_registration_user_role', $this->getTranslator()->translate('anker.settings.general.default_registration_user_role'))
            ->setItems($this->roleFacade->getRoles());

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.settings.general.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'saveGeneral'];

        return $form;
    }

    public function saveGeneral(Form $form, \stdClass $values): void
    {
        unset($values->submit);
        foreach ($values as $identifier => $value) {
            $this->settingsFacade->setOrCreate($identifier, $value);
        }
    }

    public function renderOg(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.settings.og"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_settings', 'og')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Settings/og");
        }
    }

    public function createComponentOGForm(): Form
    {
        $form = new Form;

        $form->addCheckbox('og_enabled', $this->getTranslator()->translate('anker.settings.og.enabled'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('og_enabled'));
        $form->addText('og_title', $this->getTranslator()->translate('anker.settings.og.title'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('og_title'));
        $form->addText('og_description', $this->getTranslator()->translate('anker.settings.og.description'))
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('og_description'));
        $form->addText('og_image_url', $this->getTranslator()->translate('anker.settings.og.image'))
            ->setType("url")
            ->setDefaultValue($this->settingsFacade->getValueByIdentifier('og_image_url'));

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.settings.og.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'saveOG'];

        return $form;
    }

    public function saveOG(Form $form, \stdClass $values): void
    {
        unset($values->submit);
        foreach ($values as $identifier => $value) {
            $this->settingsFacade->setOrCreate($identifier, $value);
        }
    }

    public function renderIcon(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.settings.icon"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_settings', 'icon')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Settings/icon");
            $this->setVariable("icon", $this->settingsFacade->getValueByIdentifier("icon"));
        }
    }

    public function createComponentIconForm(): Form
    {
        $form = new Form;

        $form->addUpload("icon", $this->getTranslator()->translate("anker.settings.icon.icon"))
            ->addRule(Form::IMAGE, $this->getTranslator()->translate("anker.settings.icon.rule.image"), "image/png")
            ->setAttribute("accept", "image/png")
            ->setRequired(true);

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.settings.icon.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'saveIcon'];

        return $form;
    }

    public function saveIcon(Form $form, \stdClass $values)
    {
        if ($values->icon->isOk() && $values->icon->isImage()) {
            $fileName = $values->icon->getSanitizedName();
            $path = Paths::getWWWDir() . "/" .$fileName;
            if($values->icon->getContentType() == "image/png") {
                $this->settingsFacade->setNewIcon($fileName);
                $values->icon->move($path);
                $this->redirect("/admin/settings/icon");
            }
        }
    }
}