<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Managers\ThemesManager;
use Anker\Common\AnkerWrapper;
use Anker\Presenters\Base\PrivatePresenter;
use Tracy\Debugger;

class ThemesPresenter extends PrivatePresenter
{

    private const SLUG = "themes";

    private $themesManager;

    public function __construct(AnkerWrapper $ankerWrapper, ThemesManager $themesManager)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->themesManager = $themesManager;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.error"));
        $this->setTemplate("anker.error");
    }

    public function renderFrontend(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.themes.frontend"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_themes', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Themes/frontend");
            $this->setVariable("themes", $this->themesManager->getFrontendThemes());
            $this->setVariable("active", $this->ankerWrapper->getThemesInfo()->getFrontend());
        }
    }

    public function renderBackend(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.themes.backend"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_themes', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Themes/backend");
            $this->setVariable("themes", $this->themesManager->getBackendThemes());
            $this->setVariable("active", $this->ankerWrapper->getThemesInfo()->getBackend());
        }
    }

    public function renderActivate(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.themes.activation"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_themes', 'all')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) {
                $type = $this->themesManager->activate($params[0]);
                $this->redirect("/admin/themes/" . $type);
            } else {
                $this->setTemplate("anker.error");
            }
        }
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.themes.delete"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_themes', 'all')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) {
                $type = $this->themesManager->delete($params[0]);
                $this->redirect("/admin/themes/" . $type);
            } else {
                $this->setTemplate("anker.error");
            }
        }
    }
}