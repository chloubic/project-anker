<?php

namespace Themes\Backend\Kapteinbrua\Presenters;

use Anker\BL\Factories\PostFactory;
use Anker\BL\Managers\LanguageManager;
use Anker\Common\AnkerWrapper;
use Anker\Forms\Components\UIButtonControl;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Anker\BL\Facades\PostFacade;
use Tracy\Debugger;

class PostPresenter extends PrivatePresenter
{

    private const SLUG = "post";

    private $postFactory;

    private $postFacade;

    private $languageManager;

    public function __construct(AnkerWrapper $ankerWrapper, PostFactory $postFactory, PostFacade $postFacade, LanguageManager $languageManager)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->postFactory = $postFactory;
        $this->postFacade = $postFacade;
        $this->languageManager = $languageManager;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.error"));
        $this->setTemplate("anker.error");
    }

    public function renderOverview(array $params)
    {
        $reqParams = $this->getPresenter()->getParameters();
        $page = 1;
        $limit = 20;
        if (isset($reqParams["page"])) $page = $reqParams["page"];
        if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
        if ($page < 1) $page = 1;
        if ($limit < 1) $limit = 1;
        if (!isset($params[0])) $this->setTemplate("anker.error");
        else {
            if (!$this->getUser()->getAuthorizator()->hasACL('anker_post_' . $params[0], 'overview')) $this->setTemplate("anker.error");
            else {
                $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.post.$params[0].overview"));
                $lang = $this->getPresenter()->getParameter('lang');
                if ($lang == null) $lang = "";
                $this->setTemplate("Post/overview");
                $this->setVariable("lang", $lang);
                $this->setVariable("languages", $this->languageManager->getPreparedAvailableLanguages());
                $posts = $this->postFacade->getPostsByTypeAndLangWithLimit($params[0], $lang, $limit * ($page - 1), $limit);
                $this->setVariable("posts", $posts);
                $this->setVariable("type", $params[0]);
            }
        }
    }

    public function renderNew(array $params)
    {
        if (isset($params[0])) {
            if (!$this->getUser()->getAuthorizator()->hasACL('anker_post_' . $params[0], 'new')) $this->setTemplate("anker.error");
            else {
                $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.post.$params[0].new"));
                $this->setTemplate("Post/new");
                $this->setVariable("postType", $params[0]);
            }
        } else $this->setTemplate("anker.error");
    }

    public function renderEdit(array $params)
    {
        if (!isset($params[0])) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Post/edit");
            $langs = $this->postFacade->filterAvailableLanguages($this->languageManager->getPreparedAvailableLanguages(), $params[0]);
            $this->setVariable("langs", $langs);
            $post = $this->postFacade->getPostTypeByPostId($params[0]);
            if ($post != null) {
                if (!$this->getUser()->getAuthorizator()->hasACL('anker_post_' . $post["type"]["slug"], 'edit')) $this->setTemplate("anker.error");
                else {
                    $this->setVariable("post", $post);
                    $mutArr = $this->postFacade->getMutationsByPostId($params[0]);
                    $mutations = [];
                    foreach ($mutArr as $item) {
                        $lang = null;
                        foreach ($item["metas"] as $meta) {
                            if ($meta["identifier"] == "lang") {
                                $lang = $meta["data"];
                                break;
                            }
                        }
                        if ($lang != null || $lang == "") {
                            $mutations[$item["id"]] = $lang;
                        }
                    }
                    $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.post.".$post["type"]["slug"].".edit"));
                    $lang = $this->postFacade->getLangByPostId($params[0]);
                    $this->setVariable("lang", $lang);
                    $this->setVariable("mutations", $mutations);
                }
            }
        }
    }

    public function renderDelete(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.post.delete"));
        if (!isset($params[0])) $this->setTemplate("anker.error");
        else {
            $post = $this->postFacade->getPostTypeByPostId($params[0]);
            if ($post != null) {
                if (!$this->getUser()->getAuthorizator()->hasACL('anker_post_' . $post["type"]["slug"], 'delete')) $this->setTemplate("anker.error");
                else {
                    $type = $this->postFacade->delete($params[0]);
                    $this->redirect("/admin/post/overview/" . $type);
                }
            }
        }
    }

    public function renderCreator(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.post.creator"));
        if (!$this->getUser()->getAuthorizator()->hasACL('anker_creator', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0]) || (isset($params[0]) && $params[0] == "overview")) {
                $reqParams = $this->getPresenter()->getParameters();
                $page = 1;
                $limit = 20;
                if (isset($reqParams["page"])) $page = $reqParams["page"];
                if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
                if ($page < 1) $page = 1;
                if ($limit < 1) $limit = 1;
                $this->setTemplate("Post/Creator/overview");
                $this->setVariable("postTypes", $this->postFacade->getPostTypes());
                $this->setVariable("limit", $limit);
                $this->setVariable("page", $page);
                $this->setVariable("total", $this->postFacade->getTotalCount());
            } else if ($params[0] == "new") {
                $this->setTemplate("Post/Creator/new");
                $this->setVariable("inputs", $this->postFactory->getInputs());
            } else if ($params[0] == "edit" && isset($params[1])) {
                $this->setTemplate("Post/Creator/edit");
                $this->setVariable("inputs", $this->postFactory->getInputs());
                $this->setVariable("postType", $this->postFacade->getPostTypeById($params[1]));
            } else if ($params[0] == "delete" && isset($params[1])) {
                $this->postFacade->removePostType($params[1]);
                $this->redirect("admin/post/creator");
            } else $this->setTemplate("anker.error");
        }
    }

    public function createComponentPostCreator(): Form
    {
        $form = new Form;

        $form->addText('slug', $this->getTranslator()->translate('anker.post.creator.new.slug'))->setRequired(true);
        $form->addHidden('structure')
            ->setAttribute('id', 'structureInput')
            ->setRequired(true);
        $form->addUIButton('submit', $this->getTranslator()->translate('anker.post.creator.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');
        $form->onSuccess[] = [$this, 'submitNewPostType'];

        return $form;
    }

    public function submitNewPostType(Form $form, \stdClass $values): void
    {
        $this->postFacade->createPostType($values->slug, json_decode($values->structure));
        $this->redirect("/admin/post/creator");
    }

    public function createComponentEditCreator(): Form
    {
        $params = $this->getParameters(3);

        $form = new Form;

        if (isset($params[0])) {
            $entity = $this->postFacade->getPostTypeById($params[0]);

            if ($entity != null) {
                $form->addHidden('id')
                    ->setDefaultValue($entity->getId())
                    ->setRequired(true);
                $form->addText('slug', $this->getTranslator()->translate('anker.post.creator.edit.slug'))
                    ->setDefaultValue($entity->getSlug())
                    ->setRequired(true);
                $form->addHidden('structure')
                    ->setAttribute('id', 'structureInput')
                    ->setDefaultValue(json_encode($entity->getFactories()))
                    ->setRequired(true);
                $form->addUIButton('submit', $this->getTranslator()->translate('anker.post.creator.edit.submit'))
                    ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                    ->setAttribute('class', 'kapteinbrua_button');
                $form->onSuccess[] = [$this, 'editPostType'];
            }
        }
        return $form;
    }

    public function editPostType(Form $form, \stdClass $values): void
    {
        $this->postFacade->editPostType($values->id, $values->slug, json_decode($values->structure));
        $this->redirect("/admin/post/creator");
    }

    public function createComponentNewPost(): Form
    {
        $params = $this->getParameters(1);
        if (isset($params[0]) && $params[0] == "new" && isset($params[1])) {
            $mutationId = $this->getPresenter()->getParameter("mutationId");
            if ($mutationId == null || $mutationId == "") $mutationId = 0;
            $langs = $this->postFacade->filterAvailableLanguages($this->languageManager->getPreparedAvailableLanguages(), $mutationId);
            $form = $this->postFactory->create($this->postFacade->getPostStructure($params[1]), $langs, $mutationId);
            $form["_submit"] = (new UIButtonControl($this->getTranslator()->translate('anker.post.type.' . $params[1] . '.new.submit')))
                ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                ->setAttribute('class', 'kapteinbrua_button');
            $form->onSuccess[] = [$this, "submitNewPost"];
            return $form;
        }
        return new Form;
    }

    public function submitNewPost(Form $form, \stdClass $values): void
    {
        $mutationId = $this->getPresenter()->getParameter("mutationId");
        if ($mutationId == null || $mutationId == "") $mutationId = 0;
        $this->postFacade->createPost($this->getUser(), $values, $values->_mutationId);
        $this->redirect("admin/post/overview/" . $values["_postType"]);
    }

    public function createComponentEditPost(): Form
    {
        $params = $this->getParameters(1);
        if (isset($params[0]) && $params[0] == "edit" && isset($params[1])) {
            $langs = $this->postFacade->filterAvailableLanguages($this->languageManager->getPreparedAvailableLanguages(), $params[1]);
            $postType = $this->postFacade->getPostTypeByPostId($params[1]);
            if ($postType != null) {
                $structure = $this->postFacade->getPostStructure($postType["type"]["slug"]);
                $lang = $this->postFacade->getLangByPostId($params[1]);
                if ($lang == "") $lang = "no";
                $langs[$lang] = $this->getTranslator()->translate("anker.languages.lang." . $lang);
                $form = $this->postFactory->create($structure, $langs, $params[1]);
                $form["_slug"]->setDefaultValue($postType["slug"]);
                $form["_lang"]->setDefaultValue($lang);
                foreach ($structure->getFactories() as $factory) {
                    if (isset($postType["fields"][$factory["slug"]])) $form[$factory["slug"]]->setDefaultValue($postType["fields"][$factory["slug"]]);
                }
                $form["_submit"] = (new UIButtonControl($this->getTranslator()->translate('anker.post.type.' . $postType["type"]["slug"] . '.edit.submit')))
                    ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                    ->setAttribute('class', 'kapteinbrua_button');
                $form->onSuccess[] = [$this, "editPost"];
                return $form;
            }
        }
        return new Form;
    }

    public function editPost(Form $form, \stdClass $values): void
    {
        $this->postFacade->editPost($values);
        $this->redirect("admin/post/overview/" . $values["_postType"]);
    }
}