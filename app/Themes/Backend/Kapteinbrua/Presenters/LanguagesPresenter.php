<?php


namespace Themes\Backend\Kapteinbrua\Presenters;


use Anker\BL\Managers\LanguageManager;
use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\Paths;
use Anker\Forms\Form;
use Anker\Presenters\Base\PrivatePresenter;
use Nette\Neon\Neon;
use Tracy\Debugger;

class LanguagesPresenter extends PrivatePresenter
{

    private const SLUG = "languages";

    private $languageManager;

    public function __construct(AnkerWrapper $ankerWrapper, LanguageManager $languageManager)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
        $this->languageManager = $languageManager;
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.overview"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            $this->languageManager->getAvailableLanguagesWithCatalogues();
            $this->setTemplate("Language/overview");
            $this->setVariable('availableLanguages', $this->languageManager->getAvailableLanguagesWithCatalogues());
        }
    }

    public function renderNew(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.new"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            $this->setTemplate("Language/new");
        }
    }

    public function createComponentNewLanguage(): Form
    {
        $form = new Form;

        $form->addText('languageCode', $this->getTranslator()->translate('anker.languages.new.languageCode'))
            ->setAttribute('placeholder', $this->getTranslator()->translate('anker.languages.new.languageCode.placeholder'))
            ->setRequired(true);

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.languages.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'createNewLanguage'];

        return $form;
    }

    public function createNewLanguage(Form $form, \stdClass $values): void
    {
        $this->languageManager->createLanguage($values->languageCode);
        $this->redirect("/admin/languages");
    }

    public function renderNewCatalogue(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.newCatalogue"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            if (!isset($params[0])) $this->setTemplate("anker.error");
            else {
                $this->setTemplate("Language/newCatalogue");
            }
        }
    }

    public function createComponentNewCatalogue(): Form
    {
        $form = new Form;

        $form->addHidden('languageCode');
        if (isset($this->getParameters(2)[0]))
            $form['languageCode']->setDefaultValue($this->getParameters(2)[0]);

        $form->addText('catalogueName', $this->getTranslator()->translate('anker.languages.newCatalogue.name'))
            ->setAttribute('placeholder', $this->getTranslator()->translate('anker.languages.newCatalogue.placeholder'))
            ->setRequired(true);

        $form->addUIButton('submit', $this->getTranslator()->translate('anker.languages.new.submit'))
            ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
            ->setAttribute('class', 'kapteinbrua_button');

        $form->onSuccess[] = [$this, 'createNewCatalogue'];

        return $form;
    }

    public function createNewCatalogue(Form $form, \stdClass $values): void
    {
        $this->languageManager->createCatalogue($values->catalogueName . "." . $values->languageCode);
        $this->redirect("/admin/languages");
    }

    public function renderEdit(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.edit"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0]) && file_exists(Paths::getLangDir() . "/" . $params[0] . ".neon")) {
                $reqParams = $this->getPresenter()->getParameters();
                $page = 1;
                $limit = 50;
                if (isset($reqParams["page"])) $page = $reqParams["page"];
                if (isset($reqParams["limit"])) $limit = $reqParams["limit"];
                if ($page < 1) $page = 1;
                if ($limit < 1) $limit = 1;
                $this->setTemplate("Language/edit");
                $this->setVariable("catalogue", $params[0]);
                $separated = explode(".", $params[0]);
                $this->setVariable("resources", $this->languageManager->getCatalogueResourcesWithLimit($separated[1], $separated[0], ($page-1)*$limit, $limit));
                $this->setVariable("limit", $limit);
                $this->setVariable("page", $page);
                $this->setVariable("total", $this->languageManager->getTotalCount($separated[1], $separated[0]));

            } else $this->setTemplate("anker.error");
        }
    }

    public function createComponentAddResource(): Form
    {
        $params = $this->getParameters(2);

        $form = new Form;

        if (isset($params[0]) && count(explode(".", $params[0])) == 2) {
            $form->addHidden('return')->setDefaultValue($params[0])->setRequired(true);
            $form->addText('identifier', $this->getTranslator()->translate("anker.languages.edit.new.identifier"))->setRequired(true);
            $form->addText('value', $this->getTranslator()->translate("anker.languages.edit.new.value"))->setRequired(true);
            $form->addUIButton('submit', $this->getTranslator()->translate('anker.languages.edit.new.submit'))
                ->setIcon('/Themes/Backend/Kapteinbrua/icons/check.svg')
                ->setAttribute('class', 'kapteinbrua_button');
            $form->onSuccess[] = [$this, "addResource"];
        }

        return $form;
    }

    public function addResource(Form $form, \stdClass $values)
    {
        $exploded = explode(".", $values->return);
        if (count($exploded) == 2) {
            $this->languageManager->saveTranslationToCatalogue($exploded[1], $exploded[0], $values->identifier, $values->value);
            $this->redirect("/admin/languages/edit/" . $values->return . "?page=1&limit=20");
        }
        $this->redirect("/admin/languages");
    }

    public function renderEditResource(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.editResource"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            $oldId = $this->getPresenter()->getParameter("oldId");
            $newId = $this->getPresenter()->getParameter("newId");
            $value = $this->getPresenter()->getParameter("value");
            $return = $this->getPresenter()->getParameter("return");
            if ($oldId != null && $newId != null && $value != null && $return != null) {
                $exploded = explode(".", $return);
                if (count($exploded) == 2) {
                    if ($oldId != $newId) $this->languageManager->removeResource($exploded[1], $exploded[0], $oldId);
                    $this->languageManager->saveTranslationToCatalogue($exploded[1], $exploded[0], $newId, $value);
                    $this->redirect("/admin/languages/edit/" . $return . "?page=1&limit=20");
                } else $this->setTemplate("anker.error");
            } else $this->setTemplate("anker.error");
        }
    }

    public function renderRemoveResource(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.removeResource"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            $id = $this->getPresenter()->getParameter("id");
            $return = $this->getPresenter()->getParameter("return");
            if ($id != null && $return != null) {
                $exploded = explode(".", $return);
                if (count($exploded) == 2) {
                    $this->languageManager->removeResource($exploded[1], $exploded[0], $id);
                    $this->redirect("/admin/languages/edit/" . $return . "?page=1&limit=20");
                } else $this->setTemplate("anker.error");
            } else $this->setTemplate("anker.error");
        }
    }

    public function renderRemove(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.remove"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) $this->languageManager->removeLanguage($params[0]);
            $this->redirect("/admin/languages");
        }
    }

    public function renderRemoveCatalogue(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.languages.removeCatalogue"));
        if(!$this->getUser()->getAuthorizator()->hasACL('anker_languages', 'all')) $this->setTemplate("anker.error");
        else {
            if (isset($params[0])) $this->languageManager->removeCatalogue($params[0]);
            $this->redirect("/admin/languages");
        }
    }
}