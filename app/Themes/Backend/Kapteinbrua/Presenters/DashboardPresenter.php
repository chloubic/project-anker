<?php

namespace Themes\Backend\Kapteinbrua\Presenters;

use Anker\Common\AnkerWrapper;
use Anker\Presenters\Base\PrivatePresenter;

class DashboardPresenter extends PrivatePresenter
{
    private const SLUG = ["", "dashboard"];

    public function __construct(AnkerWrapper $ankerWrapper)
    {
        parent::__construct($ankerWrapper);
        if(!$this->getUser()->isLoggedIn()) $this->redirect("/admin/login");
    }

    public function renderDefault(array $params)
    {
        $this->getVariable("ankerHeader")->setTitle($this->getTranslator()->translate("anker.meta.title.dashboard"));
        $this->setTemplate("Dashboard/page");
    }

}
