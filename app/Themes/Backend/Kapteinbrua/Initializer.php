<?php


namespace Themes\Backend\Kapteinbrua;


use Anker\Presenters\Handlers\HandlerPresenter;
use Anker\Themes\IInitializer;
use Tracy\Debugger;

class Initializer implements IInitializer
{

    public static function initialize(HandlerPresenter $presenter): void
    {
    }
}