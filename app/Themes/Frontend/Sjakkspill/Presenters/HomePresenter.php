<?php


namespace Themes\Frontend\Sjakkspill\Presenters;


use Anker\BL\Facades\MenuFacade;
use Anker\BL\Facades\PostFacade;
use Anker\BL\Factories\PostFactory;
use Anker\BL\Managers\MenuManager;
use Anker\Common\AnkerWrapper;
use Anker\Common\Utils\Text;
use Anker\DAL\Repositories\SettingsRepository;
use Anker\Forms\Form;
use Anker\Presenters\Base\PublicPresenter;
use Tracy\Debugger;

class HomePresenter extends PublicPresenter
{

    private const SLUG = ["", "home"];

    private $postFacade;

    public function __construct(AnkerWrapper $ankerWrapper, PostFacade $postFacade)
    {
        parent::__construct($ankerWrapper);
        $this->postFacade = $postFacade;
    }

    public function renderDefault(array $params)
    {
        $params = $this->getParameters();
        $imploded = trim(implode("/", $params), "/");
        if(Text::isEmpty($imploded) || $imploded == "home") {
            $this->getVariable("ankerHeader")->setTitle("Vítejte");
            $this->setTemplate("home");
        }
        else {
            if(isset($params[1]))
            {
                if($params[0] == "archive")
                {
                    $posts = $this->postFacade->getPostsByTypeAndLangWithLimit($params[1], $this->getTranslator()->getLocale(), 0, 100);
                    if(count($posts) > 0)
                    {
                        $this->setTemplate("archive-" . $params[1]);
                        $this->setVariable("posts", $posts);
                        $this->setVariable("title", $this->getTranslator()->translate("sjakkspill.archive." . $params[1]));
                    }
                    else $this->setTemplate("anker.error");
                }
                else if($params[0] != "page")
                {
                    $post = $this->postFacade->getPostBySlugAndType($params[0], $params[1], $this->getTranslator()->getLocale());
                    if (count($post) > 0) {
                        $post = $post[0];
                        $this->setTemplate($post["type"]["slug"]);
                        $this->setVariable("post", $post);
                    } else $this->setTemplate("anker.error");
                }
                else $this->lookForPost($imploded);
            }
            else $this->lookForPost($imploded);
        }
    }

    public function lookForPost($imploded)
    {
        $post = $this->postFacade->getPostBySlug($imploded, $this->getTranslator()->getLocale());
        if (count($post) > 0) {
            $post = $post[0];
            $this->setTemplate($post["type"]["slug"]);
            $this->setVariable("post", $post);
        } else $this->setTemplate("anker.error");
    }
}